'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class KardexSuppliesSchema extends Schema {
  up () {
    this.create('kardex_supplies', (table) => {
      table.increments()
      table.integer('supply_id').unsigned()
      table.datetime('operation_date')
      table.string('type') //ingreso, salida, inventario
      table.string('sub_type').nullable() //compra, venta, nulacion de compra, registro de inventario
      table.integer('operation_id').unsigned().nullable()
      table.string('type_document').nullable()
      table.string('document').nullable()
      table.decimal('quantity', 10, 2)
      table.decimal('stock', 10, 2)
      table.decimal('cost', 10, 2).nullable()
      table.decimal('total', 10, 2).nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('kardex_supplies')
  }
}

module.exports = KardexSuppliesSchema
