'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DetailOrdersSchema extends Schema {
  up () {
    this.alter('detail_orders', (table) => {
      table.integer('order_id_original').nullable();
    })
  }

  down () {
    this.alter('detail_orders', (table) => {
      table.dropColumn('order_id_original');
    })
  }
}

module.exports = DetailOrdersSchema
