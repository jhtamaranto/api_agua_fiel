'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CustomerPriceSchema extends Schema {
  up () {
    this.create('customer_prices', (table) => {
      table.increments();
      table.integer('customer_id');
      table.integer('product_id');
      table.decimal('price', 10, 2);
      table.timestamps();
    })
  }

  down () {
    this.drop('customer_prices')
  }
}

module.exports = CustomerPriceSchema
