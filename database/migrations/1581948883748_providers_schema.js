'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProvidersSchema extends Schema {
  up () {
    this.create('providers', (table) => {
      table.increments()
      table.string('document')
      table.string('name')
      table.string('phone').nullable()
      table.string('email').nullable()
      table.string('address').nullable()
      table.string('status').default('activo')
      table.timestamps()
    })
  }

  down () {
    this.drop('providers')
  }
}

module.exports = ProvidersSchema
