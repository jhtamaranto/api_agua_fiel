'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BankOperationsSchema extends Schema {
  up () {
    this.create('bank_operations', (table) => {
      table.increments();
      table.string('type');
      table.decimal('amount', 10, 2);
      table.text('glosa').nullable();
      table.timestamps();
    })
  }

  down () {
    this.drop('bank_operations')
  }
}

module.exports = BankOperationsSchema
