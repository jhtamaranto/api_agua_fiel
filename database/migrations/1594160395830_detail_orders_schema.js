'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DetailOrdersSchema extends Schema {
  up () {
    this.alter('detail_orders', (table) => {
      table.string('name').nullable();
      table.string('type_product').nullable();
    })
  }

  down () {
    this.alter('detail_orders', (table) => {
      table.dropColumn('name');
      table.dropColumn('type_product');
    })
  }
}

module.exports = DetailOrdersSchema
