'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrdersSchema extends Schema {
  up () {
    this.alter('orders', (table) => {
      table.datetime('date_invoice').nullable();
    })
  }

  down () {
    this.alter('orders', (table) => {
      table.dropColumn('date_invoice');
    })
  }
}

module.exports = OrdersSchema
