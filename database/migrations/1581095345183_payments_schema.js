'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PaymentsSchema extends Schema {
  up () {
    this.create('payments', (table) => {
      table.increments()
      table.integer('order_id').unsigned()
      table.datetime('registration_date')
      table.decimal('discount', 10, 2)
      table.decimal('subtotal', 10, 2)
      table.decimal('igv', 10, 2)
      table.decimal('total', 10, 2)
      table.decimal('balance_favor', 10, 2)
      table.decimal('total_debt', 10, 2)
      table.decimal('amount_change', 10, 2)
      table.boolean('save_change').default(false)
      table.integer('quantity_drums').default(0)
      table.string('status').default('activo')
      table.timestamps()
    })
  }

  down () {
    this.drop('payments')
  }
}

module.exports = PaymentsSchema
