'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrdersSchema extends Schema {
  up () {
    this.alter('orders', (table) => {
      table.string('source').defaultTo('orders');
    })
  }

  down () {
    this.alter('orders', (table) => {
      table.dropColumn('source');
    })
  }
}

module.exports = OrdersSchema
