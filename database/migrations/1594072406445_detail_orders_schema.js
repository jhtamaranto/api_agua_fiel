'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DetailOrdersSchema extends Schema {
  up () {
    this.alter('detail_orders', (table) => {
      table.boolean('set_discount_customer').nullable();
    })
  }

  down () {
    this.alter('detail_orders', (table) => {
      table.dropColumn('set_discount_customer');
    })
  }
}

module.exports = DetailOrdersSchema
