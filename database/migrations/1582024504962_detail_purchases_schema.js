'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DetailPurchasesSchema extends Schema {
  up () {
    this.create('detail_purchases', (table) => {
      table.increments()
      table.integer('purchase_id').unsigned()
      table.integer('supply_id').unsigned()
      table.string('supply_name')
      table.decimal('quantity', 10, 2)
      table.decimal('price', 10, 2)
      table.decimal('subtotal', 10, 2)
      table.timestamps()
    })
  }

  down () {
    this.drop('detail_purchases')
  }
}

module.exports = DetailPurchasesSchema
