'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrdersSchema extends Schema {
  up () {
    this.alter('orders', (table) => {
      table.string('type_document_old').nullable();
      table.string('serie_old').nullable();
      table.string('correlative_old').nullable();
    })
  }

  down () {
    this.alter('orders', (table) => {
      table.dropColumn('type_document_old');
      table.dropColumn('serie_old');
      table.dropColumn('correlative_old');
    })
  }
}

module.exports = OrdersSchema
