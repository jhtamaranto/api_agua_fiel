'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DrumsSchema extends Schema {
  up () {
    this.alter('drums', (table) => {
      table.string('confirmed').nullable();
    })
  }

  down () {
    this.alter('drums', (table) => {
      table.dropColumn('confirmed');
    })
  }
}

module.exports = DrumsSchema
