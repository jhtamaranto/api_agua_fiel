'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProviderAccountSchema extends Schema {
  up () {
    this.create('provider_accounts', (table) => {
      table.increments()
      table.integer('provider_id').unsigned()
      table.integer('purchase_id').unsigned()
      table.date('entry_date').nullable()
      table.decimal('amount', 10, 2).nullable()
      table.decimal('amount_paid', 10, 2).nullable()
      table.decimal('amount_pending', 10, 2).nullable()
      table.date('payment_date').nullable()
      table.integer('term_days').nullable()
      table.date('date_agreement').nullable()
      table.string('status').default('activo')
      table.timestamps()
    })
  }

  down () {
    this.drop('provider_accounts')
  }
}

module.exports = ProviderAccountSchema
