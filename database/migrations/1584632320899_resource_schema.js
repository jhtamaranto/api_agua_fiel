'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ResourceSchema extends Schema {
  up () {
    this.alter('resources', (table) => {
      table.integer('menu_id').unsigned().nullable()
    })
  }

  down () {
    this.alter('resources', (table) => {
      table.dropColumn('menu_id')
    })
  }
}

module.exports = ResourceSchema
