'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DebtsSchema extends Schema {
  up () {
    this.create('debts', (table) => {
      table.increments()
      table.integer('customer_id').unsigned()
      table.datetime('entry_date')
      table.decimal('amount', 10, 2)
      table.decimal('amount_paid', 10, 2).nullable().default(0)
      table.decimal('amount_pending', 10, 2).nullable().default(0)
      table.datetime('payment_date').nullable()
      table.string('status').default('pendiente')
      table.timestamps()
    })
  }

  down () {
    this.drop('debts')
  }
}

module.exports = DebtsSchema
