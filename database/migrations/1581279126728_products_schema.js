'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductsSchema extends Schema {
  up () {
    this.alter('products', (table) => {
      table.boolean('has_return').default(false)
    })
  }

  down () {
    this.alter('products', (table) => {
      table.dropColumn('has_return')
    })
  }
}

module.exports = ProductsSchema
