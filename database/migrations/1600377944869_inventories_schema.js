'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class InventoriesSchema extends Schema {
  up () {
    this.alter('inventories', (table) => {
      table.decimal('stock_empty').defaultTo(0);
    })
  }

  down () {
    this.alter('inventories', (table) => {
      table.dropColumn('stock_empty');
    })
  }
}

module.exports = InventoriesSchema
