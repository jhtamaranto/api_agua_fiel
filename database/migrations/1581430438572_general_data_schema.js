'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class GeneralDataSchema extends Schema {
  up () {
    this.create('general_data', (table) => {
      table.increments()
      table.string('company_document')
      table.string('company_name')
      table.string('company_address')      
      table.decimal('igv', 8, 2)
      table.string('company_email').nullable()
      table.string('company_phone').nullable()
      table.string('status').default('activo')
      table.timestamps()
    })
  }

  down () {
    this.drop('general_data')
  }
}

module.exports = GeneralDataSchema
