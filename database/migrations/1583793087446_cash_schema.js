'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CashSchema extends Schema {
  up () {
    this.create('cash', (table) => {
      table.increments()
      table.datetime('register_date')
      table.decimal('amount_open', 10, 2).default(0)
      table.decimal('amount_close', 10, 2).default(0)
      table.decimal('total_amount', 10, 2).default(0)
      table.datetime('close_date').nullable()
      table.string('status').default('open')
      table.timestamps()
    })
  }

  down () {
    this.drop('cash')
  }
}

module.exports = CashSchema
