'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DetailOrdersSchema extends Schema {
  up () {
    this.alter('detail_orders', (table) => {
      table.decimal('discount_value', 10, 2).nullable().defaultTo(0);
      table.boolean('discount_group').nullable().defaultTo(false);
      table.decimal('discount_item', 10, 2).nullable().defaultTo(0);
    })
  }

  down () {
    this.alter('detail_orders', (table) => {
      table.dropColumn('discount_value');
      table.dropColumn('discount_group');
      table.dropColumn('discount_item');
    })
  }
}

module.exports = DetailOrdersSchema
