'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CashSchema extends Schema {
  up () {
    this.alter('cash', (table) => {
      table.decimal('amount_income', 10, 2).default(0)
      table.decimal('amount_expense', 10, 2).default(0)
      table.string('condition').nullable()
    })
  }

  down () {
    this.alter('cash', (table) => {
      table.dropColumn('amount_income')
      table.dropColumn('amount_expense')
      table.dropColumn('condition')
    })
  }
}

module.exports = CashSchema
