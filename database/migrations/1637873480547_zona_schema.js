'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ZonaSchema extends Schema {
  up () {
    this.create('zonas', (table) => {
      table.increments('id')
      table.string('nombre',255).unique()
      table.text('descripcion').nullable()
      table.text('referencia').nullable()
      table.integer('estado').default(1)
      table.timestamps()
    })
  }

  down () {
    this.drop('zonas')
  }
}

module.exports = ZonaSchema
