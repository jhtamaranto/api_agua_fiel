'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DebtsSchema extends Schema {
  up () {
    this.alter('debts', (table) => {
      table.string('guide_number').nullable();
    })
  }

  down () {
    this.alter('debts', (table) => {
      table.dropColumn('guide_number');
    })
  }
}

module.exports = DebtsSchema
