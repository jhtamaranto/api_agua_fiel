'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrdersSchema extends Schema {
  up () {
    this.alter('orders', (table) => {
      table.string('guide_number').nullable();
    })
  }

  down () {
    this.alter('orders', (table) => {
      table.dropColumn('guide_number');
    })
  }
}

module.exports = OrdersSchema
