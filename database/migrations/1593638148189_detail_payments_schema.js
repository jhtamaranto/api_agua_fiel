'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DetailPaymentsSchema extends Schema {
  up () {
    this.alter('detail_payments', (table) => {
      table.integer('user_id').nullable();
    })
  }

  down () {
    this.alter('detail_payments', (table) => {
      table.dropColumn('user_id');
    })
  }
}

module.exports = DetailPaymentsSchema
