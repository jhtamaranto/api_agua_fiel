'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrdersSchema extends Schema {
  up () {
    this.alter('orders', (table) => {
      table.decimal('discount', 10, 2).nullable().defaultTo(0);
    })
  }

  down () {
    this.alter('orders', (table) => {
      table.dropColumn('discount');
    })
  }
}

module.exports = OrdersSchema
