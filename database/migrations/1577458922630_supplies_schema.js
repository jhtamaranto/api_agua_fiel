'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SuppliesSchema extends Schema {
  up () {
    this.create('supplies', (table) => {
      table.increments()
      table.integer('category_id').unsigned().nullable()
      table.integer('unit_id').unsigned().nullable()      
      table.string('name')
      table.decimal('cost', 10, 2)
      table.decimal('stock', 10, 2).nullable()
      table.decimal('minimum_stock', 10, 2).nullable()
      table.string('status').default('activo')
      table.timestamps()
    })
  }

  down () {
    this.drop('supplies')
  }
}

module.exports = SuppliesSchema
