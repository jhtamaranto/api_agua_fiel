'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SuppliesSchema extends Schema {
  up () {
    this.alter('supplies', (table) => {
      table.string('has_return').nullable();
      table.string('brand').nullable();
    })
  }

  down () {
    this.alter('supplies', (table) => {
      table.dropColumn('has_return');
      table.dropColumn('brand');
    })
  }
}

module.exports = SuppliesSchema
