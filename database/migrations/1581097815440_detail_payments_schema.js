'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DetailPaymentsSchema extends Schema {
  up () {
    this.create('detail_payments', (table) => {
      table.increments()
      table.integer('payment_id').unsigned()
      table.string('payment_method')
      table.decimal('payment_amount')
      table.string('status').default('activo')
      table.timestamps()
    })
  }

  down () {
    this.drop('detail_payments')
  }
}

module.exports = DetailPaymentsSchema
