'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class GeneralDataSchema extends Schema {
  up () {
    this.alter('general_data', (table) => {
      table.string('phone_aux').nullable();
    })
  }

  down () {
    this.alter('general_data', (table) => {
      table.dropColumn('phone_aux');
    })
  }
}

module.exports = GeneralDataSchema
