'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AsignacionSchema extends Schema {
  up () {
    this.create('asignacion', (table) => {
      table.increments('id').primary()
      table.integer('prevendedor_id')
      table.integer('zona_id')
      table.integer('cliente_id')
      table.
      table.timestamps()
    })
  }

  down () {
    this.drop('asignacions')
  }
}

module.exports = AsignacionSchema
