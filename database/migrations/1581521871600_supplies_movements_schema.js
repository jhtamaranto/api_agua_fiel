'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SuppliesMovementsSchema extends Schema {
  up () {
    this.create('supplies_movements', (table) => {
      table.increments()
      table.integer('supply_id').unsigned()
      table.datetime('operation_date')
      table.decimal('quantity', 10, 2).default(0)
      table.string('type')
      table.string('document').nullable()
      table.integer('operation_id').unsigned().nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('supplies_movements')
  }
}

module.exports = SuppliesMovementsSchema
