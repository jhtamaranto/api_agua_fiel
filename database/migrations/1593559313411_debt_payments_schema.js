'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DebtPaymentsSchema extends Schema {
  up () {
    this.alter('debt_payments', (table) => {
      table.string('method_payment').nullable();
    })
  }

  down () {
    this.alter('debt_payments', (table) => {
      table.dropColumn('method_payment');
    })
  }
}

module.exports = DebtPaymentsSchema
