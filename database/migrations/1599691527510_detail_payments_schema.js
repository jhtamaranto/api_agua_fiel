'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DetailPaymentsSchema extends Schema {
  up () {
    this.alter('detail_payments', (table) => {
      table.boolean('confirmed');
    })
  }

  down () {
    this.alter('detail_payments', (table) => {
      table.dropColumn('confirmed');
    })
  }
}

module.exports = DetailPaymentsSchema
