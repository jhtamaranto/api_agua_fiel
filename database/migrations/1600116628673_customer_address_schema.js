'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CustomerAddressSchema extends Schema {
  up () {
    this.create('customer_addresses', (table) => {
      table.increments();
      table.integer('customer_id');
      table.string('address');
      table.string('reference');
      table.timestamps()
    })
  }

  down () {
    this.drop('customer_addresses')
  }
}

module.exports = CustomerAddressSchema
