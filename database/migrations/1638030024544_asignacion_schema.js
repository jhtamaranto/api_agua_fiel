'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AsignacionSchema extends Schema {
  up () {
    this.alter('asignacion', (table) => {
     table.dateTime('fechaIni')
     table.dateTime('fechaFin')
    })
  }

  down () {
    this.drop('asignacions')
  }
}

module.exports = AsignacionSchema
