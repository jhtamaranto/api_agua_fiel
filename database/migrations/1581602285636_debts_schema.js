'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DebtsSchema extends Schema {
  up () {
    this.alter('debts', (table) => {
      table.integer('order_id').unsigned().nullable()
    })
  }

  down () {
    this.alter('debts', (table) => {
      table.dropColumn('order_id')
    })
  }
}

module.exports = DebtsSchema
