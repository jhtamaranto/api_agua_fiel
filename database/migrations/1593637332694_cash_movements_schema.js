'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CashMovementsSchema extends Schema {
  up () {
    this.alter('cash_movements', (table) => {
      table.integer('user_id').nullable();
    })
  }

  down () {
    this.alter('cash_movements', (table) => {
      table.dropColumn('user_id');
    })
  }
}

module.exports = CashMovementsSchema
