'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AssetsSchema extends Schema {
  up () {
    this.alter('assets', (table) => {
      table.decimal('price', 10, 2).nullable();
    })
  }

  down () {
    this.alter('assets', (table) => {
      table.dropColumn('price');
    })
  }
}

module.exports = AssetsSchema
