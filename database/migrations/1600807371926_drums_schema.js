'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DrumsSchema extends Schema {
  up () {
    this.alter('drums', (table) => {
      table.date('register_date');
    })
  }

  down () {
    this.alter('drums', (table) => {
      table.dropColumn('register_date');
    })
  }
}

module.exports = DrumsSchema
