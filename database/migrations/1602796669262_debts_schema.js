'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DebtsSchema extends Schema {
  up () {
    this.alter('debts', (table) => {
      table.integer('order_id_generated').nullable();
    })
  }

  down () {
    this.table('debts', (table) => {
      table.dropColumn('order_id_generated');
    })
  }
}

module.exports = DebtsSchema
