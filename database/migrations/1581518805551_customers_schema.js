'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CustomersSchema extends Schema {
  up () {
    this.alter('customers', (table) => {
      table.integer('pending_drums').default(0)
    })
  }

  down () {
    this.alter('customers', (table) => {
      table.dropColumn('pending_drums')
    })
  }
}

module.exports = CustomersSchema
