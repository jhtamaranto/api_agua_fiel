'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DrumsSchema extends Schema {
  up () {
    this.create('drums', (table) => {
      table.increments();
      table.integer('order_id');
      table.integer('product_id');
      table.string('brand');
      table.integer('quantity');
      table.timestamps()
    })
  }

  down () {
    this.drop('drums')
  }
}

module.exports = DrumsSchema
