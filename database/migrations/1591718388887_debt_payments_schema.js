'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DebtPaymentsSchema extends Schema {
  up () {
    this.alter('debt_payments', (table) => {
      table.integer('cash_id').nullable();
    })
  }

  down () {
    this.alter('debt_payments', (table) => {
      table.dropColumn('cash_id');
    })
  }
}

module.exports = DebtPaymentsSchema
