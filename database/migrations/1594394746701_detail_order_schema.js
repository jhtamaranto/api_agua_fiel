'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DetailOrderSchema extends Schema {
  up () {
    this.alter('detail_orders', (table) => {
      table.decimal('subtotal_original', 10, 2).defaultTo(0);
    })
  }

  down () {
    this.alter('detail_orders', (table) => {
      table.dropColumn('subtotal_original');
    })
  }
}

module.exports = DetailOrderSchema
