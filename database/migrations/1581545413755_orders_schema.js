'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrdersSchema extends Schema {
  up () {
    this.alter('orders', (table) => {
      table.integer('pending_drums').default(0)
    })
  }

  down () {
    this.alter('orders', (table) => {
      table.dropColumn('pending_drums')
    })
  }
}

module.exports = OrdersSchema
