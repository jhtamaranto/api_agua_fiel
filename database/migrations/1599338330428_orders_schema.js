'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrdersSchema extends Schema {
  up () {
    this.alter('orders', (table) => {
      table.decimal('total_gratuitas', 10, 2).defaultTo(0);
    })
  }

  down () {
    this.alter('orders', (table) => {
      table.dropColumn('total_gratuitas');
    })
  }
}

module.exports = OrdersSchema
