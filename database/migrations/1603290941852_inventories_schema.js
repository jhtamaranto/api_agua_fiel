'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class InventoriesSchema extends Schema {
  up () {
    this.alter('inventories', (table) => {
      table.datetime('deleted_at').nullable();
    })
  }

  down () {
    this.alter('inventories', (table) => {
      table.dropColumn('deleted_at');
    })
  }
}

module.exports = InventoriesSchema
