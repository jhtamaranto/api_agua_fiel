'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class InventoriesSchema extends Schema {
  up () {
    this.alter('inventories', (table) => {
      table.string('type').nullable();
    })
  }

  down () {
    this.alter('inventories', (table) => {
      table.dropColumn('type');
    })
  }
}

module.exports = InventoriesSchema
