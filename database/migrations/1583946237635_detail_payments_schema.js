'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DetailPaymentsSchema extends Schema {
  up () {
    this.alter('detail_payments', (table) => {
      table.decimal('amount_change', 10, 2).default(0)
    })
  }

  down () {
    this.alter('detail_payments', (table) => {
      table.dropColumn('amount_change')
    })
  }
}

module.exports = DetailPaymentsSchema
