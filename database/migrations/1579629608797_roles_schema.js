'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RolesSchema extends Schema {
  up () {
    this.alter('roles', (table) => {
      table.boolean('superadmin').default(false)
    })
  }

  down () {
    this.alter('roles', (table) => {
      table.dropColumn('superadmin')
    })
  }
}

module.exports = RolesSchema
