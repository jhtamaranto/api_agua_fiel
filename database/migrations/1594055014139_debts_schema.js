'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DebtsSchema extends Schema {
  up () {
    this.alter('debts', (table) => {
      table.date('due_date').nullable();
    })
  }

  down () {
    this.alter('debts', (table) => {
      table.dropColumn('due_date');
    })
  }
}

module.exports = DebtsSchema
