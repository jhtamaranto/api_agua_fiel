'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CompositionsSchema extends Schema {
  up () {
    this.create('compositions', (table) => {
      table.increments()
      table.integer('product_id').unsigned()
      table.integer('supply_id').unsigned()
      table.decimal('quantity', 10, 2)
      table.decimal('cost', 10, 2)
      table.timestamps()
    })
  }

  down () {
    this.drop('compositions')
  }
}

module.exports = CompositionsSchema
