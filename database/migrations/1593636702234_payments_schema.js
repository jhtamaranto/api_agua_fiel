'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PaymentsSchema extends Schema {
  up () {
    this.alter('payments', (table) => {
      table.integer('user_id').nullable();
    })
  }

  down () {
    this.alter('payments', (table) => {
      table.dropColumn('user_id');
    })
  }
}

module.exports = PaymentsSchema
