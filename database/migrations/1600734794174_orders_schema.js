'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrdersSchema extends Schema {
  up () {
    this.alter('orders', (table) => {
      table.string('loan_guide');
    })
  }

  down () {
    this.alter('orders', (table) => {
      table.dropColumn('loan_guide');
    })
  }
}

module.exports = OrdersSchema
