'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TakingsSchema extends Schema {
  up () {
    this.create('takings', (table) => {
      table.increments();
      table.integer('user_id');
      table.date('register_date');
      table.decimal('total_sales', 10, 2);
      table.decimal('total_expenses', 10, 2);
      table.decimal('total_delivered', 10, 2);
      table.timestamps();
    })
  }

  down () {
    this.drop('takings')
  }
}

module.exports = TakingsSchema
