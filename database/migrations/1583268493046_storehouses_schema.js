'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StorehousesSchema extends Schema {
  up () {
    this.create('storehouses', (table) => {
      table.increments()
      table.string('name').nullable()
      table.integer('mobility_id').unsigned().nullable()
      table.boolean('main').default(false)
      table.boolean('active').default(true)
      table.timestamps()
    })
  }

  down () {
    this.drop('storehouses')
  }
}

module.exports = StorehousesSchema
