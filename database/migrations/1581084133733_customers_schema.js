'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CustomersSchema extends Schema {
  up () {
    this.alter('customers', (table) => {
      table.decimal('balance_favor', 10, 2).nullable()
      table.integer('drums').default(0)
    })
  }

  down () {
    this.alter('customers', (table) => {
      table.dropColumn('balance_favor')
      table.dropColumn('drums')
    })
  }
}

module.exports = CustomersSchema
