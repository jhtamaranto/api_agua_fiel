'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductsSchema extends Schema {
  up () {
    this.alter('products', (table) => {
      table.datetime('deleted_at').nullable();
    })
  }

  down () {
    this.table('products', (table) => {
      table.dropColumn('deleted_at');
    })
  }
}

module.exports = ProductsSchema
