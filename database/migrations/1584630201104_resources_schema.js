'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ResourcesSchema extends Schema {
  up () {
    this.alter('resources', (table) => {
      table.string('section_main').nullable()
    })
  }

  down () {
    this.alter('resources', (table) => {
      table.dropColumn('section_main')
    })
  }
}

module.exports = ResourcesSchema
