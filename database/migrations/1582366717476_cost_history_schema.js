'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CostHistorySchema extends Schema {
  up () {
    this.create('cost_histories', (table) => {
      table.increments()
      table.integer('supply_id').unsigned()
      table.datetime('registration_date')
      table.decimal('previous_cost', 10, 2)
      table.decimal('new_cost', 10,2)
      table.timestamps()
    })
  }

  down () {
    this.drop('cost_histories')
  }
}

module.exports = CostHistorySchema
