'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DebtPaymentsSchema extends Schema {
  up () {
    this.create('debt_payments', (table) => {
      table.increments()
      table.integer('debt_id').unsigned()
      table.datetime('payment_date')
      table.string('observations').nullable()
      table.decimal('amount_payment', 10, 2)
      table.timestamps()
    })
  }

  down () {
    this.drop('debt_payments')
  }
}

module.exports = DebtPaymentsSchema
