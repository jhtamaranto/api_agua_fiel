'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CashSchema extends Schema {
  up () {
    this.alter('cash', (table) => {
      table.decimal('amount_close_cash', 10, 2).nullable()
      table.decimal('amount_close_cards', 10, 2).nullable()
    })
  }

  down () {
    this.alter('cash', (table) => {
      table.dropColumn('amount_close_cash')
      table.dropColumn('amount_close_cards')
    })
  }
}

module.exports = CashSchema
