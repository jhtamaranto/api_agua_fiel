'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CreditNotesSchema extends Schema {
  up () {
    this.alter('credit_notes', (table) => {
      table.boolean('send_fe').defaultTo(false);
    })
  }

  down () {
    this.alter('credit_notes', (table) => {
      table.dropColumn('send_fe');
    })
  }
}

module.exports = CreditNotesSchema
