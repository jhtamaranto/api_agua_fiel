'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrdersSchema extends Schema {
  up () {
    this.alter('orders', (table) => {
      table.boolean('batch_invoice').defaultTo(false).nullable();
    })
  }

  down () {
    this.alter('orders', (table) => {
      table.dropColumn('batch_invoice');
    })
  }
}

module.exports = OrdersSchema
