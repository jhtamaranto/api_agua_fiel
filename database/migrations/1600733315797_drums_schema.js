'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DrumsSchema extends Schema {
  up () {
    this.alter('drums', (table) => {
      table.integer('customer_id');
      table.string('status');
      table.string('loan_guide');
    })
  }

  down () {
    this.alter('drums', (table) => {
      table.dropColumn('customer_id');
      table.dropColumn('status');
      table.dropColumn('loan_guide');
    })
  }
}

module.exports = DrumsSchema
