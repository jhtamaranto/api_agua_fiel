'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class GeneralDataSchema extends Schema {
  up () {
    this.alter('general_data', (table) => {
      table.string('user_fe').nullable();
      table.string('password_fe').nullable();
    })
  }

  down () {
    this.alter('general_data', (table) => {
      table.dropColumn('user_fe');
      table.dropColumn('password_fe');
    })
  }
}

module.exports = GeneralDataSchema
