'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class KardexProductsSchema extends Schema {
  up () {
    this.alter('kardex_products', (table) => {
      table.string('description').nullable()
    })
  }

  down () {
    this.alter('kardex_products', (table) => {
      table.dropColumn('description')
    })
  }
}

module.exports = KardexProductsSchema
