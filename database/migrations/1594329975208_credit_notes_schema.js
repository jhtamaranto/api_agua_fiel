'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CreditNotesSchema extends Schema {
  up () {
    this.create('credit_notes', (table) => {
      table.increments();
      table.integer('order_id');
      table.string('reason');
      table.integer('user_id');
      table.string('type_document');
      table.string('serie');
      table.string('correlative');
      table.timestamps();
    })
  }

  down () {
    this.drop('credit_notes')
  }
}

module.exports = CreditNotesSchema
