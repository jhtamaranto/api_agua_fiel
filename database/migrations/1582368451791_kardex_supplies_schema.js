'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class KardexSuppliesSchema extends Schema {
  up () {
    this.alter('kardex_supplies', (table) => {
      table.string('serie').nullable()
      table.string('correlative').nullable()
    })
  }

  down () {
    this.alter('kardex_supplies', (table) => {
      table.dropColumn('serie')
      table.dropColumn('correlative')
    })
  }
}

module.exports = KardexSuppliesSchema
