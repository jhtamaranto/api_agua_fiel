'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class InventoriesSchema extends Schema {
  up () {
    this.create('inventories', (table) => {
      table.increments()
      table.integer('storehouse_id').unsigned()
      table.integer('product_id').unsigned()
      table.datetime('date_entry')
      table.decimal('stock', 10, 2).default(0)
      table.decimal('stock_previous', 10, 2).default(0)
      table.boolean('active').default(true)
      table.timestamps()
    })
  }

  down () {
    this.drop('inventories')
  }
}

module.exports = InventoriesSchema
