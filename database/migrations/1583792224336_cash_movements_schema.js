'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CashMovementsSchema extends Schema {
  up () {
    this.create('cash_movements', (table) => {
      table.increments()
      table.datetime('register_date')
      table.string('type')
      table.string('subtype').nullable()
      table.decimal('amount', 10, 2)
      table.string('description').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('cash_movements')
  }
}

module.exports = CashMovementsSchema
