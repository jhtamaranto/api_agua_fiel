'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UnitsSchema extends Schema {
  up () {
    this.create('units', (table) => {
      table.increments()
      table.string('name')
      table.string('abbreviation').nullable()
      table.string('status').default('activo')
      table.timestamps()
    })
  }

  down () {
    this.drop('units')
  }
}

module.exports = UnitsSchema
