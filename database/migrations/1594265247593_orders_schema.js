'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrdersSchema extends Schema {
  up () {
    this.alter('orders', (table) => {
      table.string('type_document');
      table.string('serie');
      table.string('correlative');
    })
  }

  down () {
    this.alter('orders', (table) => {
      table.dropColumn('type_document');
      table.dropColumn('serie');
      table.dropColumn('correlative');
    })
  }
}

module.exports = OrdersSchema
