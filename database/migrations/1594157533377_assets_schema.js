'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AssetsSchema extends Schema {
  up () {
    this.alter('assets', (table) => {
      table.boolean('loan').nullable();
    })
  }

  down () {
    this.alter('assets', (table) => {
      table.dropColumn('loan');
    })
  }
}

module.exports = AssetsSchema
