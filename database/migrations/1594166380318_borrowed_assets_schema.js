'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BorrowedAssetsSchema extends Schema {
  up () {
    this.create('borrowed_assets', (table) => {
      table.increments();
      table.integer('asset_id');
      table.string('name');
      table.integer('customer_id');
      table.string('status').defaultTo('prestado');
      table.integer('order_id');
      table.integer('quantity');
      table.timestamps()
    })
  }

  down () {
    this.drop('borrowed_assets')
  }
}

module.exports = BorrowedAssetsSchema
