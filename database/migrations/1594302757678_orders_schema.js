'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrdersSchema extends Schema {
  up () {
    this.alter('orders', (table) => {
      table.integer('user_id');
    })
  }

  down () {
    this.alter('orders', (table) => {
      table.dropColumn('user_id');
    })
  }
}

module.exports = OrdersSchema
