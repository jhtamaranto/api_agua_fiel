'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductSchema extends Schema {
  up () {
    this.create('products', (table) => {
      table.increments()
      table.integer('category_id').unsigned()
      table.integer('unit_id').unsigned()
      table.string('name')
      table.decimal('cost', 10, 2)
      table.decimal('unit_price', 10, 2)
      table.decimal('wholesale_price', 10, 2)
      table.decimal('preferential_price', 10, 2)
      table.boolean('compound').nullable()
      table.boolean('supply').nullable()
      table.boolean('check_inventory').nullable()
      table.decimal('stock', 10, 2)
      table.decimal('minimum_stock', 10, 2)
      table.string('image').nullable()
      table.string('status').default('activo')
      table.timestamps()
    })
  }

  down () {
    this.drop('products')
  }
}

module.exports = ProductSchema
