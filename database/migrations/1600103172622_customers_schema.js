'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CustomersSchema extends Schema {
  up () {
    this.alter('customers', (table) => {
      table.string('reference');
    })
  }

  down () {
    this.alter('customers', (table) => {
      table.dropColumn('reference');
    })
  }
}

module.exports = CustomersSchema
