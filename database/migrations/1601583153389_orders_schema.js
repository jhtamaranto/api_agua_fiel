'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrdersSchema extends Schema {
  up () {
    this.alter('orders', (table) => {
      table.text('observation');
    })
  }

  down () {
    this.alter('orders', (table) => {
      table.dropColumn('observation');
    })
  }
}

module.exports = OrdersSchema
