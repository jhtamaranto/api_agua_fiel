'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DrumsSchema extends Schema {
  up () {
    this.alter('drums', (table) => {
      table.text('observation');
    })
  }

  down () {
    this.alter('drums', (table) => {
      table.dropColumn('observation');
    })
  }
}

module.exports = DrumsSchema
