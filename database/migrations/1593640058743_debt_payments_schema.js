'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DebtPaymentsSchema extends Schema {
  up () {
    this.alter('debt_payments', (table) => {
      table.integer('user_id').nullable();
    })
  }

  down () {
    this.alter('debt_payments', (table) => {
      table.dropColumn('user_id');
    })
  }
}

module.exports = DebtPaymentsSchema
