'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DetailOrderSchema extends Schema {
  up () {
    this.create('detail_orders', (table) => {
      table.increments()
      table.integer('order_id').unsigned()
      table.integer('product_id').unsigned()
      table.decimal('quantity', 10, 2)
      table.decimal('base_price', 10, 2)
      table.string('discount_type').nullable()
      table.decimal('discount_amount').default(0)
      table.decimal('subtotal', 10, 2)
      table.timestamps()
    })
  }

  down () {
    this.drop('detail_orders')
  }
}

module.exports = DetailOrderSchema
