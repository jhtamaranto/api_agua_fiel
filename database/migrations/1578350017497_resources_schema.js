'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ResourcesSchema extends Schema {
  up () {
    this.create('resources', (table) => {
      table.increments()
      table.string('model_name')
      table.string('table_name')
      table.string('options')
      table.string('status').default('activo')
      table.timestamps()
    })
  }

  down () {
    this.drop('resources')
  }
}

module.exports = ResourcesSchema
