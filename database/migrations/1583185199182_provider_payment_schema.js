'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProviderPaymentSchema extends Schema {
  up () {
    this.create('provider_payments', (table) => {
      table.increments()
      table.integer('provider_account_id').unsigned()
      table.datetime('payment_date')
      table.string('description').nullable()
      table.decimal('amount_payment', 10, 2)
      table.timestamps()
    })
  }

  down () {
    this.drop('provider_payments')
  }
}

module.exports = ProviderPaymentSchema
