'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ResourcesSchema extends Schema {
  up () {
    this.alter('resources', (table) => {
      table.string('section_name').comment('Nombre de la sección en el frontend, para agrupar los permisos por este nombre')
    })
  }

  down () {
    this.alter('resources', (table) => {
      table.dropColumn('section_name')
    })
  }
}

module.exports = ResourcesSchema
