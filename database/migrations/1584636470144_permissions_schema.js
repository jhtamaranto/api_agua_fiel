'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PermissionsSchema extends Schema {
  up () {
    this.alter('permissions', (table) => {
      table.string('action_name').nullable()
    })
  }

  down () {
    this.alter('permissions', (table) => {
      table.dropColumn('action_name')
    })
  }
}

module.exports = PermissionsSchema
