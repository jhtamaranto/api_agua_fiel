'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CashMovementsSchema extends Schema {
  up () {
    this.alter('cash_movements', (table) => {
      table.integer('cash_id').unsigned()
    })
  }

  down () {
    this.alter('cash_movements', (table) => {
      table.dropColumn('cash_id')
    })
  }
}

module.exports = CashMovementsSchema
