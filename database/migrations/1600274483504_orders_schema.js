'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrdersSchema extends Schema {
  up () {
    this.alter('orders', (table) => {
      table.string('order_address');
    })
  }

  down () {
    this.alter('orders', (table) => {
      table.dropColumn('order_address');
    })
  }
}

module.exports = OrdersSchema
