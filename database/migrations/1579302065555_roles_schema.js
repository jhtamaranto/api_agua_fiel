'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RolesSchema extends Schema {
  up () {
    this.alter('roles', (table) => {
      table.string('status').default('activo')
    })
  }

  down () {
    this.alter('roles', (table) => {
      table.dropColumn('status')
    })
  }
}

module.exports = RolesSchema
