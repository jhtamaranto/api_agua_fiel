'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DetailOrdersSchema extends Schema {
  up () {
    this.alter('detail_orders', (table) => {
      table.string('type_sale').nullable();
    })
  }

  down () {
    this.alter('detail_orders', (table) => {
      table.dropColumn('type_sale');
    })
  }
}

module.exports = DetailOrdersSchema
