'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DetailOrdersSchema extends Schema {
  up () {
    this.alter('detail_orders', (table) => {
      table.decimal('unit_price', 10, 2).nullable().defaultTo(0);
      table.decimal('wholesale_price', 10, 2).nullable().defaultTo(0);
      table.decimal('preferential_price', 10, 2).nullable().defaultTo(0);
    })
  }

  down () {
    this.alter('detail_orders', (table) => {
      table.dropColumn('unit_price');
      table.dropColumn('wholesale_price');
      table.dropColumn('preferential_price');
    })
  }
}

module.exports = DetailOrdersSchema
