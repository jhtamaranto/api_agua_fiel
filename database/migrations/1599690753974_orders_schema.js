'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrdersSchema extends Schema {
  up () {
    this.alter('orders', (table) => {
      table.string('type_sale');
      table.string('credit_sales_guide');
      table.string('attention_guide');
    })
  }

  down () {
    this.alter('orders', (table) => {
      table.dropColumn('type_sale');
      table.dropColumn('credit_sales_guide');
      table.dropColumn('attention_guide');
    })
  }
}

module.exports = OrdersSchema
