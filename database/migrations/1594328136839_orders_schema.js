'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrdersSchema extends Schema {
  up () {
    this.alter('orders', (table) => {
      table.boolean('canceled').nullable().defaultTo(false);
    })
  }

  down () {
    this.alter('orders', (table) => {
      table.dropColumn('canceled');
    })
  }
}

module.exports = OrdersSchema
