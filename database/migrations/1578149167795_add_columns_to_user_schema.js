'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddColumnsToUserSchema extends Schema {
  up () {
    this.alter('users', (table) => {
      table.string('first_name')
      table.string('last_name')
      table.string('status').default('activo')
    })
  }

  down () {
    this.table('users', (table) => {
      table.dropColumn('first_name')
      table.dropColumn('last_name')
      table.dropColumn('status')
    })
  }
}

module.exports = AddColumnsToUserSchema
