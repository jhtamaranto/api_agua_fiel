'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SuppliesSchema extends Schema {
  up () {
    this.alter('supplies', (table) => {
      table.decimal('stock', 10, 2).nullable().default(0).alter()
      table.decimal('minimum_stock', 10, 2).nullable().default(0).alter()
    })
  }

  down () {
    this.alter('supplies', (table) => {
      // reverse alternations
    })
  }
}

module.exports = SuppliesSchema
