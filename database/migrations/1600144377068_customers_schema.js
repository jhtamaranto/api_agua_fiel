'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CustomersSchema extends Schema {
  up () {
    this.alter('customers', (table) => {
      table.string('mobile');
    })
  }

  down () {
    this.alter('customers', (table) => {
      table.dropColumn('mobile');
    })
  }
}

module.exports = CustomersSchema
