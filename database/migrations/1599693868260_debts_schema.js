'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DebtsSchema extends Schema {
  up () {
    this.alter('debts', (table) => {
      table.string('credit_sales_guide');
      table.string('attention_guide');
    })
  }

  down () {
    this.alter('debts', (table) => {
      table.string('credit_sales_guide');
      table.string('attention_guide');
    })
  }
}

module.exports = DebtsSchema
