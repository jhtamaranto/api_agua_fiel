'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AssetsSchema extends Schema {
  up () {
    this.create('assets', (table) => {
      table.increments()
      table.integer('unit_id').unsigned()
      table.string('name')
      table.string('description')
      table.decimal('useful_life', 10, 2)
      table.decimal('quantity', 10, 2)
      table.string('status').default('activo')
      table.timestamps()
    })
  }

  down () {
    this.drop('assets')
  }
}

module.exports = AssetsSchema
