'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CashMovementsSchema extends Schema {
  up () {
    this.alter('cash_movements', (table) => {
      table.integer('delivery_man').nullable();
    })
  }

  down () {
    this.alter('cash_movements', (table) => {
      table.dropColumn('delivery_man');
    })
  }
}

module.exports = CashMovementsSchema
