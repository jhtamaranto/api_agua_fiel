'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrderStatusSchema extends Schema {
  up () {
    this.create('order_status', (table) => {
      table.increments()
      table.string('name')
      table.string('action_name').nullable()
      table.string('options').nullable()
      table.string('status').default('activo')
      table.timestamps()
    })
  }

  down () {
    this.drop('order_status')
  }
}

module.exports = OrderStatusSchema
