'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class KardexProductsSchema extends Schema {
  up () {
    this.alter('kardex_products', (table) => {
      table.text('reason_decrease');
    })
  }

  down () {
    this.table('kardex_products', (table) => {
      table.dropColumn('reason_decrease');
    })
  }
}

module.exports = KardexProductsSchema
