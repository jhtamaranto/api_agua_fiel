'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DetailOrdersSchema extends Schema {
  up () {
    this.alter('detail_orders', (table) => {
      table.boolean('is_bonus').defaultTo(false);
    })
  }

  down () {
    this.alter('detail_orders', (table) => {
      table.dropColumn('is_bonus');
    })
  }
}

module.exports = DetailOrdersSchema
