'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MobilitiesSchema extends Schema {
  up () {
    this.create('mobilities', (table) => {
      table.increments()
      table.string('tuition_number')
      table.string('brand')
      table.string('color')
      table.string('type')
      table.integer('user_id').unsigned()
      table.string('status').default('activo')
      table.timestamps()
    })
  }

  down () {
    this.drop('mobilities')
  }
}

module.exports = MobilitiesSchema
