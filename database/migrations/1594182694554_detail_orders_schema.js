'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DetailOrdersSchema extends Schema {
  up () {
    this.alter('detail_orders', (table) => {
      table.boolean('has_return').nullable().defaultTo(false);
    })
  }

  down () {
    this.alter('detail_orders', (table) => {
      table.dropColumn('has_return');
    })
  }
}

module.exports = DetailOrdersSchema
