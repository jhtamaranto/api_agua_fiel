'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrderSchema extends Schema {
  up () {
    this.create('orders', (table) => {
      table.increments()
      table.integer('customer_id')
      table.datetime('entry_time')
      table.datetime('delivery_time').nullable()
      table.integer('deliveryman').unsigned().nullable()
      table.integer('final_deliveryman').unsigned().nullable()
      table.decimal('subtotal', 10, 2).nullable()
      table.decimal('igv', 10, 2).nullable()
      table.decimal('total', 10, 2).nullable()
      table.boolean('direct_sale').default(0)
      table.string('status')
      table.timestamps()
    })
  }

  down () {
    this.drop('orders')
  }
}

module.exports = OrderSchema
