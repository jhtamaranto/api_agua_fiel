'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PermissionsSchema extends Schema {
  up () {
    this.alter('permissions', (table) => {
      table.boolean('selected').default(false)
    })
  }

  down () {
    this.alter('permissions', (table) => {
      table.dropColumn('selected')
    })
  }
}

module.exports = PermissionsSchema
