'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DetailPaymentsSchema extends Schema {
  up () {
    this.alter('detail_payments', (table) => {
      table.integer('cash_id').nullable();
    })
  }

  down () {
    this.alter('detail_payments', (table) => {
      table.dropColumn('cash_id')
    })
  }
}

module.exports = DetailPaymentsSchema
