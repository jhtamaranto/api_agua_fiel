'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DetailPaymentsSchema extends Schema {
  up () {
    this.alter('detail_payments', (table) => {
      table.string('observations').nullable();
    })
  }

  down () {
    this.alter('detail_payments', (table) => {
      table.dropColumn('observations');
    })
  }
}

module.exports = DetailPaymentsSchema
