'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class KardexProductsSchema extends Schema {
  up () {
    this.create('kardex_products', (table) => {
      table.increments()
      table.integer('inventory_id')
      table.datetime('register_date')
      table.string('type')
      table.string('subtype')
      table.decimal('quantity', 10, 2)
      table.decimal('stock', 10, 2)
      table.boolean('active').default(true)
      table.timestamps()
    })
  }

  down () {
    this.drop('kardex_products')
  }
}

module.exports = KardexProductsSchema
