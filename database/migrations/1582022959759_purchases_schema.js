'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PurchasesSchema extends Schema {
  up () {
    this.create('purchases', (table) => {
      table.increments()
      table.integer('provider_id').unsigned()
      table.date('voucher_date')
      table.string('serie')
      table.string('correlative')
      table.boolean('is_credit')
      table.integer('term_days').nullable()
      table.date('payment_date').nullable()
      table.decimal('subtotal', 10 ,2)
      table.decimal('igv', 10 ,2)
      table.decimal('total', 10 ,2)
      table.boolean('is_paid')
      table.string('status').default('activo')
      table.timestamps()
    })
  }

  down () {
    this.drop('purchases')
  }
}

module.exports = PurchasesSchema
