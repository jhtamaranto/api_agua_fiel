'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CustomersSchema extends Schema {
  up () {
    this.create('customers', (table) => {
      table.increments()
      table.string('type')
      table.string('type_document')
      table.string('document').unique()
      table.string('name')
      table.string('surname').nullable()
      table.string('gender').nullable()
      table.date('birthdate').nullable()
      table.string('phone', 9).nullable()
      table.string('email').nullable()
      table.string('district').nullable()
      table.string('address')      
      table.decimal('credit_line', 10, 2).nullable()
      table.decimal('discount', 10, 2).nullable()
      table.string('status').defaultTo('Activo')
      table.decimal('rating', 10, 2).nullable().defaultTo(0)
      table.string('clasification').nullable().defaultTo('cooper').comment('Puede ser: cooper, silver, gold')
      table.timestamps()
      table.timestamp('deleted_at').nullable()
    })
  }

  down () {
    this.drop('customers')
  }
}

module.exports = CustomersSchema
