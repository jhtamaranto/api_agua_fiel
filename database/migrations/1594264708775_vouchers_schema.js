'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VouchersSchema extends Schema {
  up () {
    this.create('vouchers', (table) => {
      table.increments();
      table.string('type');
      table.string('correlative');
      table.integer('length');
      table.integer('serie_start_at');
      table.integer('current_number');
      table.boolean('active');
      table.timestamps()
    })
  }

  down () {
    this.drop('vouchers')
  }
}

module.exports = VouchersSchema
