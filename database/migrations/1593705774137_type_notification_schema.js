'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TypeNotificationSchema extends Schema {
  up () {
    this.create('type_notifications', (table) => {
      table.increments();
      table.string('name');
      table.integer('days_evaluated');
      table.string('status').defaultTo('activo');
      table.timestamps()
    })
  }

  down () {
    this.drop('type_notifications')
  }
}

module.exports = TypeNotificationSchema
