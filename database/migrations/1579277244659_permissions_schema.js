'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PermissionsSchema extends Schema {
  up () {
    this.alter('permissions', (table) => {
      table.integer('resource_id').unsigned()
    })
  }

  down () {
    this.alter('permissions', (table) => {
      table.dropComlumn('resource_id')
    })
  }
}

module.exports = PermissionsSchema
