'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class GeneralDataSchema extends Schema {
  up () {
    this.alter('general_data', (table) => {
      table.decimal('igv_value', 10, 2).nullable();
    })
  }

  down () {
    this.alter('general_data', (table) => {
      table.dropColumn('igv_value');
    })
  }
}

module.exports = GeneralDataSchema
