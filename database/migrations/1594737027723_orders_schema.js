'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrdersSchema extends Schema {
  up () {
    this.alter('orders', (table) => {
      table.integer('canceled_user').nullable();
      table.text('canceled_reason').nullable();
      table.datetime('canceled_date').nullable();
    })
  }

  down () {
    this.alter('orders', (table) => {
      table.dropColumn('canceled_user');
      table.dropColumn('canceled_reason');
      table.dropColumn('canceled_date');
    })
  }
}

module.exports = OrdersSchema
