'use strict'

/*
|--------------------------------------------------------------------------
| CategorySeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

const CategoryModel = use('App/Models/Category')

class CategorySeeder {
  async run () {
    /*
  	await CategoryModel.truncate()
    
    await CategoryModel.create({
      name: 'Utiles'
    })

  	await CategoryModel.create({
  		name: 'Bebidas'
  	})
  	
  	await CategoryModel.create({
  		name: 'Abarrotes'
  	})
    */
    
  }
}

module.exports = CategorySeeder
