'use strict'

/*
|--------------------------------------------------------------------------
| MobilitySeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

const MobilityModel =  use('App/Models/Mobility')

class MobilitySeeder {
  async run () {
    /*
  	await MobilityModel.truncate()
    
    await MobilityModel.create({
      tuition_number: '123-456',
      brand: 'marca A',
      color: 'rojo',
      type: 'tipo A',
      user_id: 1
    })

    await MobilityModel.create({
      tuition_number: '987-456',
      brand: 'Marca B',
      color: 'Azul',
      type: 'Tipo B',
      user_id: 2
    })

    await MobilityModel.create({
      tuition_number: 'abc-632',
      brand: 'Marca C',
      color: 'Verde',
      type: 'Tipo C',
      user_id: 3
    })
    */
    
  }
}

module.exports = MobilitySeeder
