'use strict'

/*
|--------------------------------------------------------------------------
| UnitSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

const UnitModel = use('App/Models/Unit')

class UnitSeeder {
  async run () {
    /*
  	await UnitModel.truncate()

  	await UnitModel.create({
  		name: 'Unidad',
  		abbreviation: 'Und'
  	})
  	await UnitModel.create({
  		name: 'Kilogramo',
  		abbreviation: 'Kg'
  	})
  	await UnitModel.create({
  		name: 'Caja',
  		abbreviation: 'cj'
  	})
  	await UnitModel.create({
  		name: 'Docena',
  		abbreviation: 'Doc'
  	})
    */

  }
}

module.exports = UnitSeeder
