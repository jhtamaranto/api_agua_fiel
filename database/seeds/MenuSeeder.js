'use strict'

/*
|--------------------------------------------------------------------------
| MenuSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Menu = use('App/Models/Menu')

class MenuSeeder {
  async run () {
  	/*
	await Menu.create({
		name: 'Dashboard',
		slug: 'dashboard',
		show: true
	})

	await Menu.create({
		name: 'Administración',
		slug: 'administracion',
		show: true
	})

	await Menu.create({
		name: 'Logística',
		slug: 'logistica',
		show: true
	})

	await Menu.create({
		name: 'Ventas',
		slug: 'ventas',
		show: true
	})

	await Menu.create({
		name: 'Cuentas',
		slug: 'cuentas',
		show: true
	})

	await Menu.create({
		name: 'Caja',
		slug: 'caja',
		show: true
	})

	await Menu.create({
		name: 'Reportes',
		slug: 'reportes',
		show: true
	})

	await Menu.create({
		name: 'Configuración',
		slug: 'configuracion',
		show: true
	})
	*/
  }
}

module.exports = MenuSeeder
