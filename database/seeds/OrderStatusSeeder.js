'use strict'

/*
|--------------------------------------------------------------------------
| OrderStatusSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

const OrderStatusModel = use('App/Models/OrderStatus')

class OrderStatusSeeder {

  async run () {
    /*
  	await OrderStatusModel.truncate()

  	await OrderStatusModel.create({
  		name: 'Anulado',
  		action_name: 'Anular'
  	})

  	await OrderStatusModel.create({
  		name: 'Pagado',
  		action_name: 'Pagar'
  	})

  	await OrderStatusModel.create({
  		name: 'Entregado',
  		action_name: 'Entregar',
  		options: '2'
  	})

  	await OrderStatusModel.create({
  		name: 'En reparto',
  		action_name: 'En reparto',
  		options: '3,2'
  	})

  	await OrderStatusModel.create({
  		name: 'Registrado',
  		action_name: 'Registrado',
  		options: '4,1'
  	})
    */
    

  }
}

module.exports = OrderStatusSeeder
