'use strict'

/*
|--------------------------------------------------------------------------
| SupplySeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

const SupplyModel = use('App/Models/Supply')

class SupplySeeder {
  async run () {
    /*
  	await SupplyModel.truncate()
    
    await SupplyModel.create({
    	category_id: 1,
    	unit_id: 1,
    	name: 'Primer insumo',
    	cost: 5,
    	stock: 10,
    	minimum_stock: 5
    })

    await SupplyModel.create({
    	category_id: 1,
    	unit_id: 1,
    	name: 'Segundo insumo',
    	cost: 10,
    	stock: 15,
    	minimum_stock: 10
    })

    await SupplyModel.create({
    	category_id: 1,
    	unit_id: 1,
    	name: 'Tercer insumo',
    	cost: 15,
    	stock: 10,
    	minimum_stock: 6
    })

    await SupplyModel.create({
    	category_id: 1,
    	unit_id: 1,
    	name: 'Cuarto insumo',
    	cost: 20,
    	stock: 20,
    	minimum_stock: 12
    })
    */
    
  }
}

module.exports = SupplySeeder
