'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

const UserModel = use('App/Models/User')
const RoleModel = use('App/Models/Role')
const RoleUser = use('App/Models/RoleUser')

class UserSeeder {
  async run () {
    /*
  	let user = await UserModel.create({
  		first_name: 'Elbin',
  		last_name: 'Flores',
  		username: 'eflores',
  		email: 'eflores@gmail.com',
  		password: 'secret'
  	})

    let superadmin = await Role.query().where('name', 'SUPERADMIN').first()
    if (superadmin) {
      await RoleUser.create({
        role_id: superadmin.id,
        user_id: user.id
      })
    }
    */
  }
}

module.exports = UserSeeder
