'use strict'

/*
|--------------------------------------------------------------------------
| ResourceSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

const ResourceModel = use('App/Models/Resource')
const PermissionModel = use('App/Models/Permission')

class ResourceSeeder {
  async run () {
    
  	let default_options = 'create,update,delete,read'
    
    await ResourceModel.create({
      model_name: 'Dashboard',
      table_name: 'dashboard',
      options: 'Ver',
      section_name: 'Dashboard',
      menu_id: 1
    })

    //Administracion id=2
    await ResourceModel.create({
      model_name: 'Activos',
      table_name: 'assets',
      options: 'Ver,Listar,Crear,Actualizar,Activar,Inactivar',
      section_name: 'Activos',
      menu_id: 2
    })

    await ResourceModel.create({
      model_name: 'Clientes',
      table_name: 'customers',
      options: 'Ver,Listar,Crear,Actualizar,Activar,Inactivar',
      section_name: 'Clientes',
      menu_id: 2
    })

    await ResourceModel.create({
      model_name: 'Insumos',
      table_name: 'supplies',
      options: 'Ver,Listar,Crear,Actualizar,Activar,Inactivar',
      section_name: 'Insumos',
      menu_id: 2
    })

    await ResourceModel.create({
      model_name: 'Productos',
      table_name: 'products',
      options: 'Ver,Listar,Crear,Actualizar,Activar,Inactivar',
      section_name: 'Productos',
      menu_id: 2
    })

    await ResourceModel.create({
      model_name: 'Proveedores',
      table_name: 'providers',
      options: 'Ver,Listar,Crear,Actualizar,Activar,Inactivar',
      section_name: 'Proveedores',
      menu_id: 2
    })

    await ResourceModel.create({
      model_name: 'Movilidades',
      table_name: 'mobilities',
      options: 'Ver,Listar,Crear,Actualizar,Activar,Inactivar',
      section_name: 'Unidades móviles',
      menu_id: 2
    })

    //Logística id = 3
    await ResourceModel.create({
      model_name: 'Compras',
      table_name: 'purchases',
      options: 'Ver,Registrar',
      section_name: 'Compras',
      menu_id: 3
    })

    await ResourceModel.create({
      model_name: 'Inventario',
      table_name: 'inventories',
      options: 'Ver,Guardar,Ver_Kardex',
      section_name: 'Inventario',
      menu_id: 3
    })

    await ResourceModel.create({
      model_name: 'Storehouse',
      table_name: 'storehouses',
      options: 'Ver,Registrar_Recarga,Registrar_Merma,Registrar_Descargar,Ver_Movimientos,Seleccionar_Almacen',
      section_name: 'Movimientos de almacén',
      menu_id: 3
    })

    //Ventas id = 4
    await ResourceModel.create({
      model_name: 'Pedido',
      table_name: 'orders',
      options: 'Ver,Ingresar_Pedido,Seleccionar_Producto,Pagar_Pedido',
      section_name: 'Pedido',
      menu_id: 4
    })

    await ResourceModel.create({
      model_name: 'Venta',
      table_name: 'orders',
      options: 'Ver,Ver_Historico,Seleccionar_Pedido,Cambiar_Estado,Reasignar_Repartidor',
      section_name: 'Salida de Pedido',
      menu_id: 4
    })

    //Cuentas id = 5

    await ResourceModel.create({
      model_name: 'Cuentas',
      table_name: 'orders',
      options: 'Ver,Ver_Detalle,Amortizar',
      section_name: 'Cuentas por pagar',
      menu_id: 5
    })

    await ResourceModel.create({
      model_name: 'Deudas',
      table_name: 'orders',
      options: 'Ver,Ver_Detalle,Registrar_Pago',
      section_name: 'Deudas de clientes',
      menu_id: 5
    })

    //Caja id = 6
    await ResourceModel.create({
      model_name: 'Movimiento',
      table_name: 'cash',
      options: 'Ver,Abrir_Caja,Registrar_Ingreso,Registrar_Egreso,Cerrar_Caja',
      section_name: 'Movimiento',
      menu_id: 6
    })

    await ResourceModel.create({
      model_name: 'Historial',
      table_name: 'cash',
      options: 'Ver,Ver_Detalle',
      section_name: 'Historial',
      menu_id: 6
    })

    //Reportes id = 7
    await ResourceModel.create({
      model_name: 'Reporte Compras',
      table_name: 'cash',
      options: 'Ver,Exportar',
      section_name: 'Compras',
      menu_id: 7
    })

    await ResourceModel.create({
      model_name: 'Reporte Deudas',
      table_name: 'cash',
      options: 'Ver,Exportar',
      section_name: 'Deudas',
      menu_id: 7
    })

    await ResourceModel.create({
      model_name: 'Reporte Pedidos',
      table_name: 'cash',
      options: 'Ver,Exportar',
      section_name: 'Pedidos',
      menu_id: 7
    })

    await ResourceModel.create({
      model_name: 'Reporte Ventas',
      table_name: 'cash',
      options: 'Ver,Exportar',
      section_name: 'Ventas',
      menu_id: 7
    })
  	
    //Condiguracion id = 8
    await ResourceModel.create({
      model_name: 'Roles',
      table_name: 'cash',
      options: 'Ver,Listar,Crear,Actualizar,Activar,Inactivar',
      section_name: 'Roles de usuario',
      menu_id: 8
    })

    await ResourceModel.create({
      model_name: 'Usuarios',
      table_name: 'cash',
      options: 'Ver,Listar,Crear,Actualizar,Activar,Inactivar',
      section_name: 'Usuario',
      menu_id: 8
    })

  	const resources = await ResourceModel.query()
  										.where('status', 'activo')
  										.orderBy('id', 'asc').fetch()

    for (let index in resources.rows) {
      let resource = resources.rows[index]
      let model_name = resource.model_name
      let options = resource.options.split(',')
      for (let j = 0; j < options.length; j++) {
        let item_option = options[j]
        let value_option = options[j].toLowerCase() + '_' + model_name.toLowerCase().replace(' ','_')

        await PermissionModel.create({
          slug: value_option.replace('_',' '),
          name: value_option,
          action_name: item_option.replace('_',' '),
          resource_id: resource.id
        })
      }
    }  	   
  	
  }
}

module.exports = ResourceSeeder
