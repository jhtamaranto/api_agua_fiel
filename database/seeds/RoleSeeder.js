'use strict'

/*
|--------------------------------------------------------------------------
| RoleSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

const RoleModel = use('App/Models/Role')
const RoleUserModel = use('App/Models/RoleUser')

class RoleSeeder {
  async run () {
  	/*
  	const roleSuperAdmin = new RoleModel()
  	roleSuperAdmin.name = 'SUPERADMIN'
  	roleSuperAdmin.slug = 'superadmin'
    roleSuperAdmin.superadmin = true
  	await roleSuperAdmin.save()
	*/
  }
}

module.exports = RoleSeeder
