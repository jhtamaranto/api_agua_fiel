'use strict'

const Logger = use('Logger')
const Menu = use('App/Models/Menu')
const Permission = use('App/Models/Permission')
const Resource = use('App/Models/Resource')

class MenuController {

	async getMenus({request, response}){
		try{

			let listMenu = []

			//--- Permisos para dashboard 
			let menuDashboard = await Menu.query()
									.where({name: 'Dashboard'})
									.first()
			
			let resourceDashboard = await Resource.query()
										.where('menu_id', menuDashboard.id)
										.where({section_name: 'Dashboard'})
										.first()

			let permissionDashboard = await Permission.query()
														.where('resource_id', resourceDashboard.id)
														.first()

			let itemDashboard = {
				id: resourceDashboard.id + 'r',
				name: resourceDashboard.section_name,
				children: [
					{
						id: permissionDashboard.id,
						name: permissionDashboard.action_name
					}
				]
			}

			listMenu.push(itemDashboard)

			//------

			let menus = await Menu.query()
									.whereNot({name: 'Dashboard'})
									.orderBy('id', 'ASC')
									.fetch()

			for (let index in menus.rows) {
				let menu = menus.rows[index]

				let resources = await Resource.query()
										.where('menu_id', menu.id)
										.whereNot({section_name: 'Dashboard'})
										.orderBy('id', 'ASC')
										.fetch()

				let menuChildren = []
				for (let indexResource in resources.rows) {
					let resource = resources.rows[indexResource]

					let permissions = await Permission.query()
														.where('resource_id', resource.id)
														.orderBy('id', 'ASC')
														.fetch()

					let resourcesChildren = []
					for (let indexPermission in permissions.rows) {
						let permission = permissions.rows[indexPermission]

						let itemPermission = {
							id: permission.id,
							name: permission.action_name
						}
						resourcesChildren.push(itemPermission)
					}

					let itemResource = {
						id: resource.id + 'r',
						name: resource.section_name,
						children: resourcesChildren
					}
					menuChildren.push(itemResource)
				}

				let itemMenu = {
					id: menu.id + 'm',
					name: menu.name,
					children: menuChildren
				}

				listMenu.push(itemMenu)
			}
			
			return response.status(200).json(listMenu)

		} catch(error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al listar menu.'
			})
		}
	}	
}

module.exports = MenuController
