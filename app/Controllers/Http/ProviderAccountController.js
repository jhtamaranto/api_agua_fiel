'use strict'

const Logger = use('Logger')
const DateAndTime = require('date-and-time');

const Database = use('Database')
const ProviderAccount = use('App/Models/ProviderAccount')
const ProviderPayment = use('App/Models/ProviderPayment')
const Purchase = use('App/Models/Purchase')

class ProviderAccountController {

	async pendingDebts({request, response}){
		try{
			let filter = request.all()
			let pendingDebts = await ProviderAccount.pendingDebts(filter)

			return pendingDebts[0]

		} catch(error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al listar cuentas por pagar.'
			})
		}
	}

	async accountsByProvider({request, response}) {
		try {
			const provider_id = request.input('provider_id')
			let debts = await ProviderAccount.query()
										.with('purchase')
										.with('payments')
										.where('provider_id', parseInt(provider_id))
										.orderBy('entry_date', 'asc')										
										.fetch()

			return debts

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al listar cuentas por pagar.'
			})
		}
	}

	async amortize({request, response}) {
		const transaction = await Database.beginTransaction()
		try{
			const account_id = request.input('account_id')
			let account = await ProviderAccount.query().where('id', account_id).first()

			let purchase = await Purchase.find(account.purchase_id)

			let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');

			let payment = await ProviderPayment.create({
				provider_account_id: account_id,
				payment_date: currentDate,
				description: request.input('description'),
				amount_payment: parseFloat(request.input('amount_payment'))
			}, transaction)

			//update provider account
      		account.amount_paid = account.amount_paid + payment.amount_payment
			account.amount_pending = account.amount_pending - payment.amount_payment
			if(account.amount_pending == 0) {
				account.status = 'pagada'
				account.payment_date = currentDate

				purchase.is_paid = true
				await purchase.save(transaction)
			}
			await account.save(transaction)

			await transaction.commit()

			let debts = await ProviderAccount.query()
										.with('purchase')
										.where('provider_id', parseInt(account.provider_id))
										.fetch()

			let filter = {
				provider_id: account.provider_id
			}
			let debtProvider = await ProviderAccount.pendingDebts(filter)

			return response.status(200).json({
				debts: debts,
				debtProvider: debtProvider[0]
			})

		} catch(error) {
			await transaction.rollback()
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al amortizar cuenta por pagar.'
			})
		}
	}

}

module.exports = ProviderAccountController
