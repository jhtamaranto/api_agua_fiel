'use strict'

const Logger = use('Logger')

const Zona = use('App/Models/Zona')


class ZonaController {

  async index({response,request}) {
    try {
      let perPage=request.input('perPage')
      let pageSelected=request.input('pageSelected')
      let query = Zona.query()
      let resultData={data:[]}
      if(perPage && pageSelected){
        if (perPage===-1){
          resultData.data =  await query.orderBy('created_at','desc')
            .fetch()
        }else{
          let debts =  await query
            .orderBy('created_at','desc')
            .paginate(pageSelected,perPage);
          resultData=debts.toJSON()
        }
      }else{
        resultData.data =  await query.orderBy('created_at','desc')
          .fetch()
      }

      return resultData
    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al obtener lista de Zonas.'
      })
    }
  }

  async store({request, response}) {
    try {
      const {nombre, referencia, descripcion} = request.only(['nombre', 'referencia', 'descripcion'])
      const zona = await Zona.create({
        nombre,
        referencia,
        descripcion
      })
      return zona
    } catch (error) {
      console.log(error)
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al registrar zona.',
        error
      })
    }
  }

  async delete({params, response}) {
    try {
      const {id} = params
      let zona = await Zona.find(id)
      let result = zona.delete(zona)
      return result
    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al eliminar zona.'
      })
    }
  }

  async update({params, request}) {
    const {id} = params
    const {nombre, descripcion, referencia} = request.only(['nombre', 'descripcion', 'referencia'])
    try {

      const zona = await Zona.find(id)
      const data = {
        nombre,
        descripcion,
        referencia
      }
      zona.merge(data)
      return await zona.save()

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al intentar actualizar rol.'
      })
    }
  }
  async search({request,response}){
    try{
      const search=request.input('search')
      const results=Zona.query()
        .where('nombre','LIKE',`%${search}%`)
        .orWhere('descripcion','LIKE',`%${search}%`)
        .orWhere('referencia','LIKE',`%${search}%`)
        .fetch()
      return results
    }
    catch (error) {
      console.log(error)
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'error al buscar zonas',
        name:error.name,
        description:error.message
      })
    }
  }
}

module.exports = ZonaController
