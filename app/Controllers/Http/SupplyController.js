'use strict'

const Database = use('Database')
const DateAndTime = require('date-and-time');
const Logger = use('Logger');
const Supply = use('App/Models/Supply');
const KardexSupply = use('App/Models/KardexSupply')

class SupplyController {

	async suppliesDropDown({response}) {
		const supplies = await Supply.query()
								.select('id as value', 'name as text')
								.where('status', 'activo').fetch()

		return supplies
	}

	async supplies({response}) {
		const supplies = await Supply.query().where('status', 'activo').fetch()

		return supplies
	}

	async show({params}) {
		const {id} = params
        const supply = await Supply.find(id)

        return supply
	}

	async index({response}) {
		try{
			const supplies = await Supply.query()
        .with('unit')
        .orderBy('name', 'asc')
        .fetch();

			return response.json(supplies);

		} catch(error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener lista de insumos.'
			})
		}
	}

	async store({request, response}) {
		let { category_id, unit_id, name, cost, has_return, brand } =  request.all()
    let brand_name = null;
		if (brand) {
		  brand_name = brand.toUpperCase();
    }
		try{
		  name = name.trim().toUpperCase();
			const supply = await Supply.create({
				category_id,
				unit_id,
				name,
				cost,
        has_return,
        brand: brand_name
			})

			const supplyCreated = await Supply.query().where('id', supply.id)
        .with('unit')
        .with('category')
        .first();

			return response.json(supplyCreated);

		} catch(error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al registrar insumo.'
			})
		}
	}

	async update({params, request, response}) {
		const {id} = params
		try{
			const supply = await Supply.find(id)

			let { category_id, unit_id, name, cost, has_return, brand } =  request.all();

      name = name.trim().toUpperCase();
      let brand_name = null;
      if (brand) {
        brand_name = brand.toUpperCase();
      }

			const data = {
				category_id,
        unit_id,
        name,
        cost,
        has_return,
        brand: brand_name
			}
			supply.merge(data)

      await supply.save()

      const supplyUpdated = await Supply.query()
        .where('id', supply.id)
        .with('unit')
        .with('category')
        .first();

			return response.json(supplyUpdated);

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al actualizar insumo.'
			})
		}
	}

	async inactivate({params, response}) {
		const {id} = params

		try{
			const supply = await Supply.find(id)

			let newStatus = null
      if (supply.status === 'activo') {
          newStatus = 'inactivo'
      } else{
          newStatus = 'activo'
      }

      supply.merge({
          status: newStatus
      })

      await supply.save()

      const supplyUpdated = await Supply.query()
        .where('id', supply.id)
        .with('unit')
        .with('category')
        .first();

      return response.json(supplyUpdated);

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al inactivar insumo.'
			})
		}
	}

  async brands ({request, response}) {
    try {
      let brands = await Supply.query()
        .distinct('brand')
        .whereNotNull('brand')
        .pluck('brand');

      return response.json(brands);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al listar marcas de insumos.'
      })
    }
  }

  async storeDecrease ({request, response}) {
    const transaction = await Database.beginTransaction()
	  try {

      let new_stocks = request.input('new_stocks')

      let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');
      for (let i = 0; i < new_stocks.length; i++) {
        let itemStock = new_stocks[i]
        let supply = await Supply.find(itemStock.supply_id)

        supply.stock = parseFloat(supply.stock) - parseFloat(itemStock.new_stock);
        await supply.save(transaction);

        await KardexSupply.create({
          supply_id: itemStock.supply_id,
          operation_date: currentDate,
          type: 'SALIDA',
          sub_type: 'MERMA',
          quantity: parseFloat(itemStock.new_stock),
          stock: supply.stock,
          cost: supply.cost,
          total: parseFloat(itemStock.new_stock) * supply.cost
        }, transaction);
      }

      await transaction.commit()

      return response.status(200).json({
        message: 'Opración correcta.'
      })

    } catch (error) {
      await transaction.rollback();
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al registrar merma.'
      })
    }
  }

}

module.exports = SupplyController
