'use strict'

const Unit = use('App/Models/Unit')

class UnitController {

	async dropdown({response}) {
		const units = await Unit.query().select('id as value', 'name as text').where('status', 'activo').fetch()

		return units
	}

}

module.exports = UnitController
