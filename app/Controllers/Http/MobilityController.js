'use strict'

const DetailOrder = use('App/Models/DetailOrder');
const Inventory = use('App/Models/Inventory');
const Logger = use('Logger');
const Mobility = use('App/Models/Mobility');
const Order = use('App/Models/Order');
const Storehouse = use('App/Models/Storehouse');

class MobilityController {

	async index({response}) {
		const mobilities = await Mobility.query().with('user').fetch()

		return mobilities
	}

	async store({request, response}) {

		const mobility = await Mobility.create({
			tuition_number: request.input('tuition_number'),
			brand: request.input('brand'),
			color: request.input('color'),
			type: request.input('type'),
			user_id: request.input('delivery_man')
		})

	    const storehouse = await Storehouse.create({
			name: mobility.tuition_number + ' - ' + mobility.brand,
			mobility_id: mobility.id,
			main: false
	    })

		const newMobility = await Mobility.query().where('id', mobility.id).with('user').first()

		return newMobility
	}

	async update({params, request}) {
		const {id} = params
        let mobility = await Mobility.find(id)
        let delivery_man = request.input('delivery_man')

        if(!delivery_man) {
            delivery_man = null
        }

        const data = {
            tuition_number: request.input('tuition_number'),
			brand: request.input('brand'),
			color: request.input('color'),
			type: request.input('type'),
            user_id: delivery_man
        }

        mobility.merge(data)

        await mobility.save()

        mobility = await Mobility.query().where('id', id).with('user').first()

        let storehouse =await Storehouse.query()
        							.where('mobility_id', mobility.id)
        							.where('active', true).first()

        if (storehouse) {
        	const newStorehouse = {
        		name: mobility.tuition_number + ' - ' + mobility.brand
        	}
        	storehouse.merge(newStorehouse)

        	await storehouse.save()
        } else {
        	await Storehouse.create({
				name: mobility.tuition_number + ' - ' + mobility.brand,
				mobility_id: mobility.id,
				main: false
	    	})
        }

        return mobility
	}

	async inactivate({params}) {
		const {id} = params
        let mobility = await Mobility.find(id)

        let newStatus = null
        if (mobility.status === 'activo') {
            newStatus = 'inactivo'
        } else{
            newStatus = 'activo'
        }

        mobility.merge({
            status: newStatus
        })

        await mobility.save()

        mobility = await Mobility.query().where('id', id).with('user').first()

        return mobility
	}

    async canAttendOrder({request, response}) {
        try {
            let sufficient_stock = true;
            let user_id = request.input('user_id');
            let order_id = request.input('order_id');

            let mobility = await Mobility.query()
                                            .where('user_id', user_id)
                                            .where('status', 'activo')
                                            .orderBy('created_at', 'asc')
                                            .first();

            if (mobility) {
                let storehouse = await Storehouse.query()
                                                    .where('mobility_id', mobility.id)
                                                    .where('active', true)
                                                    .first();

                if (storehouse) {
                    let products = await DetailOrder.query()
                                                    .where('order_id', order_id)
                                                    .fetch();

                    for (let index in products.rows) {
                        //comprobar si tiene stock para despachar los productos
                        let product = products.rows[index];

                        let inventory = await Inventory.query()
                                                        .where('storehouse_id', storehouse.id)
                                                        .where('product_id', product.product_id)
                                                        .first();

                        if (inventory) {
                            if (parseFloat(product.quantity) > parseFloat(inventory.stock)) {
                                sufficient_stock = false;
                            }
                        } else {
                            sufficient_stock = false;
                        }
                    }
                } else {
                    sufficient_stock = false;
                }
            } else {
                sufficient_stock = false;
            }

            return response.json({
                can_attend: sufficient_stock
            });

        } catch (error) {
            Logger.error(error.name + ', ' + error.message)
            return response.status(400).json({
                message: 'Se detectó un error al verificar stock de unidad movil.'
            })
        }
    }
}

module.exports = MobilityController
