'use strict'

const DetailOrder = use('App/Models/DetailOrder');
const Logger = use('Logger')
const DateAndTime = require('date-and-time');
const Excel = require('exceljs');

class ExcelSaleController {

  async index ({request, response}) {
    try {

      let parameters = request.all();

      let query = DetailOrder.query()
        .select([
          'orders.id',
          'orders.type_document',
          'orders.serie',
          'orders.correlative',
          'orders.attention_guide',
          'orders.credit_sales_guide',
          'orders.guide_number',
          'orders.canceled',
          'customers.type as customer_type',
          'customers.type_document as customer_type_document',
          'customers.document as customer_document',
          'customers.name as customer_name',
          'customers.surname as customer_surname',
          'orders.entry_time',
          'users.first_name as user_first_name',
          'users.last_name as user_last_name',
          'orders.type_sale',
          'orders.total',
          'detail_orders.name as product_name',
          'detail_orders.quantity as product_quantity'
        ]).innerJoin('orders', 'orders.id', 'detail_orders.order_id')
        .innerJoin('customers', 'customers.id', 'orders.customer_id')
        .leftJoin('users', 'users.id', 'orders.final_deliveryman')
        .where('orders.canceled', false)
        .where( function () {
          this.where('orders.direct_sale', true)
            .orWhere('orders.status', 'Facturado')
        })
        .where('orders.batch_invoice', false);

      if (parameters.customer_id) {
        query.where('orders.customer_id', parseInt(parameters.customer_id))
      }

      if (parameters.type_document) {
        query.where('orders.type_document', parameters.type_document);
      }

      if (parameters.from) {
        query.whereRaw('DATE(orders.entry_time) >= ?', [parameters.from]);
      }

      if (parameters.until) {
        query.whereRaw('DATE(orders.entry_time) <= ?', [parameters.until]);
      }

      let sales = await query.orderByRaw('orders.entry_time DESC, orders.id DESC')
        .fetch();

      //---------------------------------------------
      const workbook = new Excel.Workbook();
      const sheet = workbook.addWorksheet('Ventas');

      let indexRow = 1;
      let row = sheet.getRow(indexRow);
      //---- Titulos
      const font = {bold: true};

      row.getCell('A').value = 'PEDIDO';
      row.getCell('B').value = 'COMPROBANTE';
      row.getCell('C').value = 'NRO. COMPROBANTE';
      row.getCell('D').value = 'GUIA DE ATENCIÓN';
      row.getCell('E').value = 'GUIA CRÉDITO';
      row.getCell('F').value = 'CLIENTE';
      row.getCell('G').value = 'RUC / DNI';
      row.getCell('H').value = 'FECHA';
      row.getCell('I').value = 'REPARTIDOR';
      row.getCell('J').value = 'MÉTODO DE PAGO';
      row.getCell('K').value = 'TOTAL';
      row.getCell('L').value = 'PRODUCTO';
      row.getCell('M').value = 'CANTIDAD';

      sheet.getColumn('A').width = 15;
      sheet.getColumn('B').width = 15;
      sheet.getColumn('C').width = 20;
      sheet.getColumn('D').width = 20;
      sheet.getColumn('E').width = 15;
      sheet.getColumn('F').width = 40;
      sheet.getColumn('G').width = 15;
      sheet.getColumn('H').width = 15;
      sheet.getColumn('I').width = 25;
      sheet.getColumn('J').width = 18;
      sheet.getColumn('K').width = 15;
      sheet.getColumn('L').width = 25;
      sheet.getColumn('M').width = 15;

      row.getCell('A').font = font;
      row.getCell('B').font = font;
      row.getCell('C').font = font;
      row.getCell('D').font = font;
      row.getCell('E').font = font;
      row.getCell('F').font = font;
      row.getCell('G').font = font;
      row.getCell('H').font = font;
      row.getCell('I').font = font;
      row.getCell('J').font = font;
      row.getCell('K').font = font;
      row.getCell('L').font = font;
      row.getCell('M').font = font;

      const border = {
        bottom: {style: 'medium'}
      };

      row.getCell('A').border = border;
      row.getCell('B').border = border;
      row.getCell('C').border = border;
      row.getCell('D').border = border;
      row.getCell('E').border = border;
      row.getCell('F').border = border;
      row.getCell('G').border = border;
      row.getCell('H').border = border;
      row.getCell('I').border = border;
      row.getCell('J').border = border;
      row.getCell('K').border = border;
      row.getCell('L').border = border;
      row.getCell('M').border = border;

      row.commit();

      const alignment = {
        vertical: 'middle'
      };

      let order_id = 0;
      for (let item of sales.rows) {
        indexRow++;
        row = sheet.getRow(indexRow);

        let type_document = '';
        let number_document_sale = '';
        if (item.type_document) {
          type_document = item.type_document;
          number_document_sale = item.serie + '-' + item.correlative;
        }

        let attention_guide = '';
        let credit_sales_guide = '';
        if (item.attention_guide) {
          attention_guide = item.attention_guide;
        }
        if (item.credit_sales_guide) {
          credit_sales_guide = item.credit_sales_guide;
        } else if (item.guide_number) {
          credit_sales_guide = item.guide_number;
        }
        let customer = item.customer_name;
        let customer_document = '';
        if (item.customer_surname) {
          customer = customer + ' ' + item.customer_surname;
        }
        if (item.customer_document) {
          customer_document = item.customer_document;
        }
        let entry_time = DateAndTime.format(item.entry_time, 'DD/MM/YYYY');
        let user_name = '';
        if (item.user_first_name) {
          user_name = item.user_first_name + ' ';
        }
        if (item.user_last_name) {
          user_name = user_name + item.user_last_name;
        }
        let type_sale = '';
        if (item.type_sale) {
          type_sale = item.type_sale;
        }

        if (order_id !== item.id) {
          order_id = item.id;
          let count = await DetailOrder.query()
            .where('order_id', order_id)
            .count('* as total');

          let rowEnd = indexRow + count[0].total - 1;

          if (count[0].total > 1) {
            sheet.mergeCells(`A${indexRow}:A${rowEnd}`);
            sheet.mergeCells(`B${indexRow}:B${rowEnd}`);
            sheet.mergeCells(`C${indexRow}:C${rowEnd}`);
            sheet.mergeCells(`D${indexRow}:D${rowEnd}`);
            sheet.mergeCells(`E${indexRow}:E${rowEnd}`);
            sheet.mergeCells(`F${indexRow}:F${rowEnd}`);
            sheet.mergeCells(`G${indexRow}:G${rowEnd}`);
            sheet.mergeCells(`H${indexRow}:H${rowEnd}`);
            sheet.mergeCells(`I${indexRow}:I${rowEnd}`);
            sheet.mergeCells(`J${indexRow}:J${rowEnd}`);
            sheet.mergeCells(`K${indexRow}:K${rowEnd}`);
          }

          row.getCell('A').alignment = alignment;
          row.getCell('B').alignment = alignment;
          row.getCell('C').alignment = alignment;
          row.getCell('D').alignment = alignment;
          row.getCell('E').alignment = alignment;
          row.getCell('F').alignment = alignment;
          row.getCell('G').alignment = alignment;
          row.getCell('H').alignment = alignment;
          row.getCell('I').alignment = alignment;
          row.getCell('J').alignment = alignment;
          row.getCell('K').alignment = alignment;

          row.getCell('A').value = item.id;
          row.getCell('B').value = type_document;
          row.getCell('C').value = number_document_sale;
          row.getCell('D').value = attention_guide;
          row.getCell('E').value = credit_sales_guide;
          row.getCell('F').value = customer;
          row.getCell('G').value = customer_document;
          row.getCell('H').value = entry_time;
          row.getCell('I').value = user_name;
          row.getCell('J').value = type_sale;
          row.getCell('K').value = item.total;
          row.getCell('K').numFmt = '0.00';
        }

        row.getCell('L').value = item.product_name;
        row.getCell('M').value = item.product_quantity;
        row.getCell('M').numFmt = '0.00';

        row.commit();

      }

      response.header(`Content-Type`, `application/vnd.openxmlformats-officedocument.spreadsheetml.sheet`)
      response.header(`Content-Disposition`, `attachment; filename="template.xlsx`)

      return workbook.xlsx.write(response.response);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al descargar reporte de ventas.'
      })
    }
  }
}

module.exports = ExcelSaleController
