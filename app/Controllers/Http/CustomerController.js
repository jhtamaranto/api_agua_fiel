'use strict'

const Logger = use('Logger')

const Customer = use('App/Models/Customer')
const {validateAll} = use('Validator')
const Debt = use('App/Models/Debt')
const Order = use('App/Models/Order')

class CustomerController {

  async index({request, response}) {
    try {
      const customers = await Customer.query()
        .whereNull('deleted_at')
        .orderBy('name', 'ASC')
        .fetch();

      return response.json(customers);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al obtener lista de clientes.'
      })
    }

  }

  async indexDatatable({request, response}) {
    try {
      // console.log(request.input('totalItems'))
      let perPage = request.input('perPage')
      let pageSelected = request.input('pageSelected')
      let resultData = {data: []}
      // console.log(pageItems)
      if (perPage == -1) {
        resultData.data = await Customer.query()
          .whereNull('deleted_at')
          .orderBy('name', 'ASC')
          .fetch();
      } else {
        const customers = await Customer.query()
          .whereNull('deleted_at')
          .orderBy('name', 'ASC')
          .paginate(pageSelected, perPage);
        resultData = customers.toJSON()
      }
      return response.json(resultData);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al obtener lista de clientes.'
      })
    }

  }

  async store({request, response}) {
    try {

      const {
        type,
        document,
        name,
        surname,
        gender,
        birthdate,
        phone,
        email,
        district,
        address,
        credit_line,
        discount,
        reference,
        mobile,
        zonaId
      } = request.all();
      let typeDocument = 'DNI';
      if (type === 'Jurídico') {
        typeDocument = 'RUC'
      }
      const customer = await Customer.create({
        type,
        type_document: typeDocument,
        document,
        name,
        surname,
        gender,
        birthdate,
        phone,
        email,
        district,
        address,
        credit_line,
        discount,
        status: 'Activo',
        clasification: 'cooper',
        rating: 0,
        balance_favor: 0,
        reference,
        mobile,
        zona_id:zonaId
      });

      return response.json(customer);

    } catch (error) {
      console.log(error)
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al registrar cliente.'
      })
    }

  }

  async update({params, request}) {
    const {id} = params
    const customer = await Customer.find(id)

    let typeDocument = 'DNI';
    if (request.input('type') === 'Jurídico') {
      typeDocument = 'RUC'
    }

    const data = {
      type: request.input('type'),
      type_document: typeDocument,
      document: request.input('document'),
      name: request.input('name'),
      surname: request.input('surname'),
      gender: request.input('gender'),
      birthdate: request.input('birthdate'),
      phone: request.input('phone'),
      email: request.input('email'),
      district: request.input('district'),
      address: request.input('address'),
      credit_line: request.input('credit_line'),
      discount: request.input('discount'),
      reference: request.input('reference'),
      mobile: request.input('mobile'),
      zona_id:request.input('zonaId')
    }

    customer.merge(data)

    await customer.save()

    const customerUpdated = await Customer.query().where('id', customer.id).first();

    return customerUpdated
  }

  async delete({params, response}) {
    try {
      const {id} = params
      const customer = await Customer.find(id)

      let currentDate = Date.now();

      customer.merge({
        status: 'Inactivo',
        deleted_at: new Date(currentDate)
      })

      await customer.save();

      const customerUpdated = await Customer.query().where('id', customer.id)
        .with('orders').first()

      return response.json(customerUpdated);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al eliminar cliente.'
      })
    }
  }

  async changeStatus({params}) {
    const {id} = params
    const customer = await Customer.find(id)

    let newStatus = null
    if (customer.status === 'Activo') {
      newStatus = 'Inactivo'
    } else {
      newStatus = 'Activo'
    }

    customer.merge({
      status: newStatus
    })

    await customer.save()

    const customerUpdated = await Customer.query().where('id', customer.id)
      .with('orders').first()

    return customerUpdated
  }

  async validateDocument({request, response}) {
    const {type, document} = request.all();

    const customers = await Customer.query().where('type', type).andWhere('document', document).getCount()

    let isValid = false

    if (customers === 0) {
      isValid = true
    }

    //return isValid
    return response.status(200).json({
      isValid: isValid
    })
  }

  async dropdown({response}) {
    const customers = await Customer.query()
      .select('id', 'type_document', 'document', 'name', 'surname', 'balance_favor', 'discount', 'address', 'reference')
      .whereRaw('LOWER(status) = ?', ['activo'])
      .whereNull('deleted_at')
      .with('addresses')
      .with('prices')
      .orderBy('name', 'ASC')
      .fetch();

    return response.json(customers);
  }

  async getDebts({request}) {
    try {
      let cusmoter_id = request.input('customer_id')
      let debts = await Debt.query()
        .where('status', 'pendiente')
        .where('amount_pending', '>', 0)
        .where('customer_id', cusmoter_id)
        .sum('amount_pending as total_debt')

      return debts[0]

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al obtener deudas de cliente.'
      })
    }
  }

  async summary({request, response}) {
    try {
      //calcular el total de pedidos
      let customer_id = request.input('customer_id')
      let query_orders = Order.query()
      if (customer_id) {
        query_orders.where('customer_id', parseInt(customer_id))
      }
      let orders = await query_orders.whereNotIn('status', ['Anulado'])
        .count('* as total_orders')

      let total_orders = 0
      if (orders) {
        total_orders = orders[0].total_orders
      }

      //calcular los bidones no devueltos
      let query_drums = Customer.query()
      if (customer_id) {
        query_drums.where('id', parseInt(customer_id))
      }
      let drums = await query_drums.where('pending_drums', '>', 0)
        .sum('pending_drums as total_drums')
      let total_drums = 0
      if (drums) {
        total_drums = drums[0].total_drums
      }

      ////calcular el total de saldo a favor
      let query_balance = Customer.query()
      if (customer_id) {
        query_balance.where('id', parseInt(customer_id))
      }
      let balances = await query_balance.where('balance_favor', '>=', 0)
        .sum('balance_favor as total_balances')
      let total_balances = 0
      if (balances) {
        total_balances = balances[0].total_balances
      }

      let summary = {
        total_orders,
        total_drums,
        total_balances
      }

      return response.status(200).json(summary)

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al obtener resumen de cliente.'
      })
    }
  }

  async lastPurchases({request, response}) {
    try {
      let days_passed = 10;
      let lastPurchases = await Customer.lastPurchases(days_passed);

      return response.json(lastPurchases[0]);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al obtener las últimas compras de los clientes.'
      })
    }
  }
}

module.exports = CustomerController
