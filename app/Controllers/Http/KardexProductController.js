'use strict'

const Logger = use('Logger')
const Kardex = use('App/Models/KardexProduct')

class KardexProductController {

	async getByInventory({request, response}) {
		try {
			const inventory_id = parseInt(request.input('inventory_id'))
			let kardex = await Kardex.query().where('inventory_id', inventory_id)
										.orderBy('id', 'DESC')
										.fetch()

			return response.json(kardex)

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al listar kardex de producto.'
			})
		}
	}
}

module.exports = KardexProductController
