'use strict'

const Composition = use('App/Models/Composition')

class CompositionController {

	async getByProduct({params}) {
		const {id} = params
        const composition = await Composition.query()
        							.select('supplies.id as product_id', 'supplies.name as product_name', 'supplies.cost', 'compositions.quantity')
        							.innerJoin('supplies', 'compositions.supply_id', 'supplies.id')
        							.where('compositions.product_id', id)
        							.fetch()

        return composition
	}
}

module.exports = CompositionController
