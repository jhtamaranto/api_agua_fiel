'use strict'

const BankOperation = use('App/Models/BankOperation');
const Customer = use('App/Models/Customer');
const GeneralData = use('App/Models/GeneralData');
const Logger = use('Logger')
const DateAndTime = require('date-and-time');
const Database = use('Database')
const Debt = use('App/Models/Debt')
const DebtPayment = use('App/Models/DebtPayment')
const CashService = use('App/Services/CashService');
const Order = use('App/Models/Order');

class DebtController {

	constructor() {
		this.cashService = new CashService;
	}

	async pendingDebts({ request, response }) {
		try {
			let filter = request.all();
			let pendingDebts = await Debt.pendingDebts(filter);

			return pendingDebts[0]

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al listar deudas.'
			})
		}
	}

	async getByCustomer({ request, response }) {
		try {
			const customer_id = request.input('customer_id');

			let debts = await Debt.query()
				.select([
					'debts.*',
					'orders.type_document as type_document',
					'orders.serie',
					'orders.correlative'
				])
				.innerJoin('orders', 'orders.id', 'debts.order_id')
				.where('debts.customer_id', parseInt(customer_id))
				.whereIn('debts.status', ['pendiente', 'pagada'])
				.orderBy('debts.entry_date', 'desc')
				.with('payments')
				.fetch();

			return debts

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al listar deudas por cliente'
			})
		}
	}

	async registerPayment({ auth, request, response }) {
		const transaction = await Database.beginTransaction()
		try {
			let user = await auth.getUser();

			let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');

			let cash_id = request.input('cash_id');
			let debt_id = request.input('debt_id');
			let method = request.input('method');
			let debt = await Debt.query().where('id', debt_id).first();

			let payment = await DebtPayment.create({
				debt_id: debt_id,
				payment_date: currentDate,
				amount_payment: parseFloat(request.input('amount_payment')),
				observations: request.input('observations'),
				cash_id: cash_id,
				method_payment: method,
				user_id: user.id
			}, transaction);

			if (method === 'CASH') {
				await this.cashService.store('debt', transaction, 'debt', debt.order_id, payment);
			}

			//update debt
			debt.amount_paid = debt.amount_paid + payment.amount_payment
			debt.amount_pending = debt.amount_pending - payment.amount_payment

			if (debt.amount_pending == 0) {
				debt.status = 'pagada'
				debt.payment_date = currentDate;
			}
			await debt.save(transaction);

			if (method == 'TRANSFERENCIA BANCARIA' || method == 'TRANSFERENCIA' || method == 'YAPE') {
				await BankOperation.create({
					type: 'ABONO',
					amount: payment.amount_payment,
					glosa: 'AMORTIZACIÓN DE DEUDA'
				}, transaction);
			}

			await transaction.commit()

			let debts = await Debt.query()
				.where('customer_id', parseInt(debt.customer_id))
				.orderBy('entry_date', 'asc')
				.fetch();

			let filter = {
				customer_id: debt.customer_id
			}
			let debtCustomer = await Debt.pendingDebts(filter);

			return response.status(200).json({
				debts: debts,
				debtCustomer: debtCustomer[0],
			});

		} catch (error) {
			await transaction.rollback()
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al registrar pago de deudas por cliente'
			})
		}
	}
}

module.exports = DebtController
