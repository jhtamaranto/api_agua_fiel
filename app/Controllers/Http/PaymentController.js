'use strict'

const Logger = use('Logger')

const BorrowedAsset = use('App/Models/BorrowedAsset');
const Cash = use('App/Models/Cash')
const CashMovement = use('App/Models/CashMovement')
const Customer = use('App/Models/Customer')
const Database = use('Database')
const DateAndTime = require('date-and-time');
const Debt = use('App/Models/Debt');
const DetailOrder = use('App/Models/DetailOrder');
const DetailPayment = use('App/Models/DetailPayment');
const Drum = use('App/Models/Drum');
const GeneralData = use('App/Models/GeneralData');
const Inventory = use('App/Models/Inventory')
const Kardex = use('App/Models/KardexProduct')
const Mobility = use('App/Models/Mobility')
const Order = use('App/Models/Order')
const Payment = use('App/Models/Payment')
const Product = use('App/Models/Product')
const Storehouse = use('App/Models/Storehouse')
const CashService = use('App/Services/CashService');
const InvoiceService = use('App/Services/InvoiceService');
const Voucher = use('App/Models/Voucher');

class PaymentController {

  constructor() {
    this.cashService = new CashService;
    this.invoiceService = new InvoiceService;
  }

  async register({auth, request, response}) {
    const transaction = await Database.beginTransaction()

    try {
      let user = await auth.getUser();
      let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');

      const order_id = request.input('order_id')

      const order = await Order.find(order_id)
      const customer = await Customer.find(order.customer_id)

      const discount = request.input('discount')
      const subtotal = request.input('subtotal')
      const igv = request.input('igv')
      const total = request.input('total')
      const positive_balance = request.input('positive_balance')
      const balance_favor = request.input('balance_favor')
      const total_debt = request.input('total_debt')
      const amount_change = request.input('amount_change')
      const save_change = request.input('save_change')
      const quantity_drums = request.input('quantity_drums')
      const amount_balance = request.input('amount_balance')
      const amount_debt = request.input('amount_debt')
      const save_debt = request.input('save_debt')
      const drums = request.input('drums');

      const type_document_sale = request.input('type_document_sale');
      const guide_number = request.input('guide_number');

      const type_sale = request.input('type_sale');
      const due_date = request.input('due_date');
      const credit_sales_guide = request.input('credit_sales_guide');
      const attention_guide = request.input('attention_guide');
      const generate_voucher = request.input('generate_voucher');
      const loan_guide = request.input('loan_guide');

      let observation = request.input('observation');
      if (observation) {
        observation = observation.toUpperCase();
      }

      //metodos de pago
      const amount_cash = request.input('amount_cash');
      const amount_transfer = request.input('amount_transfer');
      const amount_yape = request.input('amount_yape');

      //obtener la caja actual
      let cashCurrent = await Cash.query().whereRaw('DATE(register_date) = CURRENT_DATE')
        .where('status', 'abierta')
        .orderBy('created_at', 'DESC')
        .first();

      const payment = await Payment.create({
        order_id,
        registration_date: currentDate,
        discount,
        subtotal,
        igv,
        total,
        balance_favor: amount_balance,
        total_debt,
        amount_change,
        save_change,
        quantity_drums,
        cash_id: cashCurrent.id,
        user_id: user.id
      }, transaction)

      if (positive_balance) {
        customer.balance_favor = parseFloat(customer.balance_favor) - parseFloat(amount_balance)
      }

      if (save_change) {
        customer.balance_favor = parseFloat(customer.balance_favor) + parseFloat(amount_change)

        await CashMovement.create({
          cash_id: cashCurrent.id,
          register_date: currentDate,
          type: 'balance_favor',
          amount: parseFloat(amount_change),
          description: 'Vuelto como saldo a favor.',
          user_id: user.id
        }, transaction)
      }

      if (amount_cash > 0) {
        let change = 0
        if (parseFloat(amount_change) > 0) {
          change = parseFloat(amount_change)
        }
        let detailPayment = await DetailPayment.create({
          payment_id: payment.id,
          payment_method: 'CASH',
          payment_amount: parseFloat(amount_cash) - change,
          amount_change: change,
          cash_id: cashCurrent.id,
          user_id: user.id,
          confirmed: true
        }, transaction);

        await this.cashService.store('order', transaction, 'cash', order.id, detailPayment);
      }

      if (amount_transfer > 0) {
        let detailPayment = await DetailPayment.create({
          payment_id: payment.id,
          payment_method: 'TRANSFERENCIA',
          payment_amount: amount_transfer,
          cash_id: cashCurrent.id,
          user_id: user.id,
          confirmed: false
        }, transaction);

        await this.cashService.store('order', transaction, 'card', order.id, detailPayment);
      }

      if (amount_yape > 0) {
        let detailPayment = await DetailPayment.create({
          payment_id: payment.id,
          payment_method: 'YAPE',
          payment_amount: amount_yape,
          cash_id: cashCurrent.id,
          user_id: user.id,
          confirmed: false
        }, transaction);

        await this.cashService.store('order', transaction, 'card', order.id, detailPayment);
      }

      let last_status = order.status
      order.status = 'Facturado' // 'Pagado'
      order.delivery_time = currentDate
      if (order.final_deliveryman == null) {
        order.final_deliveryman = order.deliveryman
      }
      order.user_id = user.id;
      order.type_sale = type_sale;
      order.credit_sales_guide = credit_sales_guide;
      order.attention_guide = attention_guide;
      order.loan_guide = loan_guide;
      order.observation = observation;
      await order.save(transaction);

      if (last_status != 'Entregado') {
        //seleccionar almacen
        let storehouse = null
        let kardexDescription = 'Entrega de pedido'

        if (order.direct_sale) {
          kardexDescription = 'Venta directa'
          //almacen principal
          storehouse = await Storehouse.query().where('main', true).where('active', true).first();
        } else {
          //almacen de und movil
          let mobility = await Mobility.query()
            .where('user_id', order.final_deliveryman)
            .first();

          storehouse = await Storehouse.query()
            .where('mobility_id', mobility.id)
            .where('active', true)
            .first();
        }

        let products = await DetailOrder.query().where('order_id', order.id).fetch();

        for (let index in products.rows) {
          let product = products.rows[index];
          let quantity = product.quantity;
          let type = product.type_product;

          //Registrar descuento en inventario de und movil
          let inventory = await Inventory.query()
            .where('storehouse_id', storehouse.id)
            .where('product_id', product.product_id)
            .where('type', type)
            .first();

          let new_stock_previous = inventory.stock
          let new_stock = inventory.stock - quantity
          inventory.merge({
            stock_previous: new_stock_previous,
            stock: new_stock
          });
          await inventory.save();

          //Registrar salida en kardex de und mobil
          await Kardex.create({
            inventory_id: inventory.id,
            register_date: currentDate,
            description: kardexDescription,
            type: 'SALIDA',
            subtype: 'VENTA',
            quantity: quantity,
            stock: inventory.stock
          }, transaction);

          if (order.direct_sale) {
            if (type === 'producto') {
              let itemProduct = await Product.query()
                .where('id', product.product_id)
                .first();

              itemProduct.merge({
                stock: new_stock
              })
              await itemProduct.save(transaction);
            }
          }

          if (type === 'activo') {
            let borrowedAsset = await BorrowedAsset.query()
              .where('asset_id', product.product_id)
              .where('customer_id', order.customer_id)
              .first();

            if (borrowedAsset) {
              if (parseInt(borrowedAsset.quantity) == 0) {
                borrowedAsset.created_at = currentDate;
              }
              borrowedAsset.quantity = borrowedAsset.quantity + product.quantity;
              borrowedAsset.status = 'prestado';
              await borrowedAsset.save(transaction);
            } else {
              await BorrowedAsset.create({
                asset_id: product.product_id,
                name: product.name,
                customer_id: order.customer_id,
                status: 'prestado',
                order_id: order.id,
                quantity: product.quantity
              }, transaction);
            }
          }

          if (product.has_return == 1 && type === 'producto') {
            //obtener cantidad de bidones devueltos para el producto
            let refund_amount = 0;
            for (let itemRefund of drums) {
              let valueIds = itemRefund.product_id.split('-');
              let item_type = valueIds[0];
              let item_id = valueIds[1];
              if (type === 'producto') {
                if (item_type === 'p') {
                  if (parseInt(product.product_id) === parseInt(item_id)) {
                    refund_amount = refund_amount + parseInt(itemRefund.quantity);
                  }
                }
              }
            }
            let borrowed_quantity = product.quantity - refund_amount;
            if (borrowed_quantity > 0) {
              let register_date = DateAndTime.format(new Date(), 'YYYY-MM-DD');
              await Drum.create({
                order_id: order.id,
                product_id: product.product_id,
                brand: 'CUBICA',
                quantity: borrowed_quantity,
                customer_id: order.customer_id,
                status: 'prestado',
                loan_guide: loan_guide,
                register_date: register_date
              }, transaction);
            }
          }
        }

      }

      //Agregar los bidones
      if (drums) {
        if (drums.length > 0) {
          for (let drum of drums) {
            let drumSaved = null;

            let valueIds = drum.product_id.split('-');
            let type = null;

            let brand_drum = drum.brand;
            if (brand_drum) {
              brand_drum = brand_drum.toUpperCase();
            }
            if (valueIds[0] === 'p') {
              type = 'producto';
              let register_date = DateAndTime.format(new Date(), 'YYYY-MM-DD');
              drumSaved = await Drum.create({
                order_id: order.id,
                product_id: valueIds[1],
                brand: brand_drum,
                quantity: parseInt(drum.quantity),
                customer_id: order.customer_id,
                status: 'devuelto',
                register_date: register_date,
                confirmed: 'no'
              }, transaction);

            } else if (valueIds[0] === 'a') {
              type = 'activo';
              drumSaved = {
                product_id: valueIds[1],
                brand: drum.brand,
                quantity: parseInt(drum.quantity)
              }
            }

            if (type == 'producto') {
              customer.drums = parseInt(customer.drums) + parseInt(drum.quantity);
              customer.pending_drums = parseInt(customer.pending_drums) - parseInt(drum.quantity);
            }

            if (type == 'activo') {
              let borrowedAsset = await BorrowedAsset.query()
                .where('asset_id', drumSaved.product_id)
                .where('customer_id', order.customer_id)
                .first();

              if (borrowedAsset) {
                borrowedAsset.quantity = parseInt(borrowedAsset.quantity) - parseInt(drumSaved.quantity);
                if (borrowedAsset.quantity == 0) {
                  borrowedAsset.status = 'devuelto';
                }
                await borrowedAsset.save(transaction);
              }
            }
          }
        }
      }
      await customer.save(transaction);

      if (amount_debt > 0) {
        if (type_sale == 'credito') {
          await Debt.create({
            customer_id: customer.id,
            entry_date: currentDate,
            amount: amount_debt,
            amount_pending: amount_debt,
            order_id: order.id,
            due_date: due_date,
            guide_number: guide_number,
            credit_sales_guide: credit_sales_guide,
            attention_guide: attention_guide
          }, transaction);

          order.guide_number = guide_number;
          await order.save(transaction);
        }
      }

      //datos factura, boleta, o ticket
      let send_to_senda = false;
      if (type_sale == 'contado' || generate_voucher == true) {
        if (type_document_sale == 'BOLETA' || type_document_sale == 'FACTURA') {
          send_to_senda = true;
        }

        let voucher = await Voucher.query()
          .where('type', type_document_sale)
          .first();

        await Voucher.query()
          .where('type', type_document_sale)
          .increment('current_number', 1);

        let serie = '';
        let correlative = '';
        if (voucher) {
          serie = voucher.correlative;
          let stringCorrelative = voucher.current_number.toString();
          correlative = stringCorrelative.padStart(8, '0');
        }
        order.date_invoice = order.entry_time;
        order.type_document = type_document_sale;
        order.serie = serie;
        order.correlative = correlative;
        await order.save(transaction);
      }

      await transaction.commit();

      let enterprise = await GeneralData.find(1);

      if (send_to_senda === true) {
        if (enterprise) {
          if (enterprise.user_fe && enterprise.password_fe) {
            let invoiceSend = await this.invoiceService.sendInvoiceToSenda(order.id);
            if (invoiceSend == '0000-CPE ha sido aceptada') {
              order.send_fe = true;
              await order.save();
            }
          }
        }
      }

      const orderUpdated = await Order.query()
        .where('id', order_id)
        .with('customer')
        .with('deliverystart')
        .with('deliveryend')
        .with('products')
        .with('user')
        .with('detail')
        .first();

      return response.json(orderUpdated);

    } catch (error) {
      await transaction.rollback()
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al registrar pago.'
      })
    }
  }

}

module.exports = PaymentController
