'use strict'

const Logger = use('Logger')

const User = use('App/Models/User')
const Asignacion=use('App/Models/Asignacion')


class AsignacionController {

  async findAllClientes({request,response}){
    try{
      const zona=request.input('zona')
      const prevendedor=request.input('prevendedor')
      let result
      if (zona!=null && prevendedor!=null){
        result= await Asignacion.find()
      }

      return result[0]
    }
    catch (error) {
      console.log(error)
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'error al buscar clientes',
        name:error.name,
        description:error.message
      })
    }
  }

  async findAllPrevendedores({request,response}){
    try{
      let result= await User.getPrevendedores()
      return result[0]
    }
    catch (error) {
      console.log(error)
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'error al buscar prevendedores',
        name:error.name,
        description:error.message
      })
    }
  }
}

module.exports = AsignacionController
