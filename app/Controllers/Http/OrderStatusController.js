'use strict'

const Logger = use('Logger')

const OrderStatus = use('App/Models/OrderStatus')

class OrderStatusController {

	async dropdown({response}) {
		const status = await OrderStatus.query().select('id as value', 'name as text').where('status', 'activo').fetch()

		return status
	}

	async index({response}) {
		try{
			const status = await OrderStatus.query().where('status', 'activo').fetch()

			return status

		} catch(error){
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al listar estados de ordenes.'
			})
		}
	}

}

module.exports = OrderStatusController
