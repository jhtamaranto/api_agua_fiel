
'use strict'

const Logger = use('Logger')

const Role = use('App/Models/Role')
const PermissionRole = use('App/Models/PermissionRole')

class RoleController {

	async index({response}) {
		try {
			const roles = await Role.query()
									.where('superadmin', false)
									.with('permissions')
									.orderBy('name', 'asc').fetch()

			return roles
		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener lista de roles.'
			})
		}
	}

	async store ({request, response}) {
		const {name, description, selected} = request.only(['name', 'description', 'selected'])

		try {
			let role_name = name.toUpperCase()
			let slug = name.toLowerCase().split(' ').join('_')

			const role = await Role.create({
				name: role_name,
				slug,
				description
			})

			if(selected.length > 0) {
				for (let i = 0; i < selected.length; i++) {
					await PermissionRole.create({
						permission_id: selected[i],
						role_id: role.id
					})
				}
			}

			const roleCreated = await Role.query().with('permissions').where('id', role.id).first()

			return roleCreated

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al intentar registrar rol.'
			})
		}
	}

	async update({params, request}) {
		const {id} = params
		const {name, description, selected} = request.only(['name', 'description', 'selected'])
		try{

			const role = await Role.find(id)

			let slug = name.toLowerCase().split(' ').join('_')

			const data = {
				name,
				description,
				slug
			}

			role.merge(data)

			await role.save()

			await PermissionRole.query().where('role_id', role.id).delete()
			if(selected.length > 0) {
				for (let i = 0; i < selected.length; i++) {
					await PermissionRole.create({
						permission_id: selected[i],
						role_id: role.id
					})
				}
			}

			const roleUpdated = await Role.query().with('permissions').where('id', role.id).first()

			return roleUpdated

		} catch(error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al intentar actualizar rol.'
			})
		}
	}

	async inactivate({params, response}) {
		const {id} = params

		try{
			const role = await Role.find(id)

			let newStatus = null
	        if (role.status === 'activo') {
	            newStatus = 'inactivo'
	        } else{
	            newStatus = 'activo'
	        }

	        role.merge({
	            status: newStatus
	        })

	        await role.save()

	        const roleUpdated = await Role.query().with('permissions').where('id', role.id).first()

			return roleUpdated

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al inactivar rol de usuario.'
			})
		}
	}
}

module.exports = RoleController
