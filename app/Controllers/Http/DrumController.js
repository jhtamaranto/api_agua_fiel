'use strict'

const Composition = use('App/Models/Composition');
const Database = use('Database');
const DateAndTime = require('date-and-time');
const Drum = use('App/Models/Drum');
const Inventory = use('App/Models/Inventory');
const Kardex = use('App/Models/KardexProduct');
const KardexSupply = use('App/Models/KardexSupply');
const Logger = use('Logger');
const Storehouse = use('App/Models/Storehouse');
const Supply = use('App/Models/Supply');

class DrumController {

  async brands ({request, response}) {
    try {
      let brands = await Drum.query()
        .distinct('brand')
        .pluck('brand');

      return response.json(brands);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al listar marcas de bidones.'
      })
    }
  }

  async index ({request, response}) {
    try {

      const parameters = request.all();

      let query = Drum.query()
        .select([
          'drums.id',
          'customers.name as customer_name',
          'customers.surname as customer_surname',
          'customers.document as customer_document',
          'drums.register_date',
          'drums.product_id as product_id',
          'products.name as product_name',
          'drums.brand as product_brand',
          'drums.quantity',
          'drums.status as drum_status',
          'drums.loan_guide',
          'drums.observation',
          'drums.confirmed',
        ]).innerJoin('customers', 'customers.id', 'drums.customer_id')
        .innerJoin('products', 'products.id', 'drums.product_id');

      if (parameters.customer_id) {
        query.where('drums.customer_id', parameters.customer_id);
      }

      if (parameters.product_id) {
        query.where('drums.product_id', parameters.product_id);
      }

      if (parameters.date_from) {
        query.where('drums.register_date', '>=', parameters.date_from);
      }

      if (parameters.date_until) {
        query.where('drums.register_date', '<=', parameters.date_until);
      }

      let drums = await query.orderBy('drums.register_date', 'DESC').fetch();

      return response.json(drums);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al listar movimientos de bidones'
      })
    }
  }

  async storeLoan ({request, response}) {
    try {
      let register_date = request.input('register_date');
      let customer_id = request.input('customer_id');
      let product_id = request.input('product_id');
      let quantity = request.input('quantity');
      let loan_guide = request.input('loan_guide');

      if (!loan_guide) {
        loan_guide = '';
      }

      let drum = await Drum.create({
        product_id: product_id,
        brand: 'CUBICA',
        quantity: quantity,
        customer_id: customer_id,
        status: 'prestado',
        loan_guide: loan_guide,
        register_date: register_date
      });

      return response.json(drum);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al registrar préstamo'
      });
    }
  }

  async storeReturn ({request, response}) {
    const transaction = await Database.beginTransaction();
    try {
      let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');

      let register_date = request.input('register_date');
      let customer_id = request.input('customer_id');
      let product_id = request.input('product_id');
      let brand = request.input('brand');
      let quantity = request.input('quantity');
      let observation = request.input('observation');

      if (brand) {
        brand = brand.toUpperCase();
      }

      if (observation) {
        observation = observation.toUpperCase();
      }

      let drum = await Drum.create({
        product_id: product_id,
        brand: brand,
        quantity: quantity,
        customer_id: customer_id,
        status: 'devuelto',
        register_date: register_date,
        observation: observation,
        confirmed: 'no'
      }, transaction);

      let storehouse = await Storehouse.query()
        .where('main', true)
        .where('active', true)
        .first();

      if (storehouse) {
        if (brand === 'CÚBICA' || brand === 'CUBICA') {
          let inventory = await Inventory.query()
            .where('storehouse_id', storehouse.id)
            .where('product_id', product_id)
            .where('type', 'producto')
            .first();

          if (inventory) {
            let new_stock_empty = inventory.stock_empty + parseInt(quantity);
            inventory.merge({
              stock_empty: new_stock_empty
            });
            await inventory.save(transaction);

            await Kardex.create({
              inventory_id: inventory.id,
              register_date: currentDate,
              description: 'RETORNO',
              type: 'INGRESO',
              subtype: 'RETORNO',
              quantity: quantity,
              stock: inventory.stock_empty
            }, transaction);
          }
        }
      }

      await transaction.commit();

      return response.json(drum);

    } catch (error) {
      await transaction.rollback();
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al registrar devolución'
      });
    }
  }

  async deleteLoanReturn ({params, response}) {
    const transaction = await Database.beginTransaction();
    try {
      let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');

      let drum_id = params.id;
      let drumData = await Drum.find(drum_id);

      await Drum.query()
        .where('id', drum_id)
        .delete(transaction);

      if (drumData.status === 'devuelto') {
        if (drumData.confirmed === 'si') {
          let components = await Composition.query()
            .where('product_id', drumData.product_id)
            .fetch();

          for (let element of components.rows) {
            let supply = await Supply.find(element.supply_id);

            if (supply) {
              if (supply.has_return === 'si') {
                if (drumData.brand === 'CUBICA') {
                  supply.stock = parseFloat(supply.stock) - parseFloat(drumData.quantity);
                  await supply.save(transaction);

                  await KardexSupply.create({
                    supply_id: supply.id,
                    operation_date: currentDate,
                    type: 'SALIDA',
                    sub_type: 'DEVOLUCIÓN ANULADA',
                    quantity: drumData.quantity,
                    stock: supply.stock,
                    cost: supply.cost,
                    total: drumData.quantity * supply.cost
                  }, transaction);
                } else {
                  let name_supply = supply.name + '-' + drumData.brand;
                  let supplyData = await Supply.query()
                    .where('name', name_supply)
                    .where('brand', drumData.brand)
                    .first();

                  if (supplyData) {
                    supplyData.stock = parseFloat(supplyData.stock) - parseFloat(drumData.quantity);
                    await supplyData.save(transaction);

                    await KardexSupply.create({
                      supply_id: supplyData.id,
                      operation_date: currentDate,
                      type: 'SALIDA',
                      sub_type: 'DEVOLUCIÓN ANULADA',
                      quantity: drumData.quantity,
                      stock: supplyData.stock,
                      cost: supplyData.cost,
                      total: drumData.quantity * supplyData.cost
                    }, transaction);
                  }
                }
              }
            }
          }
        }
      }

      await transaction.commit();

      return response.json(drumData);

    } catch (error) {
      await transaction.rollback();
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al eliminar prestamo o devolución'
      });
    }
  }

  async confirmReturn ({request, response}) {
    try {
      let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');

      let drum_id = request.input('drum_id');
      let product_id = request.input('product_id');
      let brand = request.input('brand_name');
      let quantity = request.input('quantity');

      //obtener composision de producto
      let components = await Composition.query()
        .where('product_id', product_id)
        .fetch();

      for (let element of components.rows) {
        let supply = await Supply.find(element.supply_id);

        if (supply) {
          if (supply.has_return === 'si') {
            if (brand === 'CUBICA') {
              let supplyKardex = await KardexSupply.create({
                supply_id: supply.id,
                operation_date: currentDate,
                type: 'INGRESO',
                sub_type: 'RETORNO',
                quantity: parseFloat(quantity),
                stock: parseFloat(supply.stock) + parseFloat(quantity),
                cost: supply.cost,
                total: parseFloat(quantity) * supply.cost
              });

              supply.stock = parseFloat(supply.stock) + parseFloat(supplyKardex.quantity);
              await supply.save();
            } else {
              let name_supply = supply.name + '-' + brand;
              let supplyData = await Supply.query()
                .where('name', name_supply)
                .where('brand', brand)
                .first();

              if (!supplyData) {
                supplyData = await Supply.create({
                  name: name_supply,
                  unit_id: supply.unit_id,
                  cost: supply.cost,
                  has_return: supply.has_return,
                  brand: brand,
                  stock: 0
                });
              }

              let supplyKardex = await KardexSupply.create({
                supply_id: supplyData.id,
                operation_date: currentDate,
                type: 'INGRESO',
                sub_type: 'RETORNO',
                quantity: parseFloat(quantity),
                stock: parseFloat(supplyData.stock) + parseFloat(quantity),
                cost: supplyData.cost,
                total: parseFloat(quantity) * supplyData.cost
              });

              supplyData.stock = parseFloat(supplyData.stock) + parseFloat(supplyKardex.quantity);
              await supplyData.save();
            }
          }
        }
      }

      let drumData = await Drum.find(drum_id);
      drumData.confirmed = 'si';
      drumData.save();

      return response.json({
        message: "Devolución fue confirmada con éxito"
      });

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al confirmar devolución'
      });
    }
  }
}

module.exports = DrumController
