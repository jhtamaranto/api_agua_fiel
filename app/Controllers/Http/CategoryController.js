'use strict'

const Category =  use('App/Models/Category')

class CategoryController {

	async index({response}) {
		const categories = await Category.query().where('status', 'activo').fetch()

		return categories
	}

	async dropdown({response}) {
		const categories = await Category.query().select('id as value', 'name as text').where('status', 'activo').fetch()

		return categories
	}
}

module.exports = CategoryController
