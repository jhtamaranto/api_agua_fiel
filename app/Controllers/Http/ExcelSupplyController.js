'use strict'

const Excel = require('exceljs');
const Logger = use('Logger');
const Supply = use('App/Models/Supply');

class ExcelSupplyController {

  async index ({request, response}) {
    try {

      let supplies = await Supply.query()
        .select(
          'supplies.*',
          'units.name as unit_name'
        ).innerJoin('units', 'units.id', 'supplies.unit_id')
        .orderBy('supplies.name', 'asc')
        .fetch();

      //---------------------------------------------------------------------
      const workbook = new Excel.Workbook();
      const sheet = workbook.addWorksheet('Worksheet');

      let indexRow = 1;
      let row = sheet.getRow(indexRow);
      //---- Titulos
      const font = {bold: true};

      row.getCell('A').value = 'INSUMO';
      row.getCell('B').value = 'UND. DE MEDIDA';
      row.getCell('C').value = 'MARCA';
      row.getCell('D').value = 'STOCK';
      row.getCell('E').value = 'VALORIZADO';

      sheet.getColumn('A').width = 35;
      sheet.getColumn('B').width = 18;
      sheet.getColumn('C').width = 33;
      sheet.getColumn('D').width = 14;
      sheet.getColumn('E').width = 15;

      row.getCell('A').font = font;
      row.getCell('B').font = font;
      row.getCell('C').font = font;
      row.getCell('D').font = font;
      row.getCell('E').font = font;

      const border = {
        bottom: {style: 'medium'}
      };

      row.getCell('A').border = border;
      row.getCell('B').border = border;
      row.getCell('C').border = border;
      row.getCell('D').border = border;
      row.getCell('E').border = border;

      row.commit();

      for (let item of supplies.rows) {
        indexRow++;
        row = sheet.getRow(indexRow);

        let unit_name = '';
        if (item.unit_name) {
          unit_name = item.unit_name;
        }

        let brand = '';
        if (item.brand) {
          brand = item.brand;
        }
        let stock = 0;
        let cost = 0;
        if (item.stock) {
          stock = parseFloat(item.stock);
        }
        if (item.cost) {
          cost = parseFloat(item.cost);
        }

        row.getCell('A').value = item.name;
        row.getCell('B').value = unit_name;
        row.getCell('C').value = brand;
        row.getCell('D').value = stock;
        row.getCell('E').value = stock * cost;
        row.getCell('E').numFmt = '0.00';

        row.commit();

      }

      response.header(`Content-Type`, `application/vnd.openxmlformats-officedocument.spreadsheetml.sheet`)
      response.header(`Content-Disposition`, `attachment; filename="template.xlsx`)

      return workbook.xlsx.write(response.response);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al descargar reporte.'
      })
    }
  }
}

module.exports = ExcelSupplyController
