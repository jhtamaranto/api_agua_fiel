'use strict'

const Customer = use('App/Models/Customer');
const DetailPayment = use('App/Models/DetailPayment');
const DateAndTime = require('date-and-time');
const GeneralData = use('App/Models/GeneralData');
const Logger = use('Logger');
const Order = use('App/Models/Order');
const Payment = use('App/Models/Payment');
const Voucher = use('App/Models/Voucher');
const InvoiceService = use('App/Services/InvoiceService');

class DetailPaymentController {

  constructor() {
    this.invoiceService = new InvoiceService;
  }

  async index ({request, response}) {
    try {
      const parameters = request.all();

      let query = DetailPayment.query()
        .select([
          'detail_payments.id',
          'detail_payments.payment_method',
          'detail_payments.payment_amount',
          'detail_payments.confirmed',
          'detail_payments.created_at as payment_date',
          'orders.type_document',
          'orders.serie',
          'orders.correlative',
          'customers.name as customer_name',
          'customers.surname as customer_surname',
          'customers.document as customer_document',
          'customers.type_document as customer_type_document',
          'users.first_name as user_name',
          'users.last_name as user_surname'
        ]).innerJoin('payments', 'payments.id', 'detail_payments.payment_id')
        .innerJoin('orders', 'orders.id', 'payments.order_id')
        .innerJoin('customers', 'customers.id', 'orders.customer_id')
        .leftJoin('users', 'users.id', 'orders.final_deliveryman');

      if (parameters.customer_id) {
        query.where('orders.customer_id', parameters.customer_id);
      }

      if (parameters.payment_method) {
        query.where('detail_payments.payment_method', parameters.payment_method);
      }

      if (parameters.status) {
        let status_string = parameters.status;
        if (status_string === 'CONFIRMADO') {
          query.where('detail_payments.confirmed', true);
        } else {
          query.where('detail_payments.confirmed', 0);
        }
      }

      if (parameters.date_from) {
        query.whereRaw('DATE(detail_payments.created_at) >= ?', [parameters.date_from]);
      }

      if (parameters.date_until) {
        query.whereRaw('DATE(detail_payments.created_at) <= ?', [parameters.date_until]);
      }

      let payments = await query.orderBy('detail_payments.created_at', 'DESC').fetch();

      return response.json(payments);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al listar pagos.'
      });
    }
  }

  async storeConfirmation ({request, response}) {

    try {
      let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');

      let detail_id = request.input('detail_id');
      let detailPayment = await DetailPayment.find(detail_id);

      let payment = await Payment.find(detailPayment.payment_id);
      let order = await Order.find(payment.order_id);
      let customer = await Customer.find(order.customer_id);

      detailPayment.confirmed = true;
      await detailPayment.save();

      let generate_voucher = false;
      let printer = 'no';
      if (order.type_document === 'TICKET') {
        let type_document = 'FACTURA';
        if (customer.type_document === 'DNI') {
          type_document = 'BOLETA';
        }

        let voucher = await Voucher.query()
          .where('type', type_document)
          .first();

        await Voucher.query()
          .where('type', type_document)
          .increment('current_number', 1);

        let serie = '';
        let correlative = '';
        if (voucher) {
          printer = 'si';
          generate_voucher = true;
          serie = voucher.correlative;
          let stringCorrelative = voucher.current_number.toString()
          correlative = stringCorrelative.padStart(8, '0');

          order.date_invoice = currentDate;
          order.type_document_old = order.type_document;
          order.serie_old = order.serie;
          order.correlative_old = order.correlative;

          order.type_document = type_document;
          order.serie = serie;
          order.correlative = correlative;
          await order.save();
        }
      }

      if (generate_voucher === true) {
        let enterprise = await GeneralData.find(1);

        if (enterprise) {
          if (enterprise.user_fe && enterprise.password_fe) {
            let invoiceSend = await this.invoiceService.sendInvoiceToSenda(order.id);
            if (invoiceSend === '0000-CPE ha sido aceptada') {
              order.send_fe = true;
              await order.save();
            }
          }
        }
      }

      let orderData = null;
      if (printer === 'si') {
        orderData = await Order.query()
          .where('id', order.id)
          .with('customer')
          .with('deliverystart')
          .with('deliveryend')
          .with('products')
          .with('user')
          .with('detail')
          .first();
      }
      return response.json({
        message: "Confirmación realizada de forma exitosa",
        printer: printer,
        invoice: orderData
      });

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al confirmar pago.'
      });
    }
  }
}

module.exports = DetailPaymentController
