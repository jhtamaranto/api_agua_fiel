'use strict'

const Logger = use('Logger');
const Customer = use('App/Models/Customer');
const CustomerPrice = use('App/Models/CustomerPrice');

class CustomerPriceController {

  async getPriceCustomerProduct({request, response}){
    try {
      let customer_id = request.input('customer_id');
      let product_id = request.input('product_id');
      let prices = await CustomerPrice.query()
        .where('customer_id', customer_id)
        .where('product_id', product_id)
        .first();

      return response.json(prices);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      console.log(error)
      return response.status(400).json({
        message: 'Se detectó un error al obtener precios del cliente.'
      })
    }
  }
  async index ({request, response}) {
    try {
      let customer_id = request.input('customer_id');

      let prices = await CustomerPrice.query()
        .where('customer_id', customer_id)
        .with('product')
        .orderBy('product_id')
        .fetch();

      return response.json(prices);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al obtener precios del cliente.'
      })
    }
  }

  async store ({request, response}) {
    try {
      let customer_id = request.input('customer_id');
      let product_id = request.input('product_id');
      let price = request.input('price');

      await CustomerPrice.create({
        customer_id: customer_id,
        product_id: product_id,
        price: price
      });

      return response.json({
        message: 'Precio fue registrado de forma correcta'
      })

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al registrar precios del cliente.'
      });
    }
  }

  async update ({params, request, response}) {
    try {
      let customerPrice = await CustomerPrice.find(params.id);

      let customer_id = request.input('customer_id');
      let product_id = request.input('product_id');
      let price = request.input('price');

      customerPrice.merge({
        product_id: product_id,
        price: price
      });

      await customerPrice.save();

      return response.json({
        'message': 'Precio fue actualizado de forma correcta'
      });

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al actualizar precio del cliente.'
      });
    }
  }

  async destroy ({params, response}) {
    try {
      let customerPrice = await CustomerPrice.find(params.id);

      await customerPrice.delete();

      return response.json({
        'message': 'Precio fue eliminado de forma correcta'
      });

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al eliminar precio del cliente.'
      });
    }
  }
}

module.exports = CustomerPriceController
