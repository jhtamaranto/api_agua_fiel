'use strict'

const Logger = use('Logger')

const Composition = use('App/Models/Composition')
const Database = use('Database')
const DateAndTime = require('date-and-time')
const DetailOrder = use('App/Models/DetailOrder')
const Inventory = use('App/Models/Inventory')
const Order = use('App/Models/Order')
const Product = use('App/Models/Product')
const Storehouse = use('App/Models/Storehouse')

class ProductController {

	async suppliesDropDown({response}) {
		const supplies = await Product.query().select('id as value', 'name as text').where('status', 'activo').andWhere('supply', 1).fetch()

		return supplies
	}

	async supplies({response}) {
		const supplies = await Product.query().where('status', 'activo').andWhere('supply', 1).fetch()

		return supplies
	}

	async index({response}) {
	  try {
	    let products = await Product.query()
        .whereNull('deleted_at')
        .orderBy('name')
        .fetch();

	    return response.json(products);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al listar productos.'
      })
    }
	}

	async store({request, response}) {
		const transaction = await Database.beginTransaction()
		try {

		  let name = request.input('name').toUpperCase();

			const product = await Product.create({
				category_id: request.input('category_id'),
				unit_id: request.input('unit_id'),
				name: name,
				cost: request.input('cost'),
				unit_price: request.input('unit_price'),
				wholesale_price: request.input('wholesale_price'),
				preferential_price: request.input('preferential_price'),
				compound: request.input('compound'),
				check_inventory: request.input('check_inventory'),
				stock: request.input('stock'),
				minimum_stock: request.input('minimum_stock'),
				has_return: request.input('has_return'),
				status: 'activo'
			}, transaction)

			if (product.id > 0) {
				if (product.compound === true) {
					const supplies = request.input('supplies')
					for(let i=0; i < supplies.length; i++) {
						await Composition.create({
							product_id: product.id,
							supply_id: supplies[i].product_id,
							quantity: supplies[i].quantity,
							cost: supplies[i].quantity * supplies[i].cost
						}, transaction)
					}
				}
			}

			//Registrar producto en inventario de almacen principal
			let storehouse =await Storehouse.query().where('main', true).first()

			let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss')

			await Inventory.create({
        storehouse_id: storehouse.id,
        product_id: product.id,
        date_entry: currentDate,
        stock: 0,
        stock_previous: 0,
        type: 'producto'
      }, transaction)

			await transaction.commit()

			const newProduct = await Product.find(product.id)

			return response.json(newProduct);

		} catch (error) {
			await transaction.rollback()
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al registrar producto.'
			})
		}

	}

	async update({params, request, response}) {
		const transaction = await Database.beginTransaction()

		try {

			const {id} = params
      const product = await Product.find(id);
			let name = request.input('name').toUpperCase();

      const data = {
			  category_id: request.input('category_id'),
				unit_id: request.input('unit_id'),
				name: name,
				cost: request.input('cost'),
				unit_price: request.input('unit_price'),
				wholesale_price: request.input('wholesale_price'),
				preferential_price: request.input('preferential_price'),
				compound: request.input('compound'),
				check_inventory: request.input('check_inventory'),
				stock: request.input('stock'),
				minimum_stock: request.input('minimum_stock'),
				has_return: request.input('has_return')
      }

      product.merge(data)

      await product.save(transaction)

      if (product.compound === true) {
        await Composition.query().where('product_id', product.id).delete(transaction)

        const supplies = request.input('supplies')
				for(let i=0; i < supplies.length; i++) {
					await Composition.create({
						product_id: product.id,
						supply_id: supplies[i].product_id,
						quantity: supplies[i].quantity,
						cost: supplies[i].quantity * supplies[i].cost
					}, transaction)
				}
      }

      //Verificar si esta registrado en inventario del almacén principal
      let storehouse =await Storehouse.query().where('main', true).first()

      let inventory = await Inventory.query().where('storehouse_id', storehouse.id)
        .where('product_id', product.id)
        .where('type', 'producto')
        .first()

      let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss')

      if(!inventory) {
        await Inventory.create({
          storehouse_id: storehouse.id,
          product_id: product.id,
          date_entry: currentDate,
          stock: 0,
          stock_previous: 0,
          type: 'producto'
        }, transaction)
      }

      await transaction.commit()

      return response.json(product);

		} catch (error) {
			await transaction.rollback()
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al actualizar producto.'
			})
		}
	}

	async inactivate({params}) {
		const {id} = params
		try {
			const product = await Product.find(id)

	        let newStatus = null
	        if (product.status === 'activo') {
	            newStatus = 'inactivo'
	        } else{
	            newStatus = 'activo'
	        }

	        product.merge({
	            status: newStatus
	        })

	        await product.save()

	        const productUpdated = await Product.find(product.id)

	        return product

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al cambiar estado de productos.'
			})
		}
	}

	async catalogue({response}){
		try {

      const products = await Product.catalogue();

			return products[0];

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener catalogo de productos.'
			})
		}
	}

	async summary({request, response}) {
		try {
			let product_id = request.input('product_id')
			//monto de ventas
			let sales = await DetailOrder.query()
											.summary(product_id)
											.sum('detail_orders.subtotal as total_sales')

			let total_sales = 0
			if(sales) {
				total_sales = sales[0].total_sales
			}

			//monto de ventas completo
      let sales_complete = await DetailOrder.query()
        .summary(product_id)
        .where('detail_orders.type_sale', 'completo')
        .sum('detail_orders.subtotal as total_sales');

			let total_sales_complete = 0;
			if (sales_complete[0].total_sales) {
        total_sales_complete = sales_complete[0].total_sales;
      }

      //monto de ventas recargar
      let sales_recharge = await DetailOrder.query()
        .summary(product_id)
        .where('detail_orders.type_sale', 'recarga')
        .sum('detail_orders.subtotal as total_sales');

      let total_sales_recharge = 0;
      if (sales_recharge[0].total_sales) {
        total_sales_recharge = sales_recharge[0].total_sales;
      }

			//número de ventas
			sales = await DetailOrder.query()
        .summary(product_id)
        .sum('detail_orders.quantity as quantity_sales')

			let quantity_sales = 0
			if(sales) {
				quantity_sales = sales[0].quantity_sales
			}

			//ventas por semana
			let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD')

			let sales_week = await DetailOrder.query()
        .summary(product_id)
        .whereRaw("WEEK(orders.entry_time) = WEEK('"+ currentDate +"')")
        .sum('detail_orders.subtotal as total_sales_week');

			let total_sales_week = 0
			if(sales_week[0].total_sales_week) {
				total_sales_week = sales_week[0].total_sales_week
			}

			//ventas por mes
			let sales_month = await DetailOrder.query()
        .summary(product_id)
        .whereRaw("MONTH(orders.entry_time) = MONTH('"+ currentDate +"')")
        .sum('detail_orders.subtotal as total_sales_month');

			let total_sales_month = 0
			if(sales_month[0].total_sales_month) {
				total_sales_month = sales_month[0].total_sales_month
			}

      //ventas del dia
      let sales_today = await DetailOrder.query()
        .summary(product_id)
        .whereRaw("DATE(orders.entry_time) = DATE('"+ currentDate +"')")
        .sum('detail_orders.subtotal as total_sales_today');

      let total_sales_today = 0;
      if(sales_today[0].total_sales_today) {
        total_sales_today = sales_today[0].total_sales_today;
      }

      //ventas del dia completo
      let sales_today_complete = await DetailOrder.query()
        .summary(product_id)
        .where('detail_orders.type_sale', 'completo')
        .whereRaw("DATE(orders.entry_time) = DATE('"+ currentDate +"')")
        .sum('detail_orders.subtotal as total_sales');

      let total_sales_today_complete = 0;
      if(sales_today_complete[0].total_sales) {
        total_sales_today_complete = sales_today_complete[0].total_sales;
      }

      //ventas del dia recarga
      let sales_today_recharge = await DetailOrder.query()
        .summary(product_id)
        .where('detail_orders.type_sale', 'recarga')
        .whereRaw("DATE(orders.entry_time) = DATE('"+ currentDate +"')")
        .sum('detail_orders.subtotal as total_sales');

      let total_sales_today_recharge = 0;
      if(sales_today_recharge[0].total_sales) {
        total_sales_today_recharge = sales_today_recharge[0].total_sales;
      }

			let summary = {
				total_sales,
        total_sales_complete,
        total_sales_recharge,
				quantity_sales,
				total_sales_week: total_sales_week,
				total_sales_month: total_sales_month,
        total_sales_today: total_sales_today,
        total_sales_today_complete,
        total_sales_today_recharge
			}
			return response.status(200).json(summary)

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener resumen de productos.'
			})
		}
	}

	async productHasReturn ({request, response}) {
	  try {
	    let products = await Product.query()
        .where('has_return', true)
        .orderBy('name', 'ASC')
        .fetch();

	    return response.json(products);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al listar productos.'
      })
    }
  }

  async destroy ({params, response}) {
	  try {
	    let product_id = params.id;
	    let product = await Product.find(product_id);

	    let current_date = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');
	    product.deleted_at = current_date;
	    product.save();

	    await Inventory.query()
        .where('product_id', product_id)
        .where('type', 'producto')
        .update({
          deleted_at: current_date
        });

	    return response.json({
        message: 'Producto eliminado de forma correcta'
      });

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al eliminar producto.'
      })
    }
  }

}

module.exports = ProductController
