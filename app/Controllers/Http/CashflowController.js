'use strict'

const Cash = use('App/Models/Cash')
const CashMovement = use('App/Models/CashMovement')
const DateAndTime = require('date-and-time')
const DebtPayment = use('App/Models/DebtPayment')
const DetailPayment = use('App/Models/DetailPayment')
const Logger = use('Logger')
const {calculateCashSum} = use('App/Helpers/CashTools');
const CashService = use('App/Services/CashService');

class CashflowController {

  constructor() {
    this.cashService = new CashService;
  }

	async getCurrentCash({request, response}) {
		try {
			let cash = await Cash.query().whereRaw('DATE(register_date) = CURRENT_DATE')
				.where('status', 'abierta')
				.orderBy('created_at', 'DESC')
				.first();

			return response.status(200).json(cash)

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener caja actual.'
			})
		}
	}

	async open({request, response}) {
		try {
			let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss')

			let amount_open = request.input('amount_open')

			const cash = await Cash.create({
				register_date: currentDate,
				amount_open: parseFloat(amount_open),
				status: 'abierta'
			})

			return response.status(201).json(cash)

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al abrir caja.'
			})
		}
	}

	async close({request, response}) {
		try {
			const currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss')

			const cash_id = request.input('cash_id')
			const amount_cash = request.input('amount_cash')
			const amount_cards = request.input('amount_cards')
			const amount_income = request.input('amount_income')
			const amount_expense = request.input('amount_expense')
			const total_amount = request.input('total_amount')
			const condition = request.input('condition')

			let cash = await Cash.find(parseInt(cash_id))

			const dataClose = {
				close_date: currentDate,
				amount_close_cash: parseFloat(amount_cash),
				amount_close_cards: parseFloat(amount_cards),
				amount_income: parseFloat(amount_income),
				amount_expense: parseFloat(amount_expense),
				total_amount: parseFloat(total_amount),
				condition: condition,
				status: 'cerrada'
			}
			cash.merge(dataClose)

			await cash.save()

			const cashClosed = await Cash.find(cash.id)

			return response.status(200).json(cashClosed)

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al cerrar caja.'
			})
		}
	}

	async registerOperation({auth, request, response}) {
		try {
      let user = await auth.getUser();

			let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss')

			let cash_id = request.input('cash_id')
			let type = request.input('type_operation')
			let amount = request.input('amount_operation')
			let description = request.input('description_operation')
      let delivery_man = request.input('delivery_man');

			let cashMovement = await CashMovement.create({
				cash_id: cash_id,
				register_date: currentDate,
				type: type,
				amount: parseFloat(amount),
				description: description,
        user_id: user.id,
        delivery_man: delivery_man
			});

			return response.status(201).json(cashMovement)

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al registrar operación.'
			})
		}
	}

	async getPaymentsDebts({request, response}) {
		try {
			let date_filter = request.input('date_filter')
			let cash_id = request.input('cash_id');

			let payments = await DebtPayment.paymentByDate(date_filter, cash_id)

			return response.status(200).json(payments[0])

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al listar pagos de deudas.'
			})
		}
	}

	async getPaymentsOrders({request, response}) {
		try {
			const method = request.input('method')
			let date_filter = request.input('date_filter')
			let cash_id = request.input('cash_id');

			let payments = await DetailPayment.paymentByMethod(method, date_filter, cash_id);

			return response.status(200).json(payments[0])

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al listar pagos de ordenes.'
			})
		}
	}

	async getOperations({request, response}) {
		try {
			let type = request.input('type')
			let date_filter = request.input('date_filter')
			let cash_id = request.input('cash_id');

			let operations = await CashMovement.query()
        .select(
          'cash_movements.*',
          'users.first_name',
          'users.last_name'
        ).leftJoin('users', 'users.id', 'cash_movements.delivery_man')
        .where('cash_movements.type', type)
        .andWhere('cash_movements.cash_id', cash_id)
        .whereRaw('DATE(cash_movements.register_date) = ?', [date_filter])
        .orderBy('cash_movements.register_date', 'DESC')
        .fetch();

			return response.status(200).json(operations)

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al listar operaciones de caja.'
			})
		}
	}

	async getTotalAmounts({request, response}) {
		try {
			let date_filter = request.input('date_filter');
			let cash_id = request.input('cash_id');

			let cashCurrent = await Cash.query().whereRaw('DATE(register_date) = ?', [date_filter])
				.where('status', 'abierta')
				.andWhere('id', cash_id)
				.first();

			let payments = await DebtPayment.paymentByDate(date_filter, cash_id)
			let totalPayments = await calculateCashSum(payments[0])

			let salesCash = await DetailPayment.paymentByMethod('CASH', date_filter, cash_id)
			let totalSalesCash = await calculateCashSum(salesCash[0])

			let salesCards = await DetailPayment.paymentByMethod('TARJETAS', date_filter, cash_id)
			let totalSalesCards = await calculateCashSum(salesCards[0])

			let incomes = await CashMovement.query()
											.where('type', 'income')
											.andWhere('cash_id', cash_id)
											.whereRaw('DATE(register_date) = ?', [date_filter])
											.orderBy('register_date', 'DESC')
											.fetch()

			let totalIncomes = await calculateCashSum(incomes.rows)

			let expenses = await CashMovement.query()
											.where('type', 'expense')
											.andWhere('cash_id', cash_id)
											.whereRaw('DATE(register_date) = ?', [date_filter])
											.orderBy('register_date', 'DESC')
											.fetch()

			let totalExpenses = await calculateCashSum(expenses.rows)

			let balance = await CashMovement.query()
											.where('type', 'balance_favor')
											.andWhere('cash_id', cash_id)
											.whereRaw('DATE(register_date) = ?', [date_filter])
											.orderBy('register_date', 'DESC')
											.fetch()

			let totalBalance = await calculateCashSum(balance.rows)

			let amount_open = 0;
			if (cashCurrent) {
				amount_open = cashCurrent.amount_open;
			}

			let totalCash = amount_open + totalPayments + totalSalesCash + totalSalesCards + totalBalance + totalIncomes - totalExpenses
			let totalMoney = amount_open + totalPayments + totalSalesCash + totalBalance + totalIncomes - totalExpenses
			let totalCards = totalSalesCards

			const totalAmounts = {
        cash_id: cash_id,
				totalCashCurrent: amount_open,
				totalPayments,
				totalSalesCash,
				totalSalesCards,
				totalIncomes,
				totalExpenses,
				totalCash,
				totalMoney,
				totalCards,
				totalBalance
			}

			return response.status(200).json(totalAmounts)

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener montos totales de caja.'
			})
		}
	}

	async movementsByUser ({request, response}) {
	  try {

	    let cash_id = request.input('cash_id');

	    let movements = await this.cashService.movementsByUser(cash_id);

	    return response.json(movements);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al obtener movimientos por usuarios.'
      })
    }
  }
}


module.exports = CashflowController
