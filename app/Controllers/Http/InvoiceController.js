'use strict'

const Cash = use('App/Models/Cash');
const CashService = use('App/Services/CashService');
const Customer = use('App/Models/Customer');
const Database = use('Database')
const DateAndTime = require('date-and-time');
const Debt = use('App/Models/Debt');
const DetailOrder = use('App/Models/DetailOrder');
const DetailPayment = use('App/Models/DetailPayment');
const GeneralData = use('App/Models/GeneralData');
const Logger = use('Logger');
const Order = use('App/Models/Order');
const Payment = use('App/Models/Payment');
const Voucher = use('App/Models/Voucher');
const InvoiceService = use('App/Services/InvoiceService');

class InvoiceController {

  constructor() {
    this.invoiceService = new InvoiceService;
    this.cashService = new CashService;
  }

  async generateInvoice ({auth, request, response}) {
    const transaction = await Database.beginTransaction();
    try {
      let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');

      let user = await auth.getUser();
      let customer_id = request.input('customer_id');
      let orders_id = request.input('orders_id');
      let cash_id = request.input('cash_id');
      let amount_payment = request.input('amount_payment');
      let observations = request.input('observations');
      let method_payment = request.input('method');

      let customer = await Customer.find(customer_id);

      let orders = await Order.query()
        .whereIn('id', orders_id)
        .orderBy('id')
        .fetch();

      let subtotal = 0;
      let igv = 0;
      let total = 0;
      let subtotal_original = 0;
      let total_gratuitas = 0;

      for (let item of orders.rows) {
        subtotal = subtotal + parseFloat(item.subtotal);
        igv = igv + parseFloat(item.igv);
        total = total + parseFloat(item.total);
        subtotal_original = subtotal_original + parseFloat(item.subtotal_original);
        total_gratuitas = total_gratuitas + parseFloat(item.total_gratuitas);
      }

      //generar numero de factura o boleta
      let type_document = 'FACTURA';
      if (customer.type_document === 'DNI') {
        type_document = 'BOLETA';
      }
      let voucher = await Voucher.query()
        .where('type', type_document)
        .first();

      await Voucher.query()
        .where('type', type_document)
        .increment('current_number', 1);

      let serie = '';
      let correlative = '';
      if (voucher) {
        serie = voucher.correlative;
        let stringCorrelative = voucher.current_number.toString()
        correlative = stringCorrelative.padStart(8, '0');
      }

      let newOrder = await Order.create({
        customer_id: customer.id,
        entry_time: currentDate,
        delivery_time: currentDate,
        subtotal: subtotal,
        total: total,
        igv: igv,
        subtotal_original: subtotal_original,
        status: 'Facturado',
        direct_sale: 1,
        discount: 0,
        date_invoice: currentDate,
        type_document: type_document,
        serie: serie,
        correlative: correlative,
        user_id: user.id,
        total_gratuitas: total_gratuitas,
        type_sale: 'contado',
        source: 'debts',
        observation: observations
      }, transaction);

      //actualizar order_id de los detalles
      for (let item of orders.rows) {
        let order_id_original = item.id;

        let debt = await Debt.query()
          .where('order_id', item.id)
          .first();

        if (debt) {
          debt.status = 'facturado';
          debt.amount_paid = debt.amount;
          debt.amount_pending = 0;
          debt.payment_date = currentDate;
          debt.order_id_generated = newOrder.id;
          await debt.save(transaction);
        }

        await Order.query()
          .where('id', item.id)
          .update({batch_invoice: true}, transaction);

        let detail = await DetailOrder.query()
          .where('order_id', item.id)
          .fetch();

        for (let detailItem of detail.rows) {
          await DetailOrder.create({
            order_id: newOrder.id,
            product_id: detailItem.product_id,
            quantity: detailItem.quantity,
            base_price: detailItem.base_price,
            discount_type: detailItem.discount_type,
            discount_amount: detailItem.discount_amount,
            subtotal: detailItem.subtotal,
            discount_value: detailItem.discount_value,
            discount_group: detailItem.discount_group,
            discount_item: detailItem.discount_item,
            type_sale: detailItem.type_sale,
            set_discount_customer: detailItem.set_discount_customer,
            name: detailItem.name,
            type_product: detailItem.type_product,
            has_return: detailItem.has_return,
            unit_price: detailItem.unit_price,
            wholesale_price: detailItem.wholesale_price,
            preferential_price: detailItem.preferential_price,
            subtotal_original: detailItem.subtotal_original,
            is_bonus: detailItem.is_bonus
          }, transaction);
        }
      }

      //registrar pago
      let cashCurrent = await Cash.query().whereRaw('DATE(register_date) = CURRENT_DATE')
        .where('status', 'abierta')
        .orderBy('created_at', 'DESC')
        .first();

      const payment = await Payment.create({
        order_id: newOrder.id,
        registration_date: currentDate,
        discount: newOrder.discount,
        subtotal: newOrder.subtotal,
        igv: newOrder.igv,
        total: newOrder.total,
        balance_favor: 0,
        total_debt: newOrder.total,
        amount_change: 0,
        save_change: false,
        cash_id: cash_id,
        user_id: user.id
      }, transaction);

      //detalle del pago
      let confirmed = true;
      let type_movement = 'cash';
      if (method_payment !== 'CASH') {
        confirmed = false;
        type_movement = 'card';
      }
      let detailPayment = await DetailPayment.create({
        payment_id: payment.id,
        payment_method: method_payment,
        payment_amount: amount_payment,
        cash_id: cash_id,
        user_id: user.id,
        confirmed: confirmed,
        observations: observations
      }, transaction);

      await this.cashService.store('order', transaction, type_movement, newOrder.id, detailPayment);

      await transaction.commit();

      let enterprise = await GeneralData.find(1);

      if (enterprise) {
        if (enterprise.user_fe && enterprise.password_fe) {
          let invoiceSend = await this.invoiceService.sendInvoiceToSenda(newOrder.id);
          if (invoiceSend === '0000-CPE ha sido aceptada') {
            newOrder.send_fe = true;
            await newOrder.save();
          }
        }
      }

      let orderData = await Order.query()
        .where('id', newOrder.id)
        .with('customer')
        .with('deliverystart')
        .with('deliveryend')
        .with('products')
        .with('user')
        .with('detail')
        .first();

      return response.json(orderData);

    } catch (error) {
      await transaction.rollback();
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al generar factura.'
      })
    }
  }
}

module.exports = InvoiceController
