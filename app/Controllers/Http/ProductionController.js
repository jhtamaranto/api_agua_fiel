'use strict'

const Logger = use('Logger')

const Supply = use('App/Models/Supply')
const SupplyMovement = use('App/Models/SupplyMovement')

class ProductionController {

	async index({response}) {
		try{
			let supplies =await Supply.query()
								.with('unit')
								.with('category')
								.orderBy('name', 'asc').fetch()

			return supplies

		} catch(error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener lista de insumos.'
			})
		}
	}
}

module.exports = ProductionController
