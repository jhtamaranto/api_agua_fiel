'use strict'

const Logger = use('Logger');
const Customer = use('App/Models/Customer');
const CustomerAddress = use('App/Models/CustomerAddress');
class CustomerAddressController {

  async index ({request, response}) {
    try {
      let customer_id = request.input('customer_id');

      let addresses = await CustomerAddress.query()
        .where('customer_id', customer_id)
        .fetch();

      return response.json(addresses);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al obtener direcciones del cliente.'
      })
    }
  }

  async store ({request, response}) {
    try {

      let customer_id = request.input('customer_id');
      let address = request.input('address');
      let reference = request.input('reference');
      if (reference) {
        reference = reference.toUpperCase();
      }

      await CustomerAddress.create({
        customer_id: customer_id,
        address: address.toUpperCase(),
        reference: reference
      });

      return response.json({
        'message': 'Dirección fue registrada de forma correcta'
      });

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al registrar direcciones del cliente.'
      });
    }
  }

  async update ({params, request, response}) {
    try {
      let customerAddress = await CustomerAddress.find(params.id);

      let address = request.input('address');
      let reference = request.input('reference');
      if (reference) {
        reference = reference.toUpperCase();
      }

      customerAddress.merge({
        address: address.toUpperCase(),
        reference: reference
      });

      await customerAddress.save();

      return response.json({
        'message': 'Dirección fue actualizada de forma correcta'
      });

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al actualizar direccion del cliente.'
      });
    }
  }

  async destroy ({params, response}) {
    try {
      let customerAddress = await CustomerAddress.find(params.id);

      await customerAddress.delete();

      return response.json({
        'message': 'Dirección fue eliminada de forma correcta'
      });

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al eliminar direccion del cliente.'
      });
    }
  }
}

module.exports = CustomerAddressController
