'use strict'

const Asset = use('App/Models/Asset')
const Composition = use('App/Models/Composition')
const Database = use('Database')
const DateAndTime = require('date-and-time')
const Inventory = use('App/Models/Inventory')
const Kardex = use('App/Models/KardexProduct')
const KardexSupply = use('App/Models/KardexSupply')
const Logger = use('Logger')
const Mobility = use('App/Models/Mobility')
const Product = use('App/Models/Product')
const Storehouse = use('App/Models/Storehouse')
const Supply = use('App/Models/Supply')

class StorehouseController {

	async index({request, response}) {
		try {
			let filter = request.all()
			let storehouses = await Storehouse.getStorehouses(filter)

			return response.json(storehouses[0])

		} catch(error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al listar almacenes.'
			})
		}
	}

	async getInventory({request, response}) {
		try {
			let storehouse_id = request.input('storehouse_id');

      let inventories = await Inventory.listProducts(storehouse_id);

			return response.json(inventories[0])

		} catch(error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al listar el inventario.'
			})
		}
	}

	async getInventoryFromStorehouseMain({request, response}) {
		try {
			let storehouse = await Storehouse.query().where('main', true).where('active', true).first()

      /*
			let inventories = await Inventory.query()
        .where('storehouse_id', storehouse.id)
        .where('active', true)
        .with('product')
        .fetch();

       */

      let inventories = await Inventory.listProducts(storehouse.id);

			return response.json(inventories[0])

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al listar el inventario del almacén principal.'
			})
		}
	}

	async registerRecharge({request, response}) {
		//const transaction = await Database.beginTransaction()
		try {

			const storehouse_id = request.input('storehouse_id')
			let products = request.input('products')

			const storehouse = await Storehouse.query().where('id', parseInt(storehouse_id)).first()

			if (storehouse.main) {
				for (let i = 0; i < products.length; i++) {
					let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');
          let rechargeQuantity = parseFloat(products[i].quantity);
					let type = products[i].type;

					let product = null;

					if (type === 'producto') {
            product = await Product.find(parseInt(products[i].product_id));

            //Si es compuesto, restar stock en insumos
            if (product.compound) {
              let supplies = await Composition.query().where('product_id', product.id).fetch()

              for (let index in supplies.rows) {
                let supplyComposition = supplies.rows[index]
                let quantityOut = parseFloat(supplyComposition.quantity) * rechargeQuantity

                let supply = await Supply.find(supplyComposition.supply_id)
                let supplyNewStock = supply.stock - quantityOut
                supply.merge({
                  stock: supplyNewStock
                })
                await supply.save()

                let supplyKardex = await KardexSupply.create({
                  supply_id: supplyComposition.supply_id,
                  operation_date: currentDate,
                  type: 'SALIDA',
                  sub_type: 'PRODUCCIÓN',
                  quantity: quantityOut,
                  stock: supply.stock,
                  cost: supply.cost,
                  total: supply.stock * supply.cost
                })
              }
            }
          } else if (type === 'activo') {
            product = await Asset.find(parseInt(products[i].product_id));
          }

					//Registrar ingreso en inventario
					let newInventory = await Inventory.find(parseInt(products[i].inventory_id))
					let new_stock_previous = newInventory.stock
					let new_stock = newInventory.stock + rechargeQuantity
					newInventory.merge({
						stock_previous: new_stock_previous,
						stock: new_stock
					})
					await newInventory.save()

					//Registrar movimiento en kardex
					await Kardex.create({
						inventory_id: newInventory.id,
						register_date: currentDate,
						description: 'Recarga de stock',
						type: 'INGRESO',
						subtype: 'RECARGA',
						quantity: rechargeQuantity,
						stock: newInventory.stock
					})

          if (type === 'producto') {
            if (product) {
              product.merge({
                stock: new_stock
              })
              await product.save();
            }
          }
				}
			} else {
				const mobility = await Mobility.find(storehouse.mobility_id)

				let storehouseMain = await Storehouse.query().where('main', true).where('active', true).first()

				for (let i = 0; i < products.length; i++) {
					let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');
          let type = products[i].type;
          let rechargeQuantity = parseFloat(products[i].quantity);

          let product = null;
          if (type === 'producto') {
            product = await Product.find(parseInt(products[i].product_id));

          } else if (type === 'activo') {
            product = await Asset.find(parseInt(products[i].product_id));
          }

					//Descontar de inventario en almacen principal
					let newInventoryMain = await Inventory.find(parseInt(products[i].inventory_id))
					let new_stock_previous = newInventoryMain.stock
					let new_stock = newInventoryMain.stock - rechargeQuantity
					newInventoryMain.merge({
						stock_previous: new_stock_previous,
						stock: new_stock
					})
					await newInventoryMain.save()

					//Registrar kardex de salida
					await Kardex.create({
						inventory_id: newInventoryMain.id,
						register_date: currentDate,
						description: 'Recarga de la unidad ' + mobility.tuition_number,
						type: 'SALIDA',
						subtype: 'RECARGA',
						quantity: rechargeQuantity,
						stock: newInventoryMain.stock
					});

          if (type === 'producto') {
            product.merge({
              stock: new_stock
            });
            await product.save();
          }

					//Aumentar en inventario de unidad movil
					let newInventory = await Inventory.query()
            .where('storehouse_id', storehouse.id)
            .where('product_id', parseInt(products[i].product_id))
            .where('type', type)
            .first();

					//Si aun no existe, se registra en invetario de und movil.s
					if(!newInventory) {
						newInventory = await Inventory.create({
              storehouse_id: storehouse.id,
              product_id: product.id,
              date_entry: currentDate,
              stock: rechargeQuantity,
              stock_previous: 0,
              type: type
						});
					} else {
						let new_stock_previous_und = newInventory.stock
						let new_stock_und = newInventory.stock + rechargeQuantity
						newInventory.merge({
							stock_previous: new_stock_previous_und,
							stock: new_stock_und
						})
						await newInventory.save()
					}

					//Registrar ingreso en kardex
					await Kardex.create({
						inventory_id: newInventory.id,
						register_date: currentDate,
						description: 'Recarga de stock',
						type: 'INGRESO',
						subtype: 'RECARGA',
						quantity: rechargeQuantity,
						stock: newInventory.stock
					})
				}
			}

			//await transaction.commit()

			return response.status(200).json({
				message: 'Operación exitosa.'
			})

		} catch(error) {
			//await transaction.rollback()
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al registrar recarga.'
			})
		}
	}

	async registerDecrease({request, response}) {
		try {
      let storehouse_id = request.input('storehouse_id');
      let inventory_id = request.input('inventory_id');
			let quantity = request.input('quantity');
      let reason = request.input('reason');
      if (reason) {
        reason = reason.toUpperCase();
      }

			const storehouse = await Storehouse.query().where('id', parseInt(storehouse_id)).first();

      let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');

      let newInventory = await Inventory.find(inventory_id);

      let new_stock_previous = newInventory.stock
      let new_stock = parseFloat(newInventory.stock) - parseFloat(quantity);
      newInventory.merge({
        stock_previous: new_stock_previous,
        stock: new_stock
      })
      await newInventory.save()

      //Registrar merma en kardex
      await Kardex.create({
        inventory_id: newInventory.id,
        register_date: currentDate,
        description: 'Merma',
        type: 'MERMA',
        subtype: 'MERMA',
        quantity: quantity,
        stock: newInventory.stock,
        reason_decrease: reason
      });

			return response.status(200).json({
				message: 'Operación exitosa.'
			})

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al registrar recarga.'
			})
		}
	}

	async registerDischarge({response, request}) {
		try {
			let storehouse_id = request.input('storehouse_id');
			let inventory_id = request.input('inventory_id');
			let quantity = request.input('quantity');
			let quantity_empty = request.input('quantity_empty');

			if (!quantity) {
        quantity = 0;
      }
			if (!quantity_empty) {
        quantity_empty = 0;
      }

			const storehouse = await Storehouse.query().where('id', parseInt(storehouse_id)).first()
			const storehouseMain = await Storehouse.query().where('main', true).where('active', true).first()

			const mobility = await Mobility.find(storehouse.mobility_id);

      let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');

      //Registrar descuento en inventario de und movil
      let inventoryMobility = await Inventory.find(inventory_id);

      let product_id = inventoryMobility.product_id;
      let type_product = inventoryMobility.type;

      let product = null;
      if (type_product === 'producto') {
        product = await Product.find(product_id);
      } else if (type_product === 'activo') {
        product = await Asset.find(product_id);
      }

      let new_stock_previous = inventoryMobility.stock
      let new_stock = parseFloat(inventoryMobility.stock) - parseFloat(quantity);
      let new_stock_empty = parseFloat(inventoryMobility.stock_empty) - parseFloat(quantity_empty);
      inventoryMobility.merge({
        stock_previous: new_stock_previous,
        stock: new_stock,
        stock_empty: new_stock_empty
      })
      await inventoryMobility.save();

      await Kardex.create({
        inventory_id: inventoryMobility.id,
        register_date: currentDate,
        description: 'DESCARGA DE STOCK',
        type: 'SALIDA',
        subtype: 'DESCARGA',
        quantity: quantity,
        stock: inventoryMobility.stock
      });

      if (quantity_empty > 0) {
        await Kardex.create({
          inventory_id: inventoryMobility.id,
          register_date: currentDate,
          description: 'DESCARGA DE STOCK VACIOS',
          type: 'SALIDA',
          subtype: 'DESCARGA',
          quantity: quantity_empty,
          stock: inventoryMobility.stock_empty
        });
      }

      //Registrar en inventario de almacén principal
      let inventoryMain = await Inventory.query()
        .where('storehouse_id', storehouseMain.id)
        .where('product_id', product_id)
        .where('type', type_product)
        .first();

      let stock_previous = inventoryMain.stock;
      let stock = parseFloat(inventoryMain.stock) + parseFloat(quantity);
      let stock_empty = parseFloat(inventoryMain.stock_empty) + parseFloat(quantity_empty);
      inventoryMain.merge({
        stock_previous: stock_previous,
        stock: stock,
        stock_empty: stock_empty
      })
      await inventoryMain.save();

      await Kardex.create({
        inventory_id: inventoryMain.id,
        register_date: currentDate,
        description: 'DESCARGA DE LA UNIDAD ' + mobility.tuition_number,
        type: 'INGRESO',
        subtype: 'PRODUCCIÓN',
        quantity: quantity,
        stock: inventoryMain.stock
      });

      if (quantity_empty > 0) {
        await Kardex.create({
          inventory_id: inventoryMain.id,
          register_date: currentDate,
          description: 'DESCARGA (VACIOS) DE LA UNIDAD ' + mobility.tuition_number,
          type: 'INGRESO',
          subtype: 'PRODUCCIÓN',
          quantity: quantity_empty,
          stock: inventoryMain.stock_empty
        });
      }

      if (type_product === 'producto') {
        if (product) {
          product.merge({
            stock: stock
          });
          await product.save();
        }
      }

			return response.status(200).json({
				message: 'Operación exitosa.'
			})

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al registrar recarga.'
			})
		}
	}
}

module.exports = StorehouseController
