'use strict'

const Logger = use('Logger')

const Resource = use('App/Models/Resource')

class ResourceController {

	async index({response}) {
		try{
			const resources = await Resource.query()
										.with('permissions').orderBy('section_name', 'asc').fetch()

			return resources

		} catch(error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener lista de recursos.'
			})
		}
	}

}

module.exports = ResourceController
