'use strict'

const DateAndTime = require('date-and-time');
const Drum = use('App/Models/Drum');
const Excel = require('exceljs');
const Logger = use('Logger');

class ExcelDrumSummaryController {

  async index ({request, response}) {
    try {

      const parameters = request.all();

      let query = Drum.query()
        .select([
          'customers.name as customer_name',
          'customers.surname as customer_surname',
          'customers.document as customer_document',
          'products.name as product_name',
          'drums.status as drum_status'
        ]).sum('drums.quantity as total')
        .innerJoin('customers', 'customers.id', 'drums.customer_id')
        .innerJoin('products', 'products.id', 'drums.product_id');

      if (parameters.customer_id) {
        query.where('drums.customer_id', parameters.customer_id);
      }

      if (parameters.product_id) {
        query.where('drums.product_id', parameters.product_id);
      }

      if (parameters.date_from) {
        query.where('drums.register_date', '>=', parameters.date_from);
      }

      if (parameters.date_until) {
        query.where('drums.register_date', '<=', parameters.date_until);
      }

      let drums = await query.groupByRaw('customers.name, customers.surname, customers.document, products.name, drums.status')
        .orderBy('customers.name', 'ASC');

      //---------------------------------------------------------------------
      const workbook = new Excel.Workbook();
      const sheet = workbook.addWorksheet('Worksheet');

      let indexRow = 1;
      let row = sheet.getRow(indexRow);
      //---- Titulos
      const font = {bold: true};

      row.getCell('A').value = 'CLIENTE';
      row.getCell('B').value = 'RUC / DNI';
      row.getCell('C').value = 'PRODUCTO';
      row.getCell('D').value = 'CANTIDAD';
      row.getCell('E').value = 'ESTADO';

      sheet.getColumn('A').width = 50;
      sheet.getColumn('B').width = 15;
      sheet.getColumn('C').width = 33;
      sheet.getColumn('D').width = 14;
      sheet.getColumn('E').width = 15;

      row.getCell('A').font = font;
      row.getCell('B').font = font;
      row.getCell('C').font = font;
      row.getCell('D').font = font;
      row.getCell('E').font = font;

      const border = {
        bottom: {style: 'medium'}
      };

      row.getCell('A').border = border;
      row.getCell('B').border = border;
      row.getCell('C').border = border;
      row.getCell('D').border = border;
      row.getCell('E').border = border;

      row.commit();

      for (let item of drums) {
        indexRow++;
        row = sheet.getRow(indexRow);

        let customer = item.customer_name;
        if (item.customer_surname) {
          customer = customer + ' ' + item.customer_surname;
        }
        let customer_document = '';
        if (item.customer_document) {
          customer_document = item.customer_document;
        }
        let status = '';
        if (item.drum_status) {
          status = item.drum_status.toUpperCase();
        }

        row.getCell('A').value = customer;
        row.getCell('B').value = customer_document;
        row.getCell('C').value = item.product_name;
        row.getCell('D').value = item.total;
        row.getCell('E').value = status;

        row.commit();
      }

      response.header(`Content-Type`, `application/vnd.openxmlformats-officedocument.spreadsheetml.sheet`)
      response.header(`Content-Disposition`, `attachment; filename="template.xlsx`)

      return workbook.xlsx.write(response.response);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al descargar reporte.'
      })
    }
  }
}

module.exports = ExcelDrumSummaryController
