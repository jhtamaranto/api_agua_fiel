'use strict'

const Logger = use('Logger')

const Asset = use('App/Models/Asset')
const DateAndTime = require('date-and-time')
const Inventory = use('App/Models/Inventory')
const Storehouse = use('App/Models/Storehouse')

class AssetController {

	async index({response}) {
		try{
			const assets = await Asset.query()
										.with('unit').orderBy('name', 'asc').fetch()

			return assets

		} catch(error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener lista de activos.'
			})
		}
	}

	async store({request, response}) {
		let { unit_id, name, description, useful_life, quantity, price, loan } =  request.all()

		try{
		  if (price == null) {
		    price = 0;
      }
			const asset = await Asset.create({
				unit_id,
				name,
				description,
				useful_life,
				quantity,
        price,
        loan
			});

		  if (asset.loan == 1 || asset.loan == true) {
        //Registrar activo en inventario de almacen principal
        let storehouse =await Storehouse.query().where('main', true).first();

        let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');

        await Inventory.create({
          storehouse_id: storehouse.id,
          product_id: asset.id,
          date_entry: currentDate,
          stock: 0,
          stock_previous: 0,
          type: 'activo'
        });
      }

			const assetCreated = await Asset.query().where('id', asset.id)
										.with('unit').first()

			return response.json(assetCreated);

		} catch(error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al registrar activo.'
			})
		}
	}

	async update({params, request, response}) {
		const {id} = params
		try{
			const asset = await Asset.find(id)

			let { unit_id, name, description, useful_life, quantity, price, loan } =  request.all();

			if (price == null) {
			  price = 0;
      }

			const data = {
				unit_id, name, description, useful_life, quantity, price, loan
			}

			asset.merge(data)

      await asset.save();

      if (asset.loan == 1 || asset.loan == true) {
        //Verificar si esta registrado en inventario del almacén principal
        let storehouse =await Storehouse.query().where('main', true).first()

        let inventory = await Inventory.query().where('storehouse_id', storehouse.id)
          .where('product_id', asset.id)
          .where('type', 'activo')
          .first();

        let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');

        if(!inventory) {
          await Inventory.create({
            storehouse_id: storehouse.id,
            product_id: asset.id,
            date_entry: currentDate,
            stock: 0,
            stock_previous: 0,
            type: 'activo'
          });
        }
      }



      const assetUpdated = await Asset.query().where('id', asset.id)
        .with('unit').first();

			return response.json(assetUpdated);

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al actualizar activo.'
			})
		}
	}

	async inactivate({params}) {
		const {id} = params

		try{
			const asset = await Asset.find(id)

			let newStatus = null
	        if (asset.status === 'activo') {
	            newStatus = 'inactivo'
	        } else{
	            newStatus = 'activo'
	        }

	        asset.merge({
	            status: newStatus
	        })

	        await asset.save()

	        const assetUpdated = await Asset.query().where('id', asset.id)
											.with('unit').first()

	        return assetUpdated

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al cambiar estado de activo.'
			})
		}
	}

}

module.exports = AssetController
