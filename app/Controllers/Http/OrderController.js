'use strict'

const Logger = use('Logger')

const DateAndTime = require('date-and-time');

const Customer = use('App/Models/Customer')
const Database = use('Database')
const DetailOrder = use('App/Models/DetailOrder')
const Inventory = use('App/Models/Inventory')
const Kardex = use('App/Models/KardexProduct')
const Mobility = use('App/Models/Mobility')
const Order = use('App/Models/Order')
const Payment = use('App/Models/Payment');
const Product = use('App/Models/Product')
const Storehouse = use('App/Models/Storehouse')

class OrderController {

	async index({ request, response }) {
		try {
			let query = Order.query()
			let user_id = request.input('user_id')

			let status = ['Registrado', 'En Reparto', 'Entregado']
			if (user_id) {
				query.where(function () {
					this.where(function () {
						this.where('deliveryman', parseInt(user_id))
							.whereRaw('final_deliveryman IS NULL')
					})
						.orWhere('final_deliveryman', parseInt(user_id))
				})

				status = ['En Reparto', 'Entregado']
			}
			let orders = await query.with('customer')
				.whereIn('status', status)
				.with('deliverystart')
				.with('deliveryend')
				.with('detail')
				.with('products')
				.fetch();

			return orders

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al listar pedidos.'
			})
		}
	}

	async store({ auth, request, response }) {
		let data = request.all()
		const transaction = await Database.beginTransaction()

		try {
			let user = await auth.getUser();
			let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');

			let status = 'Registrado'
			let direct_sale = 0
			if (data.direct_sale) {
				direct_sale = 1
				status = 'Facturado'
			}

			let entry_time = null;
			if (data.entry_time == null) {
				entry_time = currentDate;
			} else {
				entry_time = data.entry_time + ' 12:00:00';
			}

			let total_gratuitas = 0;
			if (data.total_gratuitas) {
				total_gratuitas = parseFloat(data.total_gratuitas);
			}

			let order = await Order.create({
				customer_id: data.customer.id,
				entry_time: entry_time,
				subtotal: data.subtotal,
				total: data.total,
				igv: data.amount_igv,
				status: status,
				direct_sale: direct_sale,
				discount: data.discount,
				total_gratuitas: total_gratuitas,
				user_id: user.id,
        order_address: data.order_address
			}, transaction);

			let products = data.products
			let customer = await Customer.find(data.customer.id)
			let total_pending_drums = 0;
			let subtotal_original = 0;

			for (let item of products) {
				let valueIds = item.id.split('-');

				let type_sale = null;
				if (valueIds[0] === 'p') {
					let product = await Product.find(valueIds[1]);

					if (product.has_return === true || product.has_return === 1) {
						type_sale = item.type_sale;
						if (type_sale === 'recarga') {
							total_pending_drums = total_pending_drums + parseInt(item.quantity);
						}
					} else {
						type_sale = 'completo';
					}
				} else if (valueIds[0] === 'a') {
					type_sale = 'recarga';
				}

				let subtotal_original_item = 0;
				if (!item.is_bonus) {
					subtotal_original_item = parseFloat(item.price) + parseFloat(item.discount_item);
					subtotal_original = subtotal_original + subtotal_original_item;
          console.log('subtotal_original_item',subtotal_original_item)
          console.log('subtotal_original',subtotal_original)
				}

				await DetailOrder.create({
					order_id: order.id,
					product_id: valueIds[1],
					quantity:parseInt(item.quantity),
					base_price: item.base_price,
					discount_type: item.discount_type,
					discount_amount: item.discount_amount,
					subtotal: item.price,
					discount_value: item.discount_value,
					discount_group: item.discount_group,
					discount_item: item.discount_item,
					type_sale: type_sale,
					set_discount_customer: item.set_discount_customer,
					name: item.name,
					type_product: item.type_product,
					has_return: item.has_return,
					unit_price: item.unit_price,
					wholesale_price: item.wholesale_price,
					preferential_price: item.preferential_price,
					subtotal_original: subtotal_original_item,
					is_bonus: item.is_bonus
				}, transaction);

			}

			order.subtotal_original = subtotal_original;
			order.save(transaction);

			if (total_pending_drums > 0) {
				customer.pending_drums = parseInt(customer.pending_drums) + total_pending_drums
				await customer.save(transaction)

				order.pending_drums = total_pending_drums
				await order.save(transaction)
			}

			await transaction.commit()

			return order

		} catch (error) {
			await transaction.rollback()
      console.log(error)
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al registrar pedido.'
			})
		}
	}

	async update({ request, response }) {
		let data = request.all()
		const transaction = await Database.beginTransaction()

		try {
			let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');

			let order = await Order.find(data.order_id)
			let last_pending_drums = order.pending_drums
			order.entry_time = data.entry_time;
			order.subtotal = data.subtotal
			order.total = data.total
			order.igv = data.amount_igv;
			order.order_address = data.order_address;

			await order.save(transaction)

			let products = data.products
			let customer = await Customer.find(data.customer.id)

			customer.pending_drums = customer.pending_drums - last_pending_drums
			await customer.save(transaction)

			await DetailOrder.query().where('order_id', order.id).delete(transaction)

			let total_pending_drums = 0
			for (let item of products) {
				let valueIds = item.id.split('-');

				let type_sale = null;
				if (valueIds[0] === 'p') {
					let product = await Product.find(valueIds[1]);

					if (product.has_return === true || product.has_return === 1) {
						type_sale = item.type_sale;
						if (type_sale === 'recarga') {
							total_pending_drums = total_pending_drums + parseInt(item.quantity);
						}
					} else {
						type_sale = 'completo';
					}
				} else if (valueIds[0] === 'a') {
					type_sale = 'recarga';
				}

				await DetailOrder.create({
					order_id: order.id,
					product_id: valueIds[1],
					quantity: item.quantity,
					base_price: item.base_price,
					discount_type: item.discount_type,
					discount_amount: item.discount_amount,
					subtotal: item.price,
					discount_value: item.discount_value,
					discount_group: item.discount_group,
					discount_item: item.discount_item,
					type_sale: type_sale,
					set_discount_customer: item.set_discount_customer,
					name: item.name,
					type_product: item.type_product,
					has_return: item.has_return,
					unit_price: item.unit_price,
					wholesale_price: item.wholesale_price,
					preferential_price: item.preferential_price
				}, transaction);

			}

			if (total_pending_drums > 0) {
				customer.pending_drums = parseInt(customer.pending_drums) + total_pending_drums
				await customer.save(transaction)

				order.pending_drums = total_pending_drums
				await order.save(transaction)
			}

			await transaction.commit()

			return order

		} catch (error) {
			await transaction.rollback()
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al actualizar pedido.'
			})
		}
	}

	async changeStatus({ request, response }) {
		try {

			const { order_id, status, deliveryman, final_deliveryman } = request.all()

			const order = await Order.find(order_id)

			order.status = status
			order.deliveryman = deliveryman
			if (deliveryman < 0) {
				order.deliveryman = 0;
			}
			order.final_deliveryman = final_deliveryman
			if (final_deliveryman < 0) {
				order.final_deliveryman = 0;
			}
			if (status.toLowerCase() === 'entregado') {
				let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');
				order.delivery_time = currentDate
				if (final_deliveryman == null) {
					order.final_deliveryman = deliveryman
				}

				//registrar salida en kardex de la und. mobil
				let mobility = await Mobility.query()
					.where('user_id', order.final_deliveryman)
					.first()

				let storehouse = await Storehouse.query()
					.where('mobility_id', mobility.id)
					.where('active', true)
					.first()

				let products = await DetailOrder.query().where('order_id', order.id).fetch()
				for (let index in products.rows) {
					let product = products.rows[index]
					let quantity = product.quantity

					//Registrar descuento en inventario de und movil
					let inventoryMobility = await Inventory.query()
						.where('storehouse_id', storehouse.id)
						.where('product_id', product.product_id)
						.first()

					let new_stock_previous = inventoryMobility.stock
					let new_stock = inventoryMobility.stock - quantity
					inventoryMobility.merge({
						stock_previous: new_stock_previous,
						stock: new_stock
					})
					await inventoryMobility.save()

					//Registrar salida en kardex de und mobil
					await Kardex.create({
						inventory_id: inventoryMobility.id,
						register_date: currentDate,
						description: 'Entrega de pedido',
						type: 'SALIDA',
						subtype: 'VENTA',
						quantity: quantity,
						stock: inventoryMobility.stock
					})
				}
			}

			await order.save()

			const orderUpdated = await Order.query()
				.where('id', order_id)
				.with('customer')
				.with('deliverystart')
				.with('deliveryend')
				.with('products')
				.with('detail')
				.first();

			return response.json(orderUpdated);

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al cambiar estado pedido.'
			})
		}
	}

	async assingFinalDeliveryMan({ request, response }) {
		try {
			const { order_id, final_deliveryman } = request.all()

			const order = await Order.find(order_id)

			order.final_deliveryman = final_deliveryman

			await order.save()

			const orderUpdated = Order.query()
				.where('id', order_id)
				.with('customer')
				.with('deliverystart')
				.with('deliveryend')
				.with('products')
				.first()

			return orderUpdated

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al asignar repartidor final.'
			})
		}
	}

	async getByCustomer({ request }) {
		try {
			const customer_id = request.input('customer_id')
			const option = request.input('option')

			let query = Order.query()
				.where('customer_id', customer_id)

			if (option == 'one_date') {
				query.whereRaw('date(entry_time) = ?', [request.input('date')])
			} else if (option == 'two_date') {
				query.whereRaw('date(entry_time) >= ?', [request.input('start_date')])
					.whereRaw('date(entry_time) <= ?', [request.input('end_date')])
			}

			let orders = await query.with('customer')
				.with('deliverystart')
				.with('deliveryend')
				.with('products')
				.orderBy('entry_time', 'desc')
				.fetch()

			return orders

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener las ordenes.'
			})
		}
	}

	async getSummary({ request, response }) {
		try {

			let orders = await Order.summary();

			response.status(200).json(orders[0])

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener resumen de ordenes.'
			})
		}
	}

	async getById ({request, response}) {
	  try {
	    let order_id = request.input('order_id');

	    let order = await Order.query()
        .where('id', order_id)
        .with('customer')
        .with('deliveryend')
        .with('user')
        .with('detail')
        .first();

	    return response.json(order);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al buscar orden.'
      })
    }
  }

  async storeMethodPayment ({request, response}) {
	  try {
	    let orders = await Order.query()
        .where('status', 'Facturado')
        .whereNull('type_sale')
        .orderBy('id')
        .fetch();

	    for (let item of orders.rows) {
	      let exist_payment = await Payment.query()
          .where('order_id', item.id)
          .count('* as total');

	      if (exist_payment[0].total > 0) {
          item.type_sale = 'contado';
          item.save();
        } else {
          item.type_sale = 'credito';
          item.save();
        }
      }

	    return response.json({
        message: 'Se actualizaron los metodos de pago de forma correcta'
      });

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al guardar metodos de pago.'
      })
    }
  }
}

module.exports = OrderController
