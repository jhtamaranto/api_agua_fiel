'use strict'

const DateAndTime = require('date-and-time');
const DetailPayment = use('App/Models/DetailPayment');
const Excel = require('exceljs');
const Logger = use('Logger');

class ExcelPaymentController {

  async index ({request, response}) {
    try {
      const parameters = request.all();

      let query = DetailPayment.query()
        .select([
          'detail_payments.id',
          'detail_payments.payment_method',
          'detail_payments.payment_amount',
          'detail_payments.confirmed',
          'detail_payments.created_at as payment_date',
          'orders.type_document',
          'orders.serie',
          'orders.correlative',
          'customers.name as customer_name',
          'customers.surname as customer_surname',
          'customers.document as customer_document',
          'customers.type_document as customer_type_document',
          'users.first_name as user_name',
          'users.last_name as user_surname'
        ]).innerJoin('payments', 'payments.id', 'detail_payments.payment_id')
        .innerJoin('orders', 'orders.id', 'payments.order_id')
        .innerJoin('customers', 'customers.id', 'orders.customer_id')
        .leftJoin('users', 'users.id', 'orders.final_deliveryman')

      if (parameters.customer_id) {
        query.where('orders.customer_id', parameters.customer_id);
      }

      if (parameters.payment_method) {
        query.where('detail_payments.payment_method', parameters.payment_method);
      }

      if (parameters.status) {
        let status_string = parameters.status;
        if (status_string === 'CONFIRMADO') {
          query.where('detail_payments.confirmed', true);
        } else {
          query.where('detail_payments.confirmed', 0);
        }
      }

      if (parameters.date_from) {
        query.whereRaw('DATE(detail_payments.created_at) >= ?', [parameters.date_from]);
      }

      if (parameters.date_until) {
        query.whereRaw('DATE(detail_payments.created_at) <= ?', [parameters.date_until]);
      }

      let payments = await query.orderBy('detail_payments.created_at', 'DESC').fetch();

      //-----------------
      const workbook = new Excel.Workbook();
      const sheet = workbook.addWorksheet('Worksheet');

      let indexRow = 1;
      let row = sheet.getRow(indexRow);
      //---- Titulos
      const font = {bold: true};

      row.getCell('A').value = 'CLIENTE';
      row.getCell('B').value = 'RUC/DNI';
      row.getCell('C').value = 'COMPROBANTE';
      row.getCell('D').value = 'NRO. COMPROBANTE';
      row.getCell('E').value = 'FECHA';
      row.getCell('F').value = 'REPARTIDOR';
      row.getCell('G').value = 'MÉTODO DE PAGO';
      row.getCell('H').value = 'MONTO PAGADO';
      row.getCell('I').value = 'ESTADO';

      sheet.getColumn('A').width = 45;
      sheet.getColumn('B').width = 17;
      sheet.getColumn('C').width = 15;
      sheet.getColumn('D').width = 20;
      sheet.getColumn('E').width = 13;
      sheet.getColumn('F').width = 25;
      sheet.getColumn('G').width = 17;
      sheet.getColumn('H').width = 16;
      sheet.getColumn('I').width = 16;

      row.getCell('A').font = font;
      row.getCell('B').font = font;
      row.getCell('C').font = font;
      row.getCell('D').font = font;
      row.getCell('E').font = font;
      row.getCell('F').font = font;
      row.getCell('G').font = font;
      row.getCell('H').font = font;
      row.getCell('I').font = font;

      const border = {
        bottom: {style: 'medium'}
      };

      row.getCell('A').border = border;
      row.getCell('B').border = border;
      row.getCell('C').border = border;
      row.getCell('D').border = border;
      row.getCell('E').border = border;
      row.getCell('F').border = border;
      row.getCell('G').border = border;
      row.getCell('H').border = border;
      row.getCell('I').border = border;

      row.commit();
      for (let item of payments.rows) {
        indexRow++;
        row = sheet.getRow(indexRow);

        let name = item.customer_name;
        if (item.customer_surname) {
          name = name + " " + item.customer_surname;
        }
        row.getCell('A').value = name;
        row.getCell('B').value = item.customer_document;
        row.getCell('C').value = item.type_document;
        row.getCell('D').value = item.serie + "-" + item.correlative;

        let date_payment = DateAndTime.format(item.payment_date, 'DD/MM/YYYY');
        row.getCell('E').value = date_payment;

        let user_name = ''
        if (item.user_name) {
          user_name = item.user_name + ' ';
        }
        if (item.user_surname) {
          user_name = user_name + item.user_surname;
        }
        row.getCell('F').value = user_name;

        row.getCell('G').value = item.payment_method;
        row.getCell('H').value = item.payment_amount;
        row.getCell('H').numFmt = '0.00';

        let status = 'PENDIENTE';
        if (item.confirmed == 1) {
          status = 'CONFIRMADO';
        }
        row.getCell('I').value = status;

        row.commit();

      }

      response.header(`Content-Type`, `application/vnd.openxmlformats-officedocument.spreadsheetml.sheet`)
      response.header(`Content-Disposition`, `attachment; filename="template.xlsx`)

      return workbook.xlsx.write(response.response);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al listar pagos.'
      });
    }
  }
}

module.exports = ExcelPaymentController
