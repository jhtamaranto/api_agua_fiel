'use strict'

const Logger = use('Logger')
const Order = use('App/Models/Order')
const Product = use('App/Models/Product')

class DashboardController {

	async soldProducts({request, response}) {
		try {
			let month = request.input('month')
			let year = request.input('year')

			let products = await Product.soldProducts(month, year)

			response.status(200).json(products[0])

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener productos.'
			})
		}
	}

	async salesMonth({request, response}) {
		try {
			let month = request.input('month')
			let year = request.input('year')

			let sales = await Order.salesMonth(month, year)

			response.status(200).json(sales[0])

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener ventas del mes.'
			})
		}
	}
}

module.exports = DashboardController
