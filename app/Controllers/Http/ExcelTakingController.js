'use strict'

const DateAndTime = require('date-and-time');
const Excel = require('exceljs');
const Logger = use('Logger');
const Taking = use('App/Models/Taking');

class ExcelTakingController {

  async index ({request, response}) {
    try {
      let parameters = request.all();

      let query = Taking.query()
        .select(
          'takings.*',
          'users.first_name',
          'users.last_name'
        ).innerJoin('users', 'users.id', 'takings.user_id');

      if (parameters.user_id) {
        query.where('takings.user_id', parameters.user_id);
      }

      if (parameters.date_from) {
        query.where('takings.register_date', '>=', parameters.date_from);
      }

      if (parameters.date_until) {
        query.where('takings.register_date', '<=',parameters.date_until);
      }

      let takings = await query.orderBy('takings.register_date', 'DESC')
        .fetch();

      //-----------------
      const workbook = new Excel.Workbook();
      const sheet = workbook.addWorksheet('Worksheet');

      let indexRow = 1;
      let row = sheet.getRow(indexRow);
      //---- Titulos
      const font = {bold: true};

      row.getCell('A').value = 'FECHA';
      row.getCell('B').value = 'REPARTIDOR';
      row.getCell('C').value = 'TOTAL VENTAS';
      row.getCell('D').value = 'TOTAL EGRESOS';
      row.getCell('E').value = 'TOTAL ENTREGADO';

      sheet.getColumn('A').width = 13;
      sheet.getColumn('B').width = 25;
      sheet.getColumn('C').width = 15;
      sheet.getColumn('D').width = 20;
      sheet.getColumn('E').width = 18;

      row.getCell('A').font = font;
      row.getCell('B').font = font;
      row.getCell('C').font = font;
      row.getCell('D').font = font;
      row.getCell('E').font = font;

      const border = {
        bottom: {style: 'medium'}
      };

      row.getCell('A').border = border;
      row.getCell('B').border = border;
      row.getCell('C').border = border;
      row.getCell('D').border = border;
      row.getCell('E').border = border;

      row.commit();
      for (let item of takings.rows) {
        indexRow++;
        row = sheet.getRow(indexRow);

        let register_date = DateAndTime.format(item.register_date, 'DD/MM/YYYY');
        row.getCell('A').value = register_date;

        let name = item.first_name;
        if (item.last_name) {
          name = name + " " + item.last_name;
        }
        row.getCell('B').value = name;

        row.getCell('C').value = item.total_sales;
        row.getCell('C').numFmt = '0.00';
        row.getCell('D').value = item.total_expenses;
        row.getCell('D').numFmt = '0.00';
        row.getCell('E').value = item.total_delivered;
        row.getCell('E').numFmt = '0.00';

        row.commit();

      }

      response.header(`Content-Type`, `application/vnd.openxmlformats-officedocument.spreadsheetml.sheet`)
      response.header(`Content-Disposition`, `attachment; filename="template.xlsx`)

      return workbook.xlsx.write(response.response);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al descargar recaudaciones.'
      })
    }
  }
}

module.exports = ExcelTakingController
