'use strict'

const Logger = use('Logger')

const DateAndTime = require('date-and-time');
const Debt = use('App/Models/Debt')
const DebtPayment = use('App/Models/DebtPayment')

class DebtPaymentController {

	async getByDebt({request, response}) {
		try{
			const debt_id = request.input('debt_id')
			let payment = await DebtPayment.query()
											.where('debt_id', debt_id)
											.orderBy('payment_date', 'asc')
											.fetch()
			 return payment

		} catch(error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al listar pagos de deudas.'
			})
		}
	}

}

module.exports = DebtPaymentController
