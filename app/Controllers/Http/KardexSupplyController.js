'use strict'

const Database = use('Database')
const DateAndTime = require('date-and-time');
const Logger = use('Logger')
const KardexSupply = use('App/Models/KardexSupply')
const Supply = use('App/Models/Supply')

class KardexSupplyController {

	async getBySupply({request, response}) {
		try {
			let supply_id = request.input('supply_id')
			let kardex = await KardexSupply.query()
									.where('supply_id', supply_id)
									.orderBy('operation_date', 'desc')
									.fetch()

			return kardex

		} catch(error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener kardex.'
			})
		}
	}

	async saveNewInventory({request, response}) {
		const transaction = await Database.beginTransaction()
		try {
			let new_stocks = request.input('new_stocks')

			let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');
			for (let i = 0; i < new_stocks.length; i++) {
				let itemStock = new_stocks[i]
				let supply = await Supply.find(itemStock.supply_id)

				let supplyKardex = await KardexSupply.create({
					supply_id: itemStock.supply_id,
					operation_date: currentDate,
					type: 'INGRESO',
					sub_type: 'INVENTARIO',
					quantity: parseFloat(itemStock.new_stock),
					stock: parseFloat(itemStock.new_stock),
					cost: supply.cost,
					total: parseFloat(itemStock.new_stock) * supply.cost
				}, transaction)

				supply.stock = parseFloat(itemStock.new_stock)
				await supply.save(transaction)

			}

			await transaction.commit()

			return response.status(200).json({
				message: 'Opración correcta.'
			})

		} catch (error) {
			await transaction.rollback()
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener kardex.'
			})
		}
	}
}

module.exports = KardexSupplyController
