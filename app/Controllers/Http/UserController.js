'use strict'

const Logger = use('Logger')
const Role = use('App/Models/Role')
const RoleUser = use('App/Models/RoleUser')
const User = use('App/Models/User')

class UserController {

	async deliveryUserDropDown({response}) {
		const users = await User.query()
									.select('id as value', 'first_name as text')
									.where('status', 'activo').fetch()

		return users
	}

	async rolesDropDown({response}) {
		const roles = await Role.query()
									.select('id as value', 'name as text')
									.where('status', 'activo').fetch()

		return roles
	}

	async deliveryUsers({response}) {
		const users = await User.deliveryUsers()

		return users[0]
	}

	async getAll({request, response}) {
		try {
			let users = await User.query().with('roles').orderBy('id', 'DESC').fetch()

			return response.status(200).json(users)

		} catch(error){
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener lista de usuarios.'
			})
		}
	}

	async store({request, response}) {
		try{
			let user = await User.create({
				first_name: request.input('first_name'),
				last_name: request.input('last_name'),
				email: request.input('email'),
				username: request.input('username'),
				password: request.input('password')
			})

			await RoleUser.create({
				role_id: request.input('role_id'),
				user_id: user.id
			})

			return response.status(201).json(user)

		}catch(error){
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al registrar usuario.'
			})
		}
	}

	async update({request, response}) {
		try {
			const user_id = request.input('user_id')
			let user = await User.find(parseInt(user_id))

			let data = {
				first_name: request.input('first_name'),
				last_name: request.input('last_name'),
				email: request.input('email'),
				username: request.input('username')
			}

			let change_password = request.input('change_password')
			if(change_password) {
				data.password = request.input('password')
			}

			user.merge(data)

        	await user.save()

        	let role = await RoleUser.query().where('user_id', user.id).first()
        	let dataRole = {
        		role_id: parseInt(request.input('role_id'))
        	}
        	role.merge(dataRole)
        	await role.save()

        	return response.status(200).json(user)

		} catch(error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al actualizar usuario.'
			})
		}
	}

	async changeStatus({params, response}) {
		const {id} = params
		try{
			const user = await User.find(id)

			let newStatus = null
	        if (user.status === 'activo') {
	            newStatus = 'inactivo'
	        } else{
	            newStatus = 'activo'
	        }

	        user.merge({
	            status: newStatus
	        })

	        await user.save()

			return response.status(200).json(user)

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al inactivar usuario.'
			})
		}
	}

	async availableDeliveryUsers({request, response}) {
		try {
			let users = await User.getRepartidores()

			return response.status(200).json(users[0])

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener repartidores disponibles.'
			})
		}
	}

}

module.exports = UserController
