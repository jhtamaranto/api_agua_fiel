'use strict'

const AccountingService = use('App/Services/AccountingService');
const DateAndTime = require('date-and-time');
const Excel = require('exceljs');
const Logger = use('Logger');

class ExcelSaleAccountingController {

  constructor() {
    this.accountingService = new AccountingService;
  }

  async index ({request, response}) {
    try {
      let parameters = request.all();

      let sales = await this.accountingService.sales(parameters);

      const workbook = new Excel.Workbook();
      const sheet = workbook.addWorksheet('Worksheet');

      let indexRow = 1;
      let row = sheet.getRow(indexRow);
      //---- Titulos
      const font = {bold: true};

      row.getCell('A').value = 'O.';
      row.getCell('B').value = 'Vou';
      row.getCell('C').value = 'Fecha D.';
      row.getCell('D').value = 'Fecha V.';
      row.getCell('E').value = 'Doc';
      row.getCell('F').value = 'Serie';
      row.getCell('G').value = 'Número';
      row.getCell('H').value = 'R.Fecha';
      row.getCell('I').value = 'R.Doc';
      row.getCell('J').value = 'R.Número';
      row.getCell('K').value = 'Doc';
      row.getCell('L').value = 'Número';
      row.getCell('M').value = 'Razón Social';
      row.getCell('N').value = 'Valor Exp.';
      row.getCell('O').value = 'B.Imp.';
      row.getCell('P').value = 'Inafecto';
      row.getCell('Q').value = 'I.S.C.';
      row.getCell('R').value = 'I.G.V.';
      row.getCell('S').value = 'Otros';
      row.getCell('T').value = 'Exonerado';
      row.getCell('U').value = 'Total';
      row.getCell('V').value = 'T/C';
      row.getCell('W').value = 'Glosa';

      sheet.getColumn('A').width = 4;
      sheet.getColumn('B').width = 5;
      sheet.getColumn('C').width = 11;
      sheet.getColumn('D').width = 11;
      sheet.getColumn('E').width = 4.2;
      sheet.getColumn('F').width = 5.2;
      sheet.getColumn('G').width = 10;
      sheet.getColumn('H').width = 8;
      sheet.getColumn('I').width = 6;
      sheet.getColumn('J').width = 10;
      sheet.getColumn('K').width = 4;
      sheet.getColumn('L').width = 12;
      sheet.getColumn('M').width = 30;
      sheet.getColumn('N').width = 10;
      sheet.getColumn('O').width = 8;
      sheet.getColumn('P').width = 8;
      sheet.getColumn('Q').width = 5;
      sheet.getColumn('R').width = 7;
      sheet.getColumn('S').width = 6;
      sheet.getColumn('T').width = 9;
      sheet.getColumn('U').width = 10;
      sheet.getColumn('V').width = 8;
      sheet.getColumn('W').width = 40;

      row.getCell('A').font = font;
      row.getCell('B').font = font;
      row.getCell('C').font = font;
      row.getCell('D').font = font;
      row.getCell('E').font = font;
      row.getCell('F').font = font;
      row.getCell('G').font = font;
      row.getCell('H').font = font;
      row.getCell('I').font = font;
      row.getCell('J').font = font;
      row.getCell('K').font = font;
      row.getCell('L').font = font;
      row.getCell('M').font = font;
      row.getCell('N').font = font;
      row.getCell('O').font = font;
      row.getCell('P').font = font;
      row.getCell('Q').font = font;
      row.getCell('R').font = font;
      row.getCell('S').font = font;
      row.getCell('T').font = font;
      row.getCell('U').font = font;
      row.getCell('V').font = font;
      row.getCell('W').font = font;

      const border = {
        bottom: {style: 'medium'}
      };

      row.getCell('A').border = border;
      row.getCell('B').border = border;
      row.getCell('C').border = border;
      row.getCell('D').border = border;
      row.getCell('E').border = border;
      row.getCell('F').border = border;
      row.getCell('G').border = border;
      row.getCell('H').border = border;
      row.getCell('I').border = border;
      row.getCell('J').border = border;
      row.getCell('K').border = border;
      row.getCell('L').border = border;
      row.getCell('M').border = border;
      row.getCell('N').border = border;
      row.getCell('O').border = border;
      row.getCell('P').border = border;
      row.getCell('Q').border = border;
      row.getCell('R').border = border;
      row.getCell('S').border = border;
      row.getCell('T').border = border;
      row.getCell('U').border = border;
      row.getCell('V').border = border;
      row.getCell('W').border = border;

      row.commit();
      let index = 0;
      for (let sale of sales.rows) {
        indexRow++;
        index++;
        row = sheet.getRow(indexRow);
        row.getCell('A').value = '02';
        row.getCell('B').value = index;

        let date_invoice = DateAndTime.format(sale.sales_date, 'DD/MM/YYYY');
        row.getCell('C').value = date_invoice;
        row.getCell('D').value = date_invoice;

        let type_document_sale = '01';
        if (sale.sales_document === 'BOLETA') {
          type_document_sale = '03';
        }
        row.getCell('E').value = type_document_sale;
        row.getCell('F').value = sale.sales_serie;
        row.getCell('G').value = sale.sales_correlative;

        row.getCell('J').value = '-';

        if (sale.sales_canceled == 1) {
          row.getCell('K').value = '0';
          row.getCell('L').value = '00000000000';
          row.getCell('M').value = 'COMPROBANTE ANULADO';
          row.getCell('N').value = '-';
          row.getCell('O').value = '-';
          row.getCell('P').value = '-';
          row.getCell('Q').value = '-';
          row.getCell('R').value = '-';
          row.getCell('S').value = '-';
          row.getCell('T').value = '-';
          row.getCell('U').value = '-';
          row.getCell('V').value = '-';
          row.getCell('W').value = '';
        } else {
          let type_document = '';
          if (sale.customer_type_document === 'DNI') {
            type_document = '1'
          } else if (sale.customer_type_document === 'RUC') {
            type_document = '6';
          }

          row.getCell('K').value = type_document;
          row.getCell('L').value = sale.customer_document;

          let fullname = sale.customer_name.trim();
          if (sale.customer_surname) {
            fullname = fullname + ' ' + sale.customer_surname.trim();
          }
          row.getCell('M').value = fullname;
          row.getCell('N').value = '-';

          row.getCell('O').value = sale.sales_subtotal;
          row.getCell('O').numFmt = '0.00';

          row.getCell('P').value = '-';
          row.getCell('Q').value = '-';

          row.getCell('R').value = sale.sales_igv;
          row.getCell('R').numFmt = '0.00';

          row.getCell('S').value = '-';
          row.getCell('T').value = '-';

          row.getCell('U').value = sale.sales_total;
          row.getCell('U').numFmt = '0.00';

          row.getCell('V').value = '-';
          row.getCell('W').value = 'POR LA VENTA DE BID. CON AGUA';
        }

        row.commit();
      }

      response.header(`Content-Type`, `application/vnd.openxmlformats-officedocument.spreadsheetml.sheet`)
      response.header(`Content-Disposition`, `attachment; filename="template.xlsx`)

      return workbook.xlsx.write(response.response);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error obtener reporte de contabilidad.'
      });
    }
  }


}

module.exports = ExcelSaleAccountingController
