'use strict'

const Logger = use('Logger')
const Provider = use('App/Models/Provider')

class ProviderController {

	async index({response}) {
		try {
			const providers = await Provider.all()

			return providers

		} catch(error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al proveedores.'
			})
		}
	}

	async store({request, response}) {
		try {
			let data = request.all()

			let provider = await Provider.create({
				document: data.document,
				name: data.name,
				phone: data.phone,
				address: data.address,
				email: data.email		
			})

			const providers = await Provider.all()

			return response.status(201).json({
				created: provider,
				list: providers
			})

		} catch(error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al registrar proveedor.'
			})
		}
	}

	async update({params, request, response}){
		try {
			const {id} = params
        	let provider = await Provider.find(id)

        	const data = {
	            document: request.input('document'),
				name: request.input('name'),
				phone: request.input('phone'),
				address: request.input('address'),
				email: request.input('email')
	        }

	        provider.merge(data)

        	await provider.save()

        	let providerUpdated = await Provider.find(id)

        	return providerUpdated

		} catch(error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al actualizar proveedor.'
			})
		}
	}

	async inactivate({params, response}) {
		const {id} = params

		try{
			const provider = await Provider.find(id)

			let newStatus = null
	        if (provider.status === 'activo') {
	            newStatus = 'inactivo'
	        } else{
	            newStatus = 'activo'
	        }

	        provider.merge({
	            status: newStatus
	        })

	        await provider.save()

	        const providerUpdated = await Provider.find(id)

			return providerUpdated

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al cambiar estado de proveedor.'
			})
		}
	}

}

module.exports = ProviderController
