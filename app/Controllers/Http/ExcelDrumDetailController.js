'use strict'

const DateAndTime = require('date-and-time');
const Drum = use('App/Models/Drum');
const Excel = require('exceljs');
const Logger = use('Logger');

class ExcelDrumDetailController {

  async index ({request, response}) {
    try {
      const parameters = request.all();

      let query = Drum.query()
        .select([
          'drums.id',
          'customers.name as customer_name',
          'customers.surname as customer_surname',
          'customers.document as customer_document',
          'drums.register_date',
          'drums.product_id as product_id',
          'products.name as product_name',
          'drums.brand as product_brand',
          'drums.quantity',
          'drums.status as drum_status',
          'drums.loan_guide',
          'drums.observation',
          'drums.confirmed',
        ]).innerJoin('customers', 'customers.id', 'drums.customer_id')
        .innerJoin('products', 'products.id', 'drums.product_id');

      if (parameters.customer_id) {
        query.where('drums.customer_id', parameters.customer_id);
      }

      if (parameters.product_id) {
        query.where('drums.product_id', parameters.product_id);
      }

      if (parameters.date_from) {
        query.where('drums.register_date', '>=', parameters.date_from);
      }

      if (parameters.date_until) {
        query.where('drums.register_date', '<=', parameters.date_until);
      }

      let drums = await query.orderBy('drums.register_date', 'DESC').fetch();

      //---------------------------------------------------------------------
      const workbook = new Excel.Workbook();
      const sheet = workbook.addWorksheet('Worksheet');

      let indexRow = 1;
      let row = sheet.getRow(indexRow);
      //---- Titulos
      const font = {bold: true};

      row.getCell('A').value = 'CLIENTE';
      row.getCell('B').value = 'RUC / DNI';
      row.getCell('C').value = 'FECHA';
      row.getCell('D').value = 'PRODUCTO';
      row.getCell('E').value = 'MARCA';
      row.getCell('F').value = 'CANTIDAD';
      row.getCell('G').value = 'ESTADO';
      row.getCell('H').value = 'NÚMERO DE GUIA';
      row.getCell('I').value = 'CONFIRMADO';
      row.getCell('J').value = 'OBSERVACIÓN';

      sheet.getColumn('A').width = 50;
      sheet.getColumn('B').width = 15;
      sheet.getColumn('C').width = 11;
      sheet.getColumn('D').width = 33;
      sheet.getColumn('E').width = 20;
      sheet.getColumn('F').width = 11;
      sheet.getColumn('G').width = 15;
      sheet.getColumn('H').width = 17;
      sheet.getColumn('I').width = 13;
      sheet.getColumn('J').width = 40;

      row.getCell('A').font = font;
      row.getCell('B').font = font;
      row.getCell('C').font = font;
      row.getCell('D').font = font;
      row.getCell('E').font = font;
      row.getCell('F').font = font;
      row.getCell('G').font = font;
      row.getCell('H').font = font;
      row.getCell('I').font = font;
      row.getCell('J').font = font;

      const border = {
        bottom: {style: 'medium'}
      };

      row.getCell('A').border = border;
      row.getCell('B').border = border;
      row.getCell('C').border = border;
      row.getCell('D').border = border;
      row.getCell('E').border = border;
      row.getCell('F').border = border;
      row.getCell('G').border = border;
      row.getCell('H').border = border;
      row.getCell('I').border = border;
      row.getCell('J').border = border;

      row.commit();

      for (let item of drums.rows) {
        indexRow++;
        row = sheet.getRow(indexRow);

        let customer = item.customer_name;
        if (item.customer_surname) {
          customer = customer + ' ' + item.customer_surname;
        }
        let customer_document = '';
        if (item.customer_document) {
          customer_document = item.customer_document;
        }
        let register_date = DateAndTime.format(item.register_date, 'DD/MM/YYYY');
        let status = '';
        if (item.drum_status) {
          status = item.drum_status.toUpperCase();
        }
        let loan_guide = '';
        if (item.loan_guide) {
          loan_guide = item.loan_guide;
        }
        let confirmed = '';
        if (item.confirmed) {
          confirmed = item.confirmed.toUpperCase();
        }
        let observation = '';
        if (item.observation) {
          observation = item.observation;
        }
        row.getCell('A').value = customer;
        row.getCell('B').value = customer_document;
        row.getCell('C').value = register_date;
        row.getCell('D').value = item.product_name;
        row.getCell('E').value = item.product_brand;
        row.getCell('F').value = item.quantity;
        row.getCell('G').value = status;
        row.getCell('H').value = loan_guide;
        row.getCell('I').value = confirmed;
        row.getCell('J').value = observation;

        row.commit();

      }

      response.header(`Content-Type`, `application/vnd.openxmlformats-officedocument.spreadsheetml.sheet`)
      response.header(`Content-Disposition`, `attachment; filename="template.xlsx`)

      return workbook.xlsx.write(response.response);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al descargar reporte.'
      })
    }
  }
}

module.exports = ExcelDrumDetailController
