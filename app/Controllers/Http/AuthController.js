'use strict'

const User = use('App/Models/User')
const Role = use('App/Models/Role')
const Permission = use('App/Models/Permission')
const GeneralData = use('App/Models/GeneralData')

class AuthController {

	async register({request, auth, response}) {

		let user = await User.create(request.all())

		//generate token for user;
		let token = await auth.generate(user)

		Object.assign(user, token)

		return response.json(user)
	}

	async login({request, auth, response}) {
		let {username, password} = request.all()

		try{
			if (await auth.attempt(username, password)) {
				let user =  await User.findBy('username', username)
				let token = await auth.generate(user)

				let roles = await Role.query().fromUser(user.id).pluck('name')

				let permissions =  await Permission.query().fromRole(roles).pluck('permissions.name')

				let generalData = await GeneralData.query().where('status', 'activo').first()

				return response.json({
					user: user,
					token: token,
					roles: roles,
					permissions: permissions,
					company_name: generalData.company_name,
					company_document: generalData.company_document,
					company_address: generalData.company_address,
					igv: generalData.igv,
          company_data: generalData
				})
			}

		} catch(error) {
			console.log(error)
			return response.status(400).json(
			{
				message: 'Usted no esta registrado'
			})
		}
	}
}

module.exports = AuthController
