'use strict'

const CashMovement = use('App/Models/CashMovement');
const DetailPayment = use('App/Models/DetailPayment');
const Logger = use('Logger');
const Order = use('App/Models/Order');
const Taking = use('App/Models/Taking');

class TakingController {

  async index ({request, response}) {
    try {
      let parameters = request.all();

      let query = Taking.query()
        .select(
          'takings.*',
          'users.first_name',
          'users.last_name'
        ).innerJoin('users', 'users.id', 'takings.user_id');

      if (parameters.user_id) {
        query.where('takings.user_id', parameters.user_id);
      }

      if (parameters.date_from) {
        query.where('takings.register_date', '>=', parameters.date_from);
      }

      if (parameters.date_until) {
        query.where('takings.register_date', '<=',parameters.date_until);
      }

      let takings = await query.orderBy('takings.register_date', 'DESC')
        .fetch();

      return response.json(takings);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al listar recaudaciones.'
      })
    }
  }

  async store ({request, response}) {
    try {

      let user_id = request.input('user_id');
      let register_date = request.input('register_date');
      let total_sales = request.input('total_sales');
      let total_expenses = request.input('total_expenses');
      let total_delivered = request.input('total_delivered');

      let exist = await Taking.query()
        .where('user_id', user_id)
        .where('register_date', register_date)
        .count('* as total');

      if (exist[0].total >= 1) {
        return response.status(400).json({
          message_error: 'Ya existe recaudación para el repartidor seleccionado'
        });
      }

      let taking = await Taking.create({
        user_id: user_id,
        register_date: register_date,
        total_sales: total_sales,
        total_expenses: total_expenses,
        total_delivered: total_delivered
      });

      return response.json(taking);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al registrar recaudación.'
      })
    }
  }

  async calculate ({request, response}) {
    try {
      let user_id = request.input('user_id');
      let register_date = request.input('register_date');

      //total ventas
      /*
      let total_orders = await Order.query()
        .whereRaw('DATE(delivery_time) = ?', [register_date])
        .where('final_deliveryman', user_id)
        .where('status', 'Facturado')
        .where('type_sale', 'contado')
        .where('canceled', false)
        .getSum('total');
       */

      let total_orders = 0;
      let payments = await DetailPayment.query()
        .select(
          'detail_payments.*'
        )
        .innerJoin('payments', 'payments.id', 'detail_payments.payment_id')
        .innerJoin('orders', 'orders.id', 'payments.order_id')
        .where('orders.final_deliveryman', user_id)
        .where('orders.status', 'Facturado')
        .where('orders.type_sale', 'contado')
        .where('orders.canceled', false)
        .where('detail_payments.payment_method', 'CASH')
        .whereRaw('DATE(orders.delivery_time) = ?', [register_date])
        .fetch();

      for (let item of payments.rows) {
        total_orders += parseFloat(item.payment_amount);
      }

      if (!total_orders) {
        total_orders = 0;
      }

      //total egresos
      let total_expense = await CashMovement.query()
        .whereRaw('DATE(register_date) = ?', [register_date])
        .where('delivery_man', user_id)
        .where('type', 'expense')
        .getSum('amount');

      if (!total_expense) {
        total_expense = 0;
      }

      return response.json({
        total_orders,
        total_expense
      });

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al calcular recaudación.'
      })
    }
  }

  async destroy ({params, response}) {
    try {
      let taking_id = params.id;

      const taking = await Taking.find(taking_id);
      await taking.delete();

      return response.json({
        message: 'Recaudación eliminada de forma correcta'
      });

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al eliminar recaudación.'
      })
    }
  }
}


module.exports = TakingController
