'use strict'

const Customer = use('App/Models/Customer');
const DateAndTime = require('date-and-time');
const Logger = use('Logger');
const Notification = use('App/Models/Notification');
const TypeNotification = use('App/Models/TypeNotification');
const NotificationService = use('App/Services/NotificationsService');

class NotificationController {

  constructor() {
    this.notificationsService = new NotificationService;
  }

  async index ({request, response}) {
    try {

      let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD');

      let notifications = await Notification.query()
        .where('date_notification', currentDate)
        .orderBy('created_at', 'DESC')
        .fetch();

      return response.json(notifications);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al obtener notificaciones.'
      });
    }
  }

  async store ({request, response}) {
    try {

      let stored = await this.notificationsService.store();

      if (stored === false) {
        return response.status(400).json({
          message: 'Se detectó un error al crear notificaciones.'
        });
      }

      return response.json({
        message: 'Las notificaciones se crearon de forma correcta.'
      });

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al crear notificaciones.'
      });
    }
  }

  async getTypes ({request, response}) {
    try {

      let typeCustomer = await TypeNotification.find(1);
      let typeDebts = await TypeNotification.find(2);
      let typeBorrowed = await TypeNotification.find(4);

      let data = {
        customers_days: typeCustomer.days_evaluated,
        debts_days: typeDebts.days_evaluated,
        borrowed_days: typeBorrowed.days_evaluated
      }

      return response.json(data);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al listar tipos de notificaciones.'
      });
    }
  }

  async storeTypes ({request, response}) {
    try {
      let customers_days = request.input('customers_days');
      let debts_days = request.input('debts_days');
      let borrowed_days = request.input('borrowed_days');

      let typeCustomer = await TypeNotification.find(1);
      typeCustomer.days_evaluated = customers_days;
      typeCustomer.save();

      let typeDebts = await TypeNotification.find(2);
      typeDebts.days_evaluated = debts_days;
      typeDebts.save();

      let typeBorrowed = await TypeNotification.find(4);
      typeBorrowed.days_evaluated = borrowed_days;
      typeBorrowed.save();

      let data = {
        customers_days: typeCustomer.days_evaluated,
        debts_days: typeDebts.days_evaluated,
        borrowed_days: typeBorrowed.days_evaluated
      }

      return response.json(data);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al guardar tipos de notificaciones.'
      });
    }
  }

}

module.exports = NotificationController
