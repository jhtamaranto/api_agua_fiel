'use strict'

const Cash = use('App/Models/Cash');
const CashMovement = use('App/Models/CashMovement');
const CreditNote = use('App/Models/CreditNote');
const DateAndTime = require('date-and-time');
const Debt = use('App/Models/Debt')
const DetailOrder = use('App/Models/DetailOrder');
const DetailPayment = use('App/Models/DetailPayment');
const DetailPurchase = use('App/Models/DetailPurchase');
const Drum = use('App/Models/Drum');
const GeneralData = use('App/Models/GeneralData');
const Logger = use('Logger')
const Order = use('App/Models/Order')
const Payment = use('App/Models/Payment');
const Purchase = use('App/Models/Purchase')
const Product = use('App/Models/Product');
const Voucher = use('App/Models/Voucher');
const InvoiceService = use('App/Services/InvoiceService');
const Storehouse = use('App/Models/Storehouse');
const Inventory = use('App/Models/Inventory')
const Kardex = use('App/Models/KardexProduct')
const Mobility = use('App/Models/Mobility')
const moment = require('moment');

class ReportController {

  constructor() {
    this.invoiceService = new InvoiceService;
  }

	async purchases({request, response}) {
		try {
			let query = Purchase.query()

			let provider_id = request.input('provider_id')
			let from = request.input('from')
			let until = request.input('until')

			if (provider_id) {
				query.where('provider_id', parseInt(provider_id))
			}
			if(from && until) {
				query.whereRaw('DATE(voucher_date) >= ?', [from])
						.whereRaw('DATE(voucher_date) <= ?', [until])
			}

			let purchases = await query.where('status', 'activo')
										.with('provider')
										.with('supplies')
										.orderBy('voucher_date', 'DESC')
										.fetch()

			return response.status(200).json(purchases)

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener reporte de compras.'
			})
		}
	}

	async getDetailPurchase({request, response}) {
		try{
			let purchase_id = request.input('purchase_id')

			let detail = await DetailPurchase.query()
												.where('purchase_id', parseInt(purchase_id))
												.orderBy('id', 'ASC')
												.fetch()

			return response.status(200).json(detail)

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener detalle de compras.'
			})
		}
	}

	async debts({request, response}) {
		try {
			let query = Debt.query()

			let customer_id = request.input('customer_id')
			let from = request.input('from')
			let until = request.input('until')

			if (customer_id) {
				query.where('customer_id', parseInt(customer_id))
			}

			if(from && until) {
				query.whereRaw('DATE(entry_date) >= ?', [from])
						.whereRaw('DATE(entry_date) <= ?', [until])
			}

			let debts = await query.orderBy('entry_date', 'DESC')
									.with('customer')
									.with('payments')
									.fetch()

			return response.status(200).json(debts)

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener reporte de deudas.'
			})
		}
	}
  async debtsDatatable({request, response}) {
    try {
      let query = Debt.query()
      let customer_id = request.input('customer_id')
      let from = request.input('from')
      let until = request.input('until')
      let perPage=request.input('perPage')
      let pageSelected=request.input('pageSelected')

      if (customer_id) {
        query.where('customer_id', parseInt(customer_id))
      }

      if(from && until) {
        query.whereRaw('DATE(entry_date) >= ?', [from])
          .whereRaw('DATE(entry_date) <= ?', [until])
      }

      let resultData={data:[]}
      if (perPage==-1){
        resultData.data =  await query.orderBy('entry_date', 'DESC')
          .with('customer')
          .with('payments')
          .fetch()
      }else{
        let debts =  await query.orderBy('entry_date', 'DESC')
          .with('customer')
          .with('payments')
          .paginate(pageSelected,perPage);
        resultData=debts.toJSON()
      }
      return response.status(200).json(resultData)

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al obtener reporte de deudas.'
      })
    }
  }

	async sales({request, response}) {
		try {
			let query = Order.query()

			let customer_id = request.input('customer_id')
			let final_deliveryman = request.input('final_deliveryman')
			let from = request.input('from')
			let until = request.input('until')
      let type_document = request.input('type_document');

			if (customer_id) {
				query.where('customer_id', parseInt(customer_id))
			}

			if (type_document) {
			  query.where('type_document', type_document);
      }

			if (final_deliveryman) {
				query.where('final_deliveryman', parseInt(final_deliveryman))
			}

			if(from && until) {
				query.whereRaw('DATE(entry_time) >= ?', [from])
						.whereRaw('DATE(entry_time) <= ?', [until])
			}

			let sales = await query.where(function() {
          this.where('direct_sale', true)
            .orWhere('status', 'Facturado')
        })
        .where('batch_invoice', false)
        .with('customer')
        .with('deliveryend')
        .with('products')
        .with('user')
        .with('detail')
        .with('debt')
        .orderBy('entry_time', 'DESC')
        .fetch();

			return response.status(200).json(sales)

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener reporte de ventas.'
			})
		}
	}
  async salesDatatable({request, response}) {
    try {
      let query = Order.query()

      let customer_id = request.input('customer_id')
      let final_deliveryman = request.input('final_deliveryman')
      let from = request.input('from')
      let until = request.input('until')
      let perPage=request.input('perPage')
      let pageSelected=request.input('pageSelected')
      let type_document = request.input('type_document');

      if (customer_id) {
        query.where('customer_id', parseInt(customer_id))
      }

      if (type_document) {
        query.where('type_document', type_document);
      }

      if (final_deliveryman) {
        query.where('final_deliveryman', parseInt(final_deliveryman))
      }


      if (from && until) {
        query.whereRaw('DATE(entry_time) >= ?', [from])
          .whereRaw('DATE(entry_time) <= ?', [until])
      }

      let resultData={data:[]}
      if (perPage==-1){
        resultData.data = await query.where(function () {
          this.where('direct_sale', true)
            .orWhere('status', 'Facturado')
        })
          .where('batch_invoice', false)
          .with('customer')
          .with('deliveryend')
          .with('products')
          .with('user')
          .with('detail')
          .orderBy('entry_time', 'DESC')
          .fetch();
      }else{
        let sales = await query.where(function () {
          this.where('direct_sale', true)
            .orWhere('status', 'Facturado')
        })
          .where('batch_invoice', false)
          .with('customer')
          .with('deliveryend')
          .with('products')
          .with('user')
          .with('detail')
          .orderBy('entry_time', 'DESC')
          .paginate(pageSelected,perPage);
        resultData=sales.toJSON()
      }


      return response.status(200).json(resultData)

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        error: error.name,
        description: error.message,
        message: 'Se detectó un error al obtener reporte de ventas.'
      })
    }
  }

  async sumSalesDatatable({request, response}) {
    try {
      let query = Order.query()

      let customer_id = request.input('customer_id')
      let final_deliveryman = request.input('final_deliveryman')
      let from = request.input('from')
      let until = request.input('until')
      let type_document = request.input('type_document');

      if (customer_id) {
        query.where('customer_id', parseInt(customer_id))
      }

      if (type_document) {
        query.where('type_document', type_document);
      }

      if (final_deliveryman) {
        query.where('final_deliveryman', parseInt(final_deliveryman))
      }

      if (from && until) {
        query.whereRaw('DATE(entry_time) >= ?', [from])
          .whereRaw('DATE(entry_time) <= ?', [until])
      }

      let resultData = await query.where(function () {
        this.where('direct_sale', true)
          .orWhere('status', 'Facturado')
      })
        .select('total','canceled','type_sale')
        .where('batch_invoice', false)
        .orderBy('entry_time', 'DESC')
        .fetch();
      return response.status(200).json(resultData)
    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        error: error.name,
        description: error.message,
        message: 'Se detectó un error al obtener reporte de ventas.'
      })
    }
  }

  async ordersDatatable({request, response}) {
    try {
      let query = Order.query()

      let customer_id = request.input('customer_id')
      let final_deliveryman = request.input('final_deliveryman')
      let status = request.input('status')
      let from = request.input('from')
      let until = request.input('until')
      let perPage=request.input('perPage')
      let pageSelected=request.input('pageSelected')
      if (customer_id) {
        query.where('customer_id', parseInt(customer_id))
      }

      if (final_deliveryman) {
        query.where(function(){
          this.where('deliveryman', parseInt(final_deliveryman))
            .orWhere('final_deliveryman', parseInt(final_deliveryman))
        })
      }

      if (status){
        query.where('status', status)
      }

      if(from && until) {
        query.whereRaw('DATE(entry_time) >= ?', [from])
          .whereRaw('DATE(entry_time) <= ?', [until])
      }
      let resultData={data:[]}
      if (perPage==-1){
        resultData.data = await query.whereIn('status', ['Registrado', 'Anulado', 'Entregado', 'En reparto'])
          .with('customer')
          .with('deliverystart')
          .with('deliveryend')
          .with('products')
          .with('detail')
          .orderBy('entry_time', 'DESC')
          .fetch()
      }else{
        let orders =  await query.whereIn('status', ['Registrado', 'Anulado', 'Entregado', 'En reparto'])
          .with('customer')
          .with('deliverystart')
          .with('deliveryend')
          .with('products')
          .with('detail')
          .orderBy('entry_time', 'DESC')

          .paginate(pageSelected,perPage)        ;
        resultData=orders.toJSON()
      }
      return response.status(200).json(resultData)
    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al obtener reporte de pedidos.'
      })
    }
  }
  async orders({request, response}) {
		try {
			let query = Order.query()

			let customer_id = request.input('customer_id')
			let final_deliveryman = request.input('final_deliveryman')
			let status = request.input('status')
			let from = request.input('from')
			let until = request.input('until')

			if (customer_id) {
				query.where('customer_id', parseInt(customer_id))
			}

			if (final_deliveryman) {
				query.where(function(){
					this.where('deliveryman', parseInt(final_deliveryman))
						.orWhere('final_deliveryman', parseInt(final_deliveryman))
				})
			}

			if (status){
				query.where('status', status)
			}

			if(from && until) {
				query.whereRaw('DATE(entry_time) >= ?', [from])
						.whereRaw('DATE(entry_time) <= ?', [until])
			}

			let orders = await query.whereIn('status', ['Registrado', 'Anulado', 'Entregado', 'En reparto'])
        .with('customer')
        .with('deliverystart')
        .with('deliveryend')
        .with('products')
        .with('detail')
        .orderBy('entry_time', 'DESC')
        .fetch()

			return response.status(200).json(orders)

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener reporte de pedidos.'
			})
		}
	}

	async cash({request, response}) {
		try {
			let query = Cash.query()

			let from = request.input('from')
			let until = request.input('until')

			if(from && until) {
				query.whereRaw('DATE(register_date) >= ?', [from])
						.whereRaw('DATE(register_date) <= ?', [until])
			}

			let cash = await query.orderBy('register_date', 'DESC').fetch()

			return response.status(200).json(cash)

		} catch (error) {
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al obtener reporte de caja.'
			})
		}
	}

	async cancelSale ({auth, request, response}) {
	  try {
      let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');

      let user = await auth.getUser();

	    let order_id = request.input('order_id');
	    let reason = request.input('reason');
	    let generate_credit_note = request.input('generate_credit_note');

	    let order = await Order.find(order_id);

      order.canceled = true;
      order.canceled_user = user.id;
      order.canceled_reason = reason;
      order.canceled_date = currentDate;
      order.save();

      if (generate_credit_note == true) {
        if (order.send_fe == 1 || order.send_fe == true ) {
          let type_document = 'NOTA DE CREDITO';
          let type_serie = '';
          if (order.type_document === 'BOLETA') {
            type_serie = 'B';
          } else if (order.type_document === 'FACTURA') {
            type_serie = 'F';
          }

          let voucher = await Voucher.query()
            .where('type', type_document)
            .where('correlative', 'like', type_serie + '%')
            .first();

          await Voucher.query()
            .where('id', voucher.id)
            .increment('current_number', 1);

          let serie = '';
          let correlative = '';
          if (voucher) {
            serie = voucher.correlative;
            let stringCorrelative = voucher.current_number.toString()
            correlative = stringCorrelative.padStart(8, '0');
          }

          let creditNote = await CreditNote.create({
            order_id: order_id,
            reason: reason,
            user_id: user.id,
            type_document: type_document,
            serie: serie,
            correlative: correlative
          });

          let enterprise = await GeneralData.find(1);
          if (enterprise) {
            if (enterprise.user_fe && enterprise.password_fe) {
              let invoiceSend = await this.invoiceService.sendCreditNoteToSenda(creditNote);
              if (invoiceSend == '0000-CPE ha sido aceptada') {
                creditNote.send_fe = true;
                await creditNote.save();
              }
            }
          }
        }
      } else {
        if (order.type_document === 'BOLETA' || order.type_document === 'FACTURA') {
          if (order.send_fe || order.send_fe == 1) {
            let enterprise = await GeneralData.find(1);
            if (enterprise) {
              if (enterprise.user_fe && enterprise.password_fe) {
                await this.invoiceService.anularComprobante(order.id);
              }
            }
          }
        }
      }

      //eliminar movimiento de caja
      await CashMovement.query()
        .where('order_id', order.id)
        .delete();

      let payment = await Payment.query()
        .where('order_id', order.id)
        .first();

      if (payment) {
        //eliminar detalle de pago
        await DetailPayment.query()
          .where('payment_id', payment.id)
          .delete();

        //eliminar pago
        await payment.delete();
      }

      //Si fue generada desde deudas => dejar las deudas como pendientes
      if (order.source === 'debts') {
        let debts = await Debt.query()
          .where('order_id_generated', order.id)
          .fetch();

        for (let debtItem of debts.rows) {
          let debtData = await Debt.find(debtItem.id);
          debtData.status = 'pendiente';
          debtData.amount_paid = 0;
          debtData.amount_pending = debtData.amount;
          debtData.payment_date = null;
          await debtData.save();
        }
      }

      //devolver stock -------------------------------------------------------------------------
      let storehouse = null;

      if (order.direct_sale) {
        //almacen principal
        storehouse = await Storehouse.query().where('main', true).where('active', true).first();
      } else {
        //almacen de und movil
        let mobility = await Mobility.query()
          .where('user_id', order.final_deliveryman)
          .first();

        storehouse = await Storehouse.query()
          .where('mobility_id', mobility.id)
          .where('active', true)
          .first();
      }
      let products = await DetailOrder.query().where('order_id', order.id).fetch();

      for (let product of products.rows) {
        let quantity = product.quantity;
        let type = product.type_product;

        let inventory = await Inventory.query()
          .where('storehouse_id', storehouse.id)
          .where('product_id', product.product_id)
          .where('type', type)
          .first();

        let new_stock_previous = inventory.stock;
        let new_stock = inventory.stock + quantity;
        inventory.merge({
          stock_previous: new_stock_previous,
          stock: new_stock
        });
        await inventory.save();

        //Registrar ingreso en kardex
        await Kardex.create({
          inventory_id: inventory.id,
          register_date: currentDate,
          description: 'Anulación de venta',
          type: 'INGRESO',
          subtype: 'ANULACIÓN DE VENTA',
          quantity: quantity,
          stock: inventory.stock
        });

        if (order.direct_sale) {
          if (type === 'producto') {
            let itemProduct = await Product.query()
              .where('id', product.product_id)
              .first();

            itemProduct.merge({
              stock: new_stock
            })
            await itemProduct.save();
          }
        }
      }

      //eliminar prestamos y devoluciones
      await Drum.query()
        .where('order_id', order.id)
        .delete();

      return response.json(order);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al anular factura.'
      })
    }
  }

  async creditNotes ({request, response}) {
    try {
      let query = CreditNote.query();

      let from = request.input('from');
      let until = request.input('until');

      if(from && until) {
        query.whereRaw('DATE(created_at) >= ?', [from])
          .whereRaw('DATE(created_at) <= ?', [until])
      }

      let credit_notes = await query.with('order')
        .with('user')
        .orderBy('created_at', 'DESC')
        .fetch();

      return response.json(credit_notes);

    } catch (error) {
      Logger.error(error.name + ', ' + error.message)
      return response.status(400).json({
        message: 'Se detectó un error al listar notas de crédito.'
      })
    }
  }
}

module.exports = ReportController
