'use strict'

const CostHistory = use('App/Models/CostHistory')
const DateAndTime = require('date-and-time');
const DetailPurchase = use('App/Models/DetailPurchase')
const Database = use('Database')
const KardexSupply = use('App/Models/KardexSupply')
const Logger = use('Logger')
const ProviderAccount = use('App/Models/ProviderAccount')
const Purchase = use('App/Models/Purchase')
const Supply = use('App/Models/Supply')

class PurchaseController {

	async store({request, response}) {
		const transaction = await Database.beginTransaction()
		let data = request.all()
		try{
			let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');

			let term_days = data.term_days
			let payment_date = data.payment_date

			let purchase = await Purchase.create({
				provider_id: data.provider_id,
				voucher_date: data.voucher_date,
				serie: data.serie,
				correlative: data.correlative,
				is_credit: data.is_credit,
				term_days: term_days,
				payment_date: payment_date,
				subtotal: data.subtotal,
				igv: data.igv,
				total: data.total,
				is_paid: data.is_paid
			}, transaction)

			let supplies = data.supplies
			for (let i = 0; i < supplies.length; i++) {
				let supplyPurchased = await DetailPurchase.create({
					purchase_id: purchase.id,
					supply_id: supplies[i].id,
					supply_name: supplies[i].name,
					quantity: parseInt(supplies[i].quantity),
					price: parseFloat(supplies[i].price),
					subtotal: parseFloat(supplies[i].subtotal)
				}, transaction)

				let supply = await Supply.find(supplies[i].id)
				//Register Kardex
				let supplyKardex = await KardexSupply.create({
					supply_id: supplyPurchased.supply_id,
					operation_date: currentDate,
					type: 'INGRESO',
					sub_type: 'COMPRA',
					operation_id: purchase.id,
					type_document: 'FACTURA',
					serie: purchase.serie,
					correlative: purchase.correlative,
					quantity: parseFloat(supplyPurchased.quantity),
					stock: parseFloat(supply.stock) + parseFloat(supplyPurchased.quantity),
					cost: supplyPurchased.price,
					total: supplyPurchased.subtotal
				}, transaction)

				//Register cost history
				await CostHistory.create({
					supply_id: supply.id,
					registration_date: currentDate,
					previous_cost: supply.cost,
					new_cost: supplyPurchased.price
				}, transaction)

				supply.stock = parseFloat(supply.stock) + parseFloat(supplyKardex.quantity)
				supply.cost = parseFloat(supplyPurchased.price)
				await supply.save(transaction)

			}

			if(data.is_credit){
				await ProviderAccount.create({
					provider_id: data.provider_id,
					purchase_id: purchase.id,
					entry_date: purchase.voucher_date,
					amount: purchase.total,
					amount_pending: purchase.total,
					amount_paid: 0,
					term_days: term_days,
					date_agreement: payment_date
				}, transaction)
			}

			await transaction.commit()

			return purchase

		} catch(error) {
			await transaction.rollback()
			Logger.error(error.name + ', ' + error.message)
			return response.status(400).json({
				message: 'Se detectó un error al registrar compra.'
			})
		}
	}
}

module.exports = PurchaseController
