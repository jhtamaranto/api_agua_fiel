'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class DetailOrder extends Model {

	static scopeSummary(query, product_id = null) {
		query.innerJoin('orders', 'orders.id', 'detail_orders.order_id')

		if(product_id) {
			query.where('detail_orders.product_id', product_id)
		}

		return query.where('orders.status', 'Facturado')
	}

}

module.exports = DetailOrder
