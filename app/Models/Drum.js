'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Drum extends Model {

  static get table () {
    return 'drums';
  }

}

module.exports = Drum
