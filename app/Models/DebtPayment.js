'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Database = use('Database')
const Model = use('Model')

class DebtPayment extends Model {

	static get table () {
		return 'debt_payments'
	}

	static get hidden(){
		return ['created_at', 'updated_at']
	}

	static paymentByDate(dateFilter, cash_id = null) {
		let sql = "select id, payment_date as register_date, observations as description, amount_payment as amount"
					+ " from debt_payments"
					+ " where DATE(payment_date) = '" +dateFilter + "'"
					+ " and cash_id = " + cash_id
          + " and method_payment = 'CASH'"
					+ " order by 2 desc";

		return Database.raw(sql)
	}

}

module.exports = DebtPayment
