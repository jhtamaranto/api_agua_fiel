'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Database = use('Database')
const Model = use('Model')

class DetailPayment extends Model {

	static get table () {
		return 'detail_payments'
	}

	static get hidden(){
		return ['created_at', 'updated_at']
	}

	static paymentByMethod(method, dateFilter, cash_id = null) {
		let filter = " and detail_payments.payment_method = 'CASH' "
		if (method != 'CASH') {
			filter = " and detail_payments.payment_method in ('TRANSFERENCIA', 'YAPE') "
		}
		let sql = "select detail_payments.id, detail_payments.created_at as register_date, "
					+ " detail_payments.payment_amount as amount, payments.order_id, detail_payments.payment_method"
					+ " from detail_payments"
					+ " inner join payments on detail_payments.payment_id = payments.id "
					+ " where DATE(detail_payments.created_at) = '" + dateFilter + "' "
					+ " and detail_payments.cash_id = " + cash_id
					+ filter
					+ " order by 2 desc;"

		return Database.raw(sql)
	}

}

module.exports = DetailPayment
