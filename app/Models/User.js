'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

const Database = use('Database')

class User extends Model {

  static get traits () {
    return [
      '@provider:Adonis/Acl/HasRole',
      '@provider:Adonis/Acl/HasPermission'
    ]
  }

  static boot () {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })
  }

  static get hidden(){
    return ['created_at', 'updated_at', 'password']
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/Token')
  }

  roles() {
    return this.belongsToMany('App/Models/Role')
  }

  static deliveryUsers() {
    const sql = "select tabla.id, tabla.fullname, (tabla.slopes + tabla.final_slopes) as slopes"
                + " from"
                + " ("
                + " select users.id, CONCAT(users.first_name, ' ', users.last_name) as fullname,"
                + " ("
                + " select count(orders.id)"
                + " from orders"
                + " where LOWER(orders.status) = 'en reparto'"
                + " and orders.deliveryman = users.id"
                + " ) as slopes,"
                + " ("
                + " select count(orders.id)"
                + " from orders"
                + " where LOWER(orders.status) = 'en reparto'"
                + " and orders.final_deliveryman = users.id"
                + " ) as final_slopes"
                + " from users"
                + " inner join role_user on users.id = role_user.user_id"
                + " inner join roles on role_user.role_id = roles.id"
                + " where users.status = 'activo'"
                + " and roles.status = 'activo'"
                + " and LOWER(roles.name) = 'repartidor'"
                + " and users.id in ("
                + " select mobilities.user_id  from mobilities where mobilities.user_id is not null"
                + " )"
                + " order by 3"
                + " ) as tabla"
                + " order by 3"

    return Database.raw(sql)
  }

  static getRepartidores() {
    const sql = "select users.id as value, CONCAT(users.first_name, ' ', users.last_name) as text"
                + " from users"
                + " inner join role_user on users.id = role_user.user_id"
                + " inner join roles on role_user.role_id = roles.id"
                + " where users.status = 'activo'"
                + " and roles.status = 'activo'"
                + " and LOWER(roles.name) = 'repartidor'"
                + " and users.id not in ("
                  + " select mobilities.user_id  from mobilities where mobilities.user_id is not null"
                + " )"
                + " order by 2"
    return Database.raw(sql)
  }

  static getPrevendedores(){
    let sql=`select *from users u join role_user ru on u.id = ru.user_id
inner join roles r on ru.role_id = r.id
where r.slug='prevendedor'`;
    return Database.raw(sql)
  }

}

module.exports = User
