'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Database = use('Database')
const Model = use('Model')

class Inventory extends Model {

	static get table() {
		return 'inventories'
	}

	storehouse() {
		return this.belongsTo('App/Models/Storehouse', 'storehouse_id', 'id')
	}

	product() {
		return this.belongsTo('App/Models/Product', 'product_id', 'id')
	}

	static listProducts (storehouse_id) {
	  let query = "SELECT"
    + " inventories.id,"
    + " inventories.storehouse_id,"
    + " inventories.product_id,"
    + " inventories.stock,"
    + " inventories.stock_empty,"
    + " inventories.type,"
    + " products.name,"
    + " products.has_return"
    + " FROM inventories"
    + " inner join products on products.id = inventories.product_id and inventories.type = 'producto'"
    + " where inventories.storehouse_id = ? "
    + " and inventories.active = 1"
    + " and inventories.deleted_at is null"
    + " UNION"
    + " SELECT"
    + " inventories.id,"
    + " inventories.storehouse_id,"
    + " inventories.product_id,"
    + " inventories.stock,"
    + " inventories.stock_empty,"
    + " inventories.type,"
    + " assets.name,"
    + " 0 as has_return"
    + " FROM inventories"
    + " inner join assets on assets.id = inventories.product_id and inventories.type = 'activo'"
    + " where inventories.storehouse_id = ? "
    + " and inventories.active = 1"
    + " and inventories.deleted_at is null";

    return Database.raw(query, [storehouse_id, storehouse_id]);
  }
}

module.exports = Inventory
