'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Asset extends Model {

	static get hidden(){
		return ['created_at', 'updated_at']
	}

	unit () {
		return this.belongsTo('App/Models/Unit', 'unit_id', 'id')
	}
	
}

module.exports = Asset
