'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Database = use('Database');
const Model = use('Model')

class BorrowedAsset extends Model {

  static get table () {
    return 'borrowed_assets';
  }

  static borrowed (days_numbers) {

    let query = "SELECT"
    + " customers.id,"
    + " customers.type_document,"
    + " customers.document,"
    + " customers.name customer_name,"
    + " customers.surname,"
    + " borrowed_assets.name,"
    + " borrowed_assets.quantity,"
    + " DATEDIFF(CURRENT_DATE, DATE(borrowed_assets.created_at)) as days_passed"
    + " FROM borrowed_assets"
    + " inner join customers on customers.id = borrowed_assets.customer_id"
    + " where borrowed_assets.status = 'prestado'"
    + " having days_passed >= ?";

    return Database.raw(query, [days_numbers]);

  }
}

module.exports = BorrowedAsset
