'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Role extends Model {

	static get hidden(){
		return ['created_at', 'updated_at']
	}

	permissions() {
		return this.belongsToMany('App/Models/Permission')
	}

	static scopeFromUser(query, user) {
		return query.select('roles.name')
					.innerJoin('role_user', 'roles.id', 'role_user.role_id')
					.where('role_user.user_id', user)
	}
}

module.exports = Role
