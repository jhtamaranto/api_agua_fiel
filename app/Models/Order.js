'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Database = use('Database')
const Model = use('Model')

class Order extends Model {

	static get hidden(){
		return ['created_at', 'updated_at']
	}

	customer() {
		return this.belongsTo('App/Models/Customer', 'customer_id', 'id')
	}

	deliverystart() {
		return this.belongsTo('App/Models/User', 'deliveryman', 'id')
	}

	deliveryend() {
		return this.belongsTo('App/Models/User', 'final_deliveryman', 'id')
	}

  user() {
    return this.belongsTo('App/Models/User', 'user_id', 'id')
  }

  debt() {
    return this.belongsTo('App/Models/Debt', 'id', 'order_id')
  }

	products() {
		return this.belongsToMany('App/Models/Product')
					.pivotTable('detail_orders')
					.withPivot([
					  'quantity',
            'base_price',
            'discount_amount',
            'discount_type',
            'subtotal',
            'discount_value',
            'discount_group',
            'discount_item',
            'type_sale',
            'set_discount_customer'
          ]);
	}

  detail () {
    return this.hasMany('App/Models/DetailOrder', 'id', 'order_id');
  }

	static salesMonth(month, year) {
		let sql = "select DATE(entry_time) as entry_time, DATE_FORMAT(entry_time, '%d %b') AS date_order, sum(total) as total"
					+ " from orders"
					+ " where MONTH(entry_time) = " + month
					+ " AND YEAR(entry_time) = " + year
					+ " group by 1, 2"
					+ " order by 1";

		return Database.raw(sql)
	}

	static summary() {
		let sql = "select status, count(*) as total"
				+ " from orders"
				+ " group by status;";

		return Database.raw(sql)
	}
}

module.exports = Order
