'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProviderPayment extends Model {
	static get table(){
		return 'provider_payments'
	}

	static get hidden(){
		return ['created_at', 'updated_at']
	}

	account() {
		return this.belongsTo('App/Models/ProviderAccount', 'provider_account_id', 'id')
	}
}

module.exports = ProviderPayment
