'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CreditNote extends Model {

  static get table () {
    return 'credit_notes';
  }

  order() {
    return this.belongsTo('App/Models/Order', 'order_id', 'id')
  }

  user() {
    return this.belongsTo('App/Models/User', 'user_id', 'id')
  }
}

module.exports = CreditNote
