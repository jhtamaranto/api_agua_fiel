'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Database = use('Database')
const Model = use('Model')

class Cash extends Model {

	static get table() {
		return 'cash'
	}

	static report(from, until) {
		let where = ''
		if (from && until) {
			where = " where DATE(cash.register_date) BETWEEN '" + from + "' AND '" + until + "'"
		}
		let sql = "select cash.id, cash.register_date, cash.amount_open, cash.total_amount,"
					+ " cash.close_date, cash.status, cash.amount_close_cash, amount_close_cards,"
					+ " ("
						+ " select sum(cm.amount)"
						+ " from cash_movements as cm"
						+ " where cm.cash_id = cash.id"
						+ " and cm.type = 'income'"
					+ " ) as income,"
					+ " ("
						+ " select sum(cm.amount)"
						+ " from cash_movements as cm"
						+ " where cm.cash_id = cash.id"
						+ " and cm.type = 'expense'"
					+ " ) as expense"
					+ " from cash"
					+ where
					+ " order by cash.register_date desc;"

		return Database.raw(sql)
	}

	static amountByUser (cash_id) {
	  let sql = "select"
    + " users.first_name, "
    + " users.last_name, "
    + " tabla.* "
    + " from "
    + " ( "
    + " SELECT "
    + " cash_movements.cash_id, "
    + " cash_movements.user_id, "
    + " cash_movements.type, "
    + " sum(cash_movements.amount) as total "
    + " FROM cash_movements "
    + " where cash_id = ? "
    + " group by cash_movements.cash_id, cash_movements.user_id, cash_movements.type "
    + " ) as tabla "
    + " inner join users on users.id = tabla.user_id ";

    return Database.raw(sql, [cash_id]);
  }
}

module.exports = Cash
