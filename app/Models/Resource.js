'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Resource extends Model {

	static get hidden(){
		return ['created_at', 'updated_at', 'deleted_at']
	}

	permissions() {
		return this.hasMany('App/Models/Permission')
	}
	
}

module.exports = Resource
