'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TypeNotification extends Model {

  static get table () {
    return 'type_notifications';
  }
}

module.exports = TypeNotification
