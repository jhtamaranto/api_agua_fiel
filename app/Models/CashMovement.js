'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CashMovement extends Model {

	static get table() {
		return 'cash_movements'
	}
}

module.exports = CashMovement
