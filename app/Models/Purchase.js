'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Purchase extends Model {

	static get table () {
		return 'purchases'
	}

	static get hidden(){
		return ['created_at', 'updated_at']
	}

	provider() {
		return this.belongsTo('App/Models/Provider', 'provider_id', 'id')
	}

	supplies() {
		return this.hasMany('App/Models/DetailPurchase', 'id', 'purchase_id')
	}

}

module.exports = Purchase
