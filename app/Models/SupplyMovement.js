'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SupplyMovement extends Model {

	static get table () {
		return 'supplies_movements'
	}

	static get hidden(){
		return ['created_at', 'updated_at']
	}

	supply() {
		return this.belongsTo('App/Models/Supply', 'supply_id', 'id')
	}

}

module.exports = SupplyMovement
