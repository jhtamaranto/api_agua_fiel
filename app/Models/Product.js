'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Database = use('Database')
const Model = use('Model')

class Product extends Model {

	static get hidden(){
		return ['created_at', 'updated_at']
	}

	static soldProducts(month, year) {
		let sql = "select products.name, sum(detail_orders.quantity) as total"
					+ " from detail_orders"
					+ " inner join products on detail_orders.product_id = products.id"
					+ " where MONTH(detail_orders.created_at) = " + month
					+ " AND YEAR(detail_orders.created_at) = " + year
					+ " group by 1"
					+ " order by 1";

		return Database.raw(sql)
	}

	static catalogue () {
	  let query = "SELECT CONCAT('p-', id) as id, name, unit_price, image,"
    + " status,"
    + " wholesale_price,"
    + " preferential_price,"
    + " has_return,"
    + " stock,"
    + " 'producto' as type_product"
    + " FROM products"
    + " where deleted_at is null"
    + " UNION"
    + " SELECT CONCAT('a-', id) as id, name, price as unit_price, null as image,"
    + " status,"
    + " 0 as wholesale_price,"
    + " 0 as preferential_price,"
    + " 1 as has_return,"
    + " quantity as stock,"
    + " 'activo' as type_product"
    + " FROM assets"
    + " where loan = 1";

    return Database.raw(query);

  }

}

module.exports = Product
