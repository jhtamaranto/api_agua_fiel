'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Taking extends Model {
  static get table () {
    return 'takings';
  }
}

module.exports = Taking
