'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Permission extends Model {

	static get hidden(){
	    return ['created_at', 'updated_at', 'password']
	}

	static scopeFromRole(query, roles){
		return query.select('permissions.name')
					.innerJoin('permission_role', 'permissions.id', 'permission_role.permission_id')
					.innerJoin('roles', 'permission_role.role_id', 'roles.id')
					.whereIn('roles.name', roles)
	}

	resource () {
		return this.belongsTo('App/Models/Resource', 'resource_id', 'id')
	}
  
}

module.exports = Permission
