'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Database = use('Database')

class ProviderAccount extends Model {

	static get table() {
		return 'provider_accounts'
	}

	static get hidden(){
		return ['created_at', 'updated_at']
	}

	static get computed(){
		return ['condition']
	}

	purchase() {
		return this.belongsTo('App/Models/Purchase', 'purchase_id', 'id')
	}

	static pendingDebts(filter = null){
		let where = ''
		if(filter.provider_id){
			where = " where providers.id = " + filter.provider_id + " "
		} 

		let sql = "select tabla.id, tabla.document, tabla.name, tabla.total_debt, tabla.paid_debt, "
					+ " (tabla.total_debt - tabla.paid_debt) as balance"
					+ " from "
					+ " ( "
					+ " select providers.id, providers.document, providers.name, "
					+ " sum(pa.amount) as total_debt, sum(pa.amount_paid) paid_debt"
					+ " from provider_accounts as pa"
					+ " inner join providers on pa.provider_id = providers.id"
					+ where
					+ " group by providers.id, providers.name"
					+ " ) as tabla"
					+ " order by 6 desc;"

		return Database.raw(sql)
	}

	getCondition({date_agreement, amount_pending}) {
		const now = new Date()
        let status = ''
        let expiration = new Date(date_agreement)
        let term = Math.round((expiration.getTime() - now.getTime())/(1000*60*60*24))
        if (amount_pending > 0) {
            if (term <= 0) {
                status = 'vencida'
            } else {
                status = 'pendiente'
            }        
        } else {
            status = 'pagada'
        }
        return status
	}

	payments() {
		return this.hasMany('App/Models/ProviderPayment')
	}

}

module.exports = ProviderAccount
