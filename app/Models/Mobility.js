'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Mobility extends Model {

	static get hidden(){
		return ['created_at', 'updated_at']
	}

	user () {
		return this.belongsTo('App/Models/User', 'user_id', 'id')
	}
	
}

module.exports = Mobility
