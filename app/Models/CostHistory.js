'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CostHistory extends Model {

	static get table() {
		return 'cost_histories'
	}

	static get hidden(){
		return ['created_at', 'updated_at']
	}
}

module.exports = CostHistory
