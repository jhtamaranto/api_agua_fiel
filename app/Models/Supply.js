'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Supply extends Model {

	static get hidden(){
		return ['created_at', 'updated_at']
	}

	category () {
		return this.belongsTo('App/Models/Category', 'category_id', 'id')
	}

	unit () {
		return this.belongsTo('App/Models/Unit', 'unit_id', 'id')
	}

	movements() {
		return this.hasMany('App/Models/SupplyMovement', 'supply_id', 'id')
	}
	
}

module.exports = Supply
