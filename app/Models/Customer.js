'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Database = use('Database');
const Model = use('Model');

class Customer extends Model {

	static get hidden(){
		return ['created_at', 'updated_at', 'deleted_at']
	}

	static get deleteTimestamp () {
		return 'deleted_at'
  }

  static get computed () {
    return ['fullname'];
  }

  getFullname ({name, surname}) {
    let full_name = name;
    if (surname) {
      full_name += ' ' + surname;
    }

    return full_name;
  }

	debts() {
		return this.hasMany('App/Models/Debt')
	}

	orders() {
		return this.hasMany('App/Models/Order')
	}

	addresses() {
	  return this.hasMany('App/Models/CustomerAddress');
  }

  prices() {
	  return this.hasMany('App/Models/CustomerPrice');
  }

	static lastPurchases (days_passed) {
	  let query = "select "
    + " customers.id,"
    + " customers.type_document,"
    + " customers.document,"
    + " customers.name,"
    + " customers.surname,"
    + " tabla.*,"
    + " DATEDIFF(CURRENT_DATE, DATE(tabla.last_date)) as days_passed"
    + " from"
    +" ("
      +" select"
      + " orders.customer_id,"
      + "max(orders.entry_time) as last_date"
      + " from orders"
      + " group by orders.customer_id"
    + " ) as tabla"
    + " inner join customers on customers.id = tabla.customer_id"
    + " having days_passed >= ?"
    + " order by days_passed desc";

    return Database.raw(query, [days_passed]);

  }

  static pendingDrums () {
	  let query = "SELECT"
      + " id,"
      + " type_document,"
      + " document,"
      + " name,"
      + " surname,"
      + " pending_drums"
      + " FROM `customers`"
      + " where pending_drums > 0"
      + " order by pending_drums desc";

    return Database.raw(query);
  }

}

module.exports = Customer
