'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

const Database = use('Database')

class Debt extends Model {

	static get table () {
		return 'debts'
	}

	static get hidden(){
		return ['created_at', 'updated_at']
	}

	static pendingDebts(filter = null){
		let where = " where debts.status in ('pendiente', 'pagada') ";
		if(filter.customer_id){
			where += " and debts.customer_id = " + parseInt(filter.customer_id) + " ";
		} else if(filter.type_document) {
			where += " and customers.type_document = '" + filter.type_document + "' ";
		}
		let sql = "select tabla.id, tabla.type_document, tabla.document, tabla.name, tabla.surname, tabla.total_debt, tabla.paid_debt, "
					+ " (tabla.total_debt - tabla.paid_debt) as balance"
					+ " from"
					+ " ("
					+ " select customers.id, customers.type_document, customers.document, customers.name, customers.surname, "
					+ " sum(debts.amount) as total_debt, sum(debts.amount_paid) paid_debt"
					+ " from debts"
					+ " inner join customers on debts.customer_id = customers.id"
					+ where
					+ " group by customers.id, customers.type_document"
					+ " ) as tabla"
					+ " order by 8 desc;"

		return Database.raw(sql)
	}

	payments() {
		return this.hasMany('App/Models/DebtPayment')
	}

	customer() {
		return this.belongsTo('App/Models/Customer', 'customer_id', 'id')
	}

	static toExpire(days_expire) {
	  let query = "select"
    + " customers.id,"
    + " customers.type_document,"
    + " customers.document,"
    + " customers.name,"
    + " customers.surname,"
    + " debts.amount_pending,"
    + " debts.due_date,"
    + " DATEDIFF(DATE(debts.due_date), CURRENT_DATE) as days_expire"
    + " from debts"
    + " inner join customers on customers.id = debts.customer_id"
    + " where debts.status = 'pendiente'"
    + " having days_expire <= ?"
    + " order by debts.due_date";

    return Database.raw(query, [days_expire]);
  }

}

module.exports = Debt
