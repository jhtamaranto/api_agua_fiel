'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CustomerPrice extends Model {
  static get table () {
    return 'customer_prices';
  }

  product () {
    return this.belongsTo('App/Models/Product', 'product_id', 'id');
  }
}

module.exports = CustomerPrice
