'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class BankOperation extends Model {
  static get table () {
    return 'bank_operations';
  }
}

module.exports = BankOperation
