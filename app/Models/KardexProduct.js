'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class KardexProduct extends Model {

	static get table() {
		return 'kardex_products'
	}

	inventory() {
		return this.belongsTo('App/Models/Inventory', 'inventory_id', 'id')
	}
}

module.exports = KardexProduct
