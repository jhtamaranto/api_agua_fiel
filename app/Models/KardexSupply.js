'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class KardexSupply extends Model {

	static get table() {
		return 'kardex_supplies'
	}

	static get hidden(){
		return ['created_at', 'updated_at']
	}
	
}

module.exports = KardexSupply
