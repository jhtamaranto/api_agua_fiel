'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

const Database = use('Database')

class Storehouse extends Model {
  static get table() {
    return 'storehouses'
  }

  mobility() {
    return this.belongsTo('App/Models/Mobility', 'mobility_id', 'id')
  }

  static getStorehouses(filter = null) {
    let where = ''
    if(filter.storehouse_id){
      where = " and storehouses.id = " + filter.storehouse_id + " "
    }

    let sql = "select storehouses.id, storehouses.name, concat(users.first_name, ' ', users.last_name) deliveryman,"
    		+ " users.id as deliveryman_id, mobilities.id as mobility, storehouses.main"
    		+ " from storehouses"
    		+ " left join mobilities on storehouses.mobility_id = mobilities.id"
    		+ " left join users on mobilities.user_id = users.id"
    		+ " where storehouses.active is true"
        + where
    		+ " order by storehouses.main desc;"

     return Database.raw(sql)
  }

  inventories() {
    return this.hasMany('App/Models/Inventory', 'id', 'inventory_id')
  }
}

module.exports = Storehouse
