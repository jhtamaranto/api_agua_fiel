'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CustomerAddress extends Model {
  static get table () {
    return 'customer_addresses';
  }
}

module.exports = CustomerAddress
