'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class DetailPurchase extends Model {

	static get table () {
		return 'detail_purchases'
	}

	static get hidden(){
		return ['created_at', 'updated_at']
	}

}

module.exports = DetailPurchase
