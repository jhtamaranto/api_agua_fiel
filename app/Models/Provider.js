'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Provider extends Model {

	static get table () {
		return 'providers'
	}

	static get hidden(){
		return ['created_at', 'updated_at']
	}

}

module.exports = Provider
