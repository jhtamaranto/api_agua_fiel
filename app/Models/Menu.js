'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Menu extends Model {

	static get table () {
		return 'menus'
	}

	resources() {
		return this.hasMany('App/Models/Resource')
	}

}

module.exports = Menu
