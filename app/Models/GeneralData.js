'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class GeneralData extends Model {

	static get table () {
		return 'general_data'
	}

	static get hidden(){
		return ['created_at', 'updated_at']
	}

}

module.exports = GeneralData
