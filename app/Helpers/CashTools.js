'use strict'

const calculateCashSum = async (dataCash) => {
	let sum = 0
	dataCash.forEach(element => {
		sum += element.amount
	})

	return sum
}

module.exports = {
	calculateCashSum
}