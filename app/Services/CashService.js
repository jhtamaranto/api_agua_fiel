'use strict';

const Cash = use('App/Models/Cash');
const CashMovement = use('App/Models/CashMovement');
const DateAndTime = require('date-and-time');
const Logger = use('Logger')

class CashService {

  async store (option, transaction, type, order_id, data) {
    try {
      let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');

      let amount = 0;
      let description = null;
      if (option === 'order') {
        amount = data.payment_amount;
        description = 'Pago de pedido #' + order_id;
      } else if (option === 'debt') {
        amount = data.amount_payment;
        description = 'Amortización de deuda del pedido #' + order_id;
      }

      await CashMovement.create({
        cash_id: data.cash_id,
        register_date: currentDate,
        type: type,
        amount: amount,
        description: description,
        user_id: data.user_id,
        order_id: order_id
      }, transaction);

      return true;

    } catch (error) {
      return false;
    }
  }

  async movementsByUser (cash_id) {
    try {
      let data = [];
      let movements = await Cash.amountByUser(cash_id);

      for (let movement of movements[0]) {
        let item = {
          user_id: movement.user_id,
          first_name: movement.first_name,
          last_name: movement.last_name,
          income: 0.00,
          expense: 0.00,
          cash: 0.00,
          card: 0.00,
          balance_favor: 0.00,
          debt: 0.00
        };

        let indexFounded = await this.existInArray(item.user_id, data);
        if (indexFounded > -1) {
          data[indexFounded][movement.type] = parseFloat(movement.total);
        } else {
          data.push(item);
          let index = await this.existInArray(item.user_id, data);
          data[index][movement.type] = parseFloat(movement.total);
        }
      }

      return data;

    } catch (error) {
      Logger.error(error.name + ', ' + error.message);
      return false;
    }
  }

  async existInArray (user_id, data) {
    let indexFounded = -1;
    data.forEach( (element, index) => {
      if (element.user_id == user_id) {
        indexFounded = index;
      }
    });

    return indexFounded;
  }
}

module.exports = CashService;
