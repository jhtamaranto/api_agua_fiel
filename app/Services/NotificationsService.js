'use strict';

const BorrowedAsset = use('App/Models/BorrowedAsset');
const Customer = use('App/Models/Customer');
const DateAndTime = require('date-and-time');
const Debt = use('App/Models/Debt');
const Logger = use('Logger');
const Notification = use('App/Models/Notification');
const TypeNotification = use('App/Models/TypeNotification');

class NotificationsService {

  async store () {
    try {
      let currentDate = DateAndTime.format(new Date(), 'YYYY-MM-DD');

      //notificacion: clientes q han dejado de comprar
      await this.customersWhoHaveNotBought(currentDate);

      //notificacion: deudas por vencer
      await this.unpaidDebts(currentDate);

      //bidones pendientes
      await this.pendingDrums(currentDate);

      await this.assetsBorrowed(currentDate);

      return true;

    } catch (error) {
      Logger.error(error.name + ', ' + error.message);
      return false;
    }
  }

  async customersWhoHaveNotBought (currentDate) {
    try {

      let type = await TypeNotification.find(1);

      if (type) {
        let days_passed = parseInt(type.days_evaluated);

        let numberNotifications = await Notification.query()
          .where('type_notification_id', type.id)
          .where('date_notification', currentDate)
          .getCount();

        if (numberNotifications === 0) {
          let lastPurchases = await Customer.lastPurchases(days_passed);
          for (let item of lastPurchases[0]) {
            let customer = item.name;
            if (item.type_document === 'DNI') {
              if (item.surname) {
                customer = customer + ' ' + item.surname;
              }
            }

            if (item.document) {
              customer = customer + ' (' + item.type_document + ': ' + item.document + ')';
            }

            let message = customer + ' no realiza compras hace ' + item.days_passed  + ' días.';

            await Notification.create({
              type_notification_id: type.id,
              date_notification: currentDate,
              messages: message,
              viewed: false
            });
          }
        }
      }

      return true;

    } catch (error) {
      Logger.error(error.name + ', ' + error.message);
      return false;
    }
  }

  async unpaidDebts (currentDate) {
    try {
      let type = await TypeNotification.find(2);

      if (type) {
        let days_expire = parseInt(type.days_evaluated);

        let numberNotifications = await Notification.query()
          .where('type_notification_id', type.id)
          .where('date_notification', currentDate)
          .getCount();

        if (numberNotifications === 0) {
          let debtsToExpire = await Debt.toExpire(days_expire);

          for (let item of debtsToExpire[0]) {
            let customer = item.name;
            if (item.type_document === 'DNI') {
              if (item.surname) {
                customer = customer + ' ' + item.surname;
              }
            }

            if (item.document) {
              customer = customer + ' (' + item.type_document + ': ' + item.document + ')';
            }

            let message = customer;
            if (parseInt(item.days_expire) < 0) {
              let days = parseInt(item.days_expire) * -1;
              message = message + ' tiene una deuda que ha vencido hace ' + days + ' días.';
            } else {
              message = message + ' tiene una deuda que vence dentro de ' + item.days_expire + ' días.';
            }

            await Notification.create({
              type_notification_id: type.id,
              date_notification: currentDate,
              messages: message,
              viewed: false
            });
          }
        }

      }

    } catch (error) {
      Logger.error(error.name + ', ' + error.message);
      return false;
    }
  }

  async pendingDrums (currentDate) {
    try {
      let type = await TypeNotification.find(3);

      if (type) {
        let numberNotifications = await Notification.query()
          .where('type_notification_id', type.id)
          .where('date_notification', currentDate)
          .getCount();

        if (numberNotifications === 0) {

          let drumsToReturn = await Customer.pendingDrums();

          for (let item of drumsToReturn[0]) {
            let customer = item.name;
            if (item.type_document === 'DNI') {
              if (item.surname) {
                customer = customer + ' ' + item.surname;
              }
            }

            if (item.document) {
              customer = customer + ' (' + item.type_document + ': ' + item.document + ')';
            }

            let message = customer + ' debe ' + item.pending_drums  + ' bidones.';

            await Notification.create({
              type_notification_id: type.id,
              date_notification: currentDate,
              messages: message,
              viewed: false
            });

          }
        }
      }
    } catch (error) {
      Logger.error(error.name + ', ' + error.message);
      return false;
    }
  }

  async assetsBorrowed (currentDate) {
    try {
      let type = await TypeNotification.find(4);

      if (type) {
        let numberNotifications = await Notification.query()
          .where('type_notification_id', type.id)
          .where('date_notification', currentDate)
          .getCount();

        if (numberNotifications === 0) {
          let borrowedAsset = await BorrowedAsset.borrowed(parseInt(type.days_evaluated));

          for (let item of borrowedAsset[0]) {
            let customer = item.customer_name;
            if (item.type_document === 'DNI') {
              if (item.surname) {
                customer = customer + ' ' + item.surname;
              }
            }

            if (item.document) {
              customer = customer + ' (' + item.type_document + ': ' + item.document + ')';
            }

            let message = customer + ' tiene ' + item.quantity  + ' ' + item.name + ' por ' + item.days_passed + ' día(s).';

            await Notification.create({
              type_notification_id: type.id,
              date_notification: currentDate,
              messages: message,
              viewed: false
            });
          }
        }

      }


    } catch (error) {
      Logger.error(error.name + ', ' + error.message);
      return false;
    }
  }

}

module.exports = NotificationsService;
