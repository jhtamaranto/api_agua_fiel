'use strict';

const Customer = use('App/Models/Customer');
const DetailOrder = use('App/Models/DetailOrder');
const GeneralData = use('App/Models/GeneralData');
const Logger = use('Logger');
const Order = use('App/Models/Order');
const RequestPromise = use('request-promise');
const User = use('App/Models/User');
const moment = require('moment');
const Debt = use ('App/Models/Debt');

const { NumerosaLetras } = use('App/Helpers/Currency');

class InvoiceService {

  async sendInvoiceToSenda(order_id) {
    try {
      let responseSenda = null;
      let generalData = await GeneralData.find(1);
      let data = await this.generateStructureInvoice(order_id);

      //return data;
      let url_production = `http://feint.sendaefact.pe/v3/cpe/cpecabe/`;
      //let url_production = `http://fe.sendaefact.pe/v3/cpe/cpecabe/`;
      let url_dev = `http://api.sendaefact.pe/v3/cpe/cpecabe/`;

      let options = {
        method: "POST",
        uri: url_production,
        body: data,
        auth: {
          username: generalData.user_fe,
          password: generalData.password_fe
        },
        headers: {
          "content-type": "application/json",
          "Accept": "application/json"
        },
        json: true
      };

      await RequestPromise(options)
        .then(response => {
          responseSenda = response;
        })
        .catch(error => {
          console.log(error);
          responseSenda = 'error';
        });

      return responseSenda;
    } catch (error) {
      Logger.error(error.name + ', ' + error.message);
      return null;
    }
  }

  async sendCreditNoteToSenda(creditNote) {
    try {

      let responseSenda = null;
      let generalData = await GeneralData.find(1);
      let data = await this.generateStructureCreditNote(creditNote);

      let url_production = `http://feint.sendaefact.pe/v3/cpe/cpecabe/`;
      //let url_production = `http://fe.sendaefact.pe/v3/cpe/cpecabe/`;
      let url_dev = `http://api.sendaefact.pe/v3/cpe/cpecabe/`;
      let url_baja = `http://fe.sendaefact.pe/v2/cpe/cpebaja/`;

      let options = {
        method: "POST",
        uri: url_production,
        body: data,
        auth: {
          username: generalData.user_fe,
          password: generalData.password_fe
        },
        headers: {
          "content-type": "application/json",
          "Accept": "application/json"
        },
        json: true
      };

      await RequestPromise(options)
        .then(response => {
          responseSenda = response;
        })
        .catch(error => {
          responseSenda = 'error';
        });

      return responseSenda;

    } catch (error) {
      Logger.error(error.name + ', ' + error.message);
      return null;
    }
  }

  async cancelDocument(order_id) {
    try {
      let responseSenda = null;

      let generalData = await GeneralData.find(1);
      let order = await Order.find(order_id);

      let tipodocu = '';
      if (order.type_document == 'BOLETA') {
        tipodocu = '03';
      } else if (order.type_document == 'FACTURA') {
        tipodocu = '01';
      }

      let data = {};
      data['ruc_emisor'] = generalData.company_document;
      data['tipodocu'] = tipodocu;
      data['nro_efact'] = order.serie + order.correlative;
      data['fechabaja'] = moment(order.canceled_date).format('YYYY-MM-DD[T]HH:mm:ss');//order.canceled_date;
      data['motivobaja'] = order.canceled_reason;

      let url_production = `http://feint.sendaefact.pe/v2/cpe/cpebaja/`;
      let url_dev = `http://fe.sendaefact.pe/v2/cpe/cpebaja/`;

      let options = {
        method: "POST",
        uri: url_production,
        body: data,
        auth: {
          username: generalData.user_fe,
          password: generalData.password_fe
        },
        headers: {
          "content-type": "application/json",
          "Accept": "application/json"
        },
        json: true
      };

      RequestPromise(options)
        .then(response => {
          responseSenda = true;
          console.log('documento dado de bajo de forma correcta');
        })
        .catch(error => {
          responseSenda = false;
          console.log('documento dado de bajo de forma incorrecta');
        });

      return responseSenda;

    } catch (error) {
      Logger.error(error.name + ', ' + error.message);
      return null;
    }
  }

  async generateStructureCreditNote(creditNote) {
    try {
      let general = await GeneralData.find(1);
      let order = await Order.find(creditNote.order_id);
      let detail = await DetailOrder.query()
        .where('order_id', order.id)
        .fetch();

      let nro_efact = creditNote.serie + creditNote.correlative;
      let tipodocu = '07';

      let index = 0;
      let items = [];
      let baseAmountToApplyDiscount = 0.00;
      let total_discount = 0.00;

      for (let item of detail.rows) {
        index++;
        let item_data = await this.getItemStructure(item);

        item_data['indice'] = index;
        item_data['ruc_emisor'] = general.company_document;
        item_data['tipodocu'] = tipodocu;
        item_data['nro_efact'] = nro_efact;

        if (parseFloat(item.discount_item) > 0) {
          total_discount = total_discount + parseFloat(item.discount_item);
          baseAmountToApplyDiscount = baseAmountToApplyDiscount + parseFloat(item.subtotal_original);
        }

        items.push(item_data);
      }

      let header = await this.getHeaderStructure(order, baseAmountToApplyDiscount, total_discount);

      //cambiando header por los datos de la nota de crédito
      header['tipodocurefe'] = header['tipodocu'];
      header['numerorefe'] = header['nro_efact'];
      header['fecharefe'] = header['fecha'];
      header['motivo_07_08'] = '01';
      header['descripcion_07_08'] = 'Anulación de la operación';

      header['tipodocu'] = '07';
      header['nro_efact'] = nro_efact;
      header['numero'] = creditNote.correlative;
      header['nro_serie_efact'] = creditNote.serie;
      header['fecha'] = moment(creditNote.created_at).format('YYYY-MM-DD[T]HH:mm:ss');//creditNote.created_at;
      header['fvenc'] = moment(creditNote.created_at).format('YYYY-MM-DD[T]HH:mm:ss');//creditNote.created_at;

      let total_descuento = parseInt(header['total_descuento']);
      let descuentoBruto = await (await this.getIgv(parseInt(total_descuento))).toFixed(2);
      let cantItems = 0;

      for (let i = 0; i < items.length; i++) {
        if (items[i].tipoprecioventa == '01') {
          cantItems = cantItems + 1;
        }
        
      }

      let descuentoItemBruto = (parseInt(total_discount)/cantItems).toFixed(2);
      let descuentoItemIgv =  await (await this.getIgv(parseInt(descuentoItemBruto))).toFixed(2);

      for (let i = 0; i < items.length; i++) {
        if (items[i].tipoprecioventa == '01') {
          let descuentoEnItem = (descuentoItemBruto/(parseInt(items[i].cantidad))).toFixed(2);
          let descuentoEnItemIGV = (descuentoEnItem/1.18).toFixed(2);

          items[i].valorunit = (items[i].valorunitbruto - descuentoEnItemIGV).toFixed(2);
          items[i].valorventa = items[i].valorunit * items[i].cantidad;
          items[i].preciounit = items[i].preciounitbruto - descuentoEnItem;
          items[i].precioventa = (items[i].preciounitbruto * items[i].cantidad) - descuentoItemBruto;
          items[i].preciounitc = (items[i].preciounitbruto * items[i].cantidad) - descuentoItemBruto;
          items[i].igv = (items[i].preciounitc*0.18).toFixed(2);
        }
        
      }

      let data = {
        items: items,
        cabecera: header
      };

      return data;

    } catch (error) {
      let message = 'FACTURACION-ERROR: generar estructura de nota de credito ' + creditNote.order_id;
      Logger.error(error.name + ', ' + message + ', '+ error.message);
      return null;
    }
  }

  async generateStructureInvoice(order_id) {
    try {
      let general = await GeneralData.find(1);

      let order = await Order.find(order_id);

      let detail = await DetailOrder.query()
        .where('order_id', order.id)
        .fetch();

      let nro_efact = order.serie + order.correlative;

      let tipodocu = '';
      if (order.type_document === 'BOLETA') {
        tipodocu = '03';
      } else if (order.type_document === 'FACTURA') {
        tipodocu = '01';
      }

      let index = 0;
      let items = [];
      let baseAmountToApplyDiscount = 0.00;
      let total_discount = 0.00;
      for (let item of detail.rows) {
        index++;
        let item_data = await this.getItemStructure(item);

        item_data['indice'] = index;
        item_data['ruc_emisor'] = general.company_document;
        item_data['tipodocu'] = tipodocu;
        item_data['nro_efact'] = nro_efact;

        if (parseFloat(item.discount_item) > 0) {
          total_discount = total_discount + parseFloat(item.discount_item);
          baseAmountToApplyDiscount = baseAmountToApplyDiscount + parseFloat(item.subtotal_original);
        }

        items.push(item_data);
      }

      let header = await this.getHeaderStructure(order, baseAmountToApplyDiscount, total_discount);

      let data = {
        items: items,
        cabecera: header
      }

      return data;

    } catch (error) {
      let message = 'FACTURACION-ERROR: generar estructura de factura ' + order_id;
      Logger.error(error.name + ', ' + message + ', ' + error.message);
      return null;
    }

  }

  async getItemStructure(itemDetail) {

      let orderData = await Order.find(itemDetail.order_id);
      let cod_cargodesc = '';
      let base_cargodesc = 0;

      if (parseFloat(itemDetail.discount_value) > 0) {
        cod_cargodesc = '01';
        if (orderData.type_document === 'BOLETA') {
          cod_cargodesc = '00';
        }
        base_cargodesc = itemDetail.subtotal_original;
      }

      let codigo_sunat = '';
      let valorventa = await this.getSubtotal(parseFloat(itemDetail.subtotal));
      let precioventa = parseFloat(itemDetail.subtotal);
      let igv = await this.getIgv(parseFloat(itemDetail.subtotal));
      let dscto_unit = itemDetail.discount_item;
      let tipo_afect_igv = '';
      let codigo_tributo = '';
      let tipoprecioventa = '';
      if (itemDetail.is_bonus) {
        tipo_afect_igv = '15';
        codigo_tributo = '9996';
        tipoprecioventa = '02';
        dscto_unit = '0.00';
      } else {
        tipo_afect_igv = '10';
        codigo_tributo = '1000';
        tipoprecioventa = '01';
      }

      let item_data = {};
      item_data['ruc_emisor'] = '';
      item_data['tipodocu'] = '';
      item_data['nro_efact'] = '';
      item_data['indice'] = 0;
      item_data['codigo'] = itemDetail.id.toString();
      item_data['codigo_sunat'] = codigo_sunat;
      item_data['codigo_gs1'] = '';
      item_data['descripcion'] = itemDetail.name;
      item_data['cantidad'] = (itemDetail.quantity) * 1.0000000000;
      item_data['unid'] = 'NIU';
      item_data['tipoprecioventa'] = tipoprecioventa;
      item_data['tipo_afect_igv'] = tipo_afect_igv;
      item_data['codigo_tributo'] = codigo_tributo;
      item_data['is_anticipo'] = '0';
      item_data['valorunitbruto'] = await this.getSubtotal(parseFloat(itemDetail.base_price)) * 1.0000000000;
      item_data['valorventabruto'] = item_data['cantidad'] * item_data['valorunitbruto'];
      item_data['valorunit'] = await this.getSubtotal(parseFloat(itemDetail.base_price) * 1.0000000000);
      item_data['valorventa'] =  item_data['cantidad'] *item_data['valorunit'];//valorventa;
      item_data['preciounitbruto'] = parseFloat(itemDetail.base_price);
      item_data['preciounit'] = parseFloat(itemDetail.base_price);
      item_data['precioventa'] = precioventa;
      item_data['preciounitc'] = precioventa;
      item_data['igv'] = igv;
      item_data['porc_igv'] = '18';
      item_data['isc'] = '0.00';
      item_data['porc_isc'] = '0.00';
      item_data['dscto_unit'] = '0.00'; //dscto_unit;
      item_data['porc_dscto_unit'] = '0.00';
      item_data['cod_cargodesc'] = ""; //cod_cargodesc;
      item_data['base_cargodesc'] = '0.00';// base_cargodesc;
      item_data['otrostributos_porc'] = '0.00';
      item_data['otrostributos_monto'] = '0.00';
      item_data['otrostributos_base'] = '0.00';
      item_data['placavehiculo'] = '';
      item_data['tot_impuesto'] = '0.00';
      item_data['opt_tipodoi'] = '0';
      item_data['opt_numerodoi'] = '';
      item_data['opt_pasaportepais'] = '';
      item_data['opt_huesped'] = '';
      item_data['opt_huespedpais'] = '';
      item_data['opt_fingresopais'] = '1900-01-01T00:00:00';
      item_data['opt_fcheckin'] = '1900-01-01T00:00:00';
      item_data['opt_fcheckout'] = '1900-01-01T00:00:00';
      item_data['opt_fconsumo'] = '1900-01-01T00:00:00';
      item_data['opt_diaspermanencia'] = '0';
      item_data['visible'] = '1';
      item_data['grupo'] = '1';
      item_data['flat'] = '0';

      return item_data;

  }

  async getHeaderStructure(order, baseAmountToApplyDiscount, total_discount) {

      let enterprise = await GeneralData.find(1);

      let local = '';
      let customer = await Customer.find(order.customer_id);
      let typeDocument = customer.type_document;

      let customerFullname = customer.name;
      if (customer.surname) {
        customerFullname = customerFullname + ' ' + customer.surname;
      }

      let email_cc = 'contabilidad.aguacubica@gmail.com';
      let email_customer = customer.email;
      if (email_customer == null || email_customer == '') {
        email_customer = email_cc;
        email_cc = '';
      }

      let code_sunat = '';
      let tipodoi = code_sunat;
      let numerodoi = customer.document;
      let desc_tipodocu = typeDocument;
      let documentCustomer = customer.document;
      if (typeDocument === 'DNI') {
        code_sunat = '1';
        tipodoi = code_sunat;
        if (!documentCustomer || documentCustomer.length != 8) {
          tipodoi = '0';
          numerodoi = '-';
          desc_tipodocu = 'DOC.TRIB.NO.DOM.SIN.RUC';
        }
      } else if (typeDocument === 'RUC') {
        code_sunat = '6';
        tipodoi = code_sunat;
      }

      let seller = await User.find(order.user_id);
      let sellerName = seller.first_name;
      if (seller.last_name) {
        sellerName = sellerName + ' ' + seller.last_name;
      }

      let nro_efact = order.serie + order.correlative;

      let tipodocu = '';
      if (order.type_document === 'BOLETA') {
        tipodocu = '03';
      } else if (order.type_document === 'FACTURA') {
        tipodocu = '01';
      }

      let cargodesc_motivo = '';
      if (total_discount > 0) {
        cargodesc_motivo = '02'; //03
      }

      let telefono_emisor = '';
      if (enterprise.company_phone) {
        telefono_emisor = enterprise.company_phone + ' / ' + enterprise.phone_aux;
      }

      let orderDate = moment(order.date_invoice).format('YYYY-MM-DD[T]HH:mm:ss');//order.date_invoice;

      let glosa = '';

      

      let customer_address = customer.address;
      let order_address = order.order_address;
      if (order_address) {
        customer_address = order_address;
      }
      if (!customer_address) {
        customer_address = '';
      }
      let metodo_pago = 'CONTADO';
      let codigo_metodopago = 'CON';
      let desc_metodopago = '';
      let debt = await Debt.query()
                        .where('order_id', order.id)
                        .first();
      if (order.type_sale == 'credito') {
        metodo_pago = 'CREDITO';
        codigo_metodopago = 'CRE';
        
        let paymentDate = moment(debt.due_date).format('YYYY-MM-DD');
        desc_metodopago = `${order.total},1;${order.total};${paymentDate}`;

        if (order.total_payment > 0) {
          glosa = `Pagó ${order.total_payment} de adelanto`;
        }
      }

      let header = {};

      header['ruc_emisor'] = enterprise.company_document;
      header['razonsocial_emisor'] = enterprise.company_name;
      header['direccion_emisor'] = enterprise.company_address;
      header['telefono_emisor'] = telefono_emisor;
      header['email_emisor'] = enterprise.company_email;
      header['cod_domifiscal'] = '0000';
      header['tipoventasunat'] = '01';
      header['tiop_codi'] = '0101';
      header['is_gratuita'] = '0';
      header['fecha'] = orderDate;
      header['fvenc'] = orderDate;
      header['tipodocu'] = tipodocu;
      header['nro_serie_efact'] = order.serie;
      header['numero'] = order.correlative;
      header['nro_efact'] = nro_efact;
      header['tipo_moneda'] = 'PEN';
      header['tipodocurefe'] = '';
      header['numerorefe'] = '';
      header['motivo_07_08'] = '';
      header['descripcion_07_08'] = '';
      header['fecharefe'] = '1900-01-01T00:00:00';
      header['tipodoi'] = tipodoi;
      header['numerodoi'] = numerodoi;
      header['desc_tipodocu'] = desc_tipodocu;
      header['razonsocial'] = customerFullname;
      header['direccion'] = customer_address;
      header['cliente'] = customerFullname;
      header['email_cliente'] = email_customer;
      header['email_cc'] = email_cc;
      header['codigo_cliente'] = '';
      header['rec_tele'] = '';
      header['rec_ubigeo'] = '';
      header['rec_pais'] = '';
      header['rec_depa'] = '';
      header['rec_provi'] = '';
      header['rec_distri'] = '';
      header['rec_urb'] = '';
      header['vendedor'] = sellerName;
      header['metodo_pago'] = metodo_pago;
      header['codigo_metodopago'] = codigo_metodopago;
      header['desc_metodopago'] = desc_metodopago;
      header['totalpagado_efectivo'] = '0.00';
      header['propina'] = '0.00';
      header['prestamo'] = '0.00';
      header['vuelto'] = '0.00';
      header['local'] = local;
      header['caja'] = '';
      header['cajero'] = '';
      header['nro_serie'] = '';
      header['nro_transaccion'] = '';
      header['orden_compra'] = '';
      header['glosa'] = glosa;
      header['glosa_refe'] = '';
      header['glosa_pie_pagina'] = '';
      header['mensaje'] = '';
      header['tipodocu_gr'] = '';
      header['numero_gr'] = '';
      header['docurela_numero'] = '';
      header['subtotal'] = order.subtotal;
      header['op_exportacion'] = '0.00';
      header['op_exonerada'] = '0.00';
      header['op_inafecta'] = '0.00';
      header['op_gravada'] = order.subtotal;
      header['tot_valorventa'] = order.subtotal;
      header['tot_precioventa'] = order.total;
      header['isc'] = '0.00';
      header['igv'] = order.igv;
      header['porc_igv'] = '18';
      header['igv_gratuita'] = await this.getIgv(parseFloat(order.total_gratuitas));
      header['importe_total'] = order.total;
      header['total_pagar'] = order.total;
      header['redondeo'] = '0.00';
      header['total_otros_tributos'] = '0.00';
      header['total_otros_cargos'] = 0;
      header['cargodesc_motivo'] = cargodesc_motivo;
      header['cargodesc_base'] = baseAmountToApplyDiscount;
      header['porc_dsctoglobal'] = '0.00';
      header['total_descuento'] = total_discount;
      header['descto_global'] = await this.getSubtotal(parseFloat(total_discount)); //0;
      header['total_gratuitas'] = order.total_gratuitas;
      header['importe_letras'] = NumerosaLetras(order.total).toUpperCase() + ' SOLES';
      header['total_servicio'] = '0.00';
      header['porc_servicio'] = '0.00';
      header['file_nro'] = '';
      header['centro_costo'] = '';
      header['nropedido'] = '';
      header['modelo_transp'] = '';
      header['puerto_embarque'] = '';
      header['puerto_destino'] = '';
      header['icoterms'] = '';
      header['pais_origen'] = '';
      header['cant_bultos'] = '0';
      header['peso_bruto'] = '0.00';
      header['peso_neto'] = '0.00';
      header['codigo_sucu'] = '';
      header['detraccion_bs'] = '';
      header['detraccion_nrocta'] = '';
      header['detraccion_porc'] = '0.00';
      header['detraccion_monto'] = '0.00';
      header['detraccion_moneda'] = '';
      header['detraccion_mediopago'] = '';
      header['glosa_detraccion'] = '';
      header['total_anticipo'] = '0.00';
      header['ant_ruc'] = '';
      header['ant_idpago'] = '';
      header['ant_fpago'] = '1900-01-01T00:00:00';
      header['ant_tipodocu'] = '';
      header['ant_numero'] = '';
      header['ant_monto'] = '0.00';
      header['ant_moneda'] = '';
      header['importe_percepcion'] = '0.00';
      header['per_regimen'] = '';
      header['per_codigo'] = '';
      header['per_porc'] = '0.00';
      header['gr_activo'] = '0';
      header['gr_motitraslado'] = '';
      header['gr_pesobruto'] = '0.00';
      header['gr_modalidadtransp'] = '';
      header['gr_fini_tras_entrega'] = '1900-01-01T00:00:00';
      header['gr_ubigeo_partida'] = '';
      header['gr_ubigeo_llegada'] = '';
      header['gr_direpartida'] = '';
      header['gr_direllegada'] = '';
      header['gr_rztransportista'] = '';
      header['gr_ructransportista'] = '';
      header['gr_regmtctransportista'] = '';
      header['gr_nombconductor'] = '';
      header['gr_licenciaconductor'] = '';
      header['gr_tipodoiconductor'] = '';
      header['gr_nroconductor'] = '';
      header['gr_marcavehiculo'] = '';
      header['gr_placavehiculo'] = '';
      header['gr_nroconstvehiculo'] = '';
      header['ds_value'] = '';
      header['con_detalle'] = 'S';
      header['id_resumen'] = '';
      header['usuario'] = sellerName;
      header['identificador'] = '';
      header['tipocambio'] = '3.354';
      header['fcheckin'] = '1900-01-01T00:00:00';
      header['fcheckout'] = '1900-01-01T00:00:00';
      header['cantdias'] = '';
      header['habitaciones'] = '';
      header['nro_reserva'] = '';
      header['nomb_reserva'] = '';
      header['nro_prepagos'] = '';
      header['estado'] = 'E';
      header['flat'] = '0';
      header['xml'] = '0';
      header['descripcion_xml'] = '';
      header['xml_firmado'] = '0';
      header['pdf'] = '0';
      header['email'] = '0';
      header['codigobarra'] = '';
      header['url_pdf'] = '';
      header['url_xml'] = '';
      header['cdr'] = '';
      header['descripcioncdr'] = '';
      header['frecepcionsunat'] = '1900-01-01T00:00:00';
      header['visibledesgloseimpuesto'] = '0';
      header['visibletc'] = '0';
      header['visiblehabitacion'] = '0';
      header['visiblehoraemision'] = '0';
      header['visiblediasohoras'] = '0';
      header['visiblehoracheckinout'] = '0';
      header['visibleglosadetra'] = '0'

      return header;

  }

  async getSubtotal(amount) {
    let igv = await this.getIgv(amount);
    let total = amount - igv;

    return Math.round(total * 100) / 100;
  }

  async getIgv(amount) {
    let general = await GeneralData.find(1);

    let igv = parseFloat(general.igv_value);

    let amountIgv = amount - amount / (1 + igv / 100);
    return Math.round(amountIgv * 100) / 100;
  }

  async anularComprobante(order_id) {
    try {
      let responseSenda = null;

      let generalData = await GeneralData.find(1);
      let order = await Order.find(order_id);

      let tipodocu = '';
      if (order.type_document === 'BOLETA') {
        tipodocu = '03';
      } else if (order.type_document === 'FACTURA') {
        tipodocu = '01';
      }

      let data = {};
      data['ruc_emisor'] = generalData.company_document;
      data['tipodocu'] = tipodocu;
      data['nro_efact'] = order.serie + order.correlative;
      data['fechabaja'] = order.canceled_date;
      data['motivobaja'] = order.canceled_reason;

      let url_production = `http://feint.sendaefact.pe/v2/cpe/cpebaja/`;
      //let url_production = `http://fe.sendaefact.pe/v2/cpe/cpebaja/`;

      let options = {
        method: "POST",
        uri: url_production,
        body: data,
        auth: {
          username: generalData.user_fe,
          password: generalData.password_fe
        },
        headers: {
          "content-type": "application/json",
          "Accept": "application/json"
        },
        json: true
      };

      RequestPromise(options)
        .then(response => {
          responseSenda = true;
          console.log('documento dado de bajo de forma correcta');
        })
        .catch(error => {
          responseSenda = false;
          console.log('documento dado de bajo de forma incorrecta');
        });

      return responseSenda;

    } catch (error) {
      Logger.error(error.name + ', ' + error.message);
      return null;
    }
  }
}

module.exports = InvoiceService;
