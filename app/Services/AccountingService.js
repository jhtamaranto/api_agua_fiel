'use strict';

const Order = use('App/Models/Order');

class AccountingService {
  async sales (parameters) {
    let query = Order.query()
      .select([
        'orders.date_invoice as sales_date',
        'orders.type_document as sales_document',
        'orders.serie as sales_serie',
        'orders.correlative as sales_correlative',
        'orders.subtotal as sales_subtotal',
        'orders.igv as sales_igv',
        'orders.total as sales_total',
        'orders.canceled as sales_canceled',
        'customers.document as customer_document',
        'customers.name as customer_name',
        'customers.surname as customer_surname',
        'customers.type_document as customer_type_document'
      ]).innerJoin('customers', 'customers.id', 'orders.customer_id')
      .whereIn('orders.type_document', ['BOLETA', 'FACTURA']);

    if (parameters.date_from) {
      query.whereRaw('DATE(orders.date_invoice) >= ?', [parameters.date_from]);
    }

    if (parameters.date_until) {
      query.whereRaw('DATE(orders.date_invoice) <= ?', [parameters.date_until]);
    }

    let sales = await query
      .orderByRaw('date(orders.date_invoice), orders.type_document, orders.correlative')
      .fetch();

    return sales;
  }
}

module.exports = AccountingService;
