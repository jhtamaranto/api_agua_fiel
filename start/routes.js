'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const date = require('date-and-time');
const Route = use('Route')
const InvoiceService = use('App/Services/InvoiceService');
const CreditNote = use('App/Models/CreditNote');
const Order = use('App/Models/Order');

Route.get('/', () => {
  return {greeting: 'Hello world in JSON'}
})

Route.group(() => {
  Route.post('/register', 'AuthController.register')
  Route.post('/login', 'AuthController.login')

  Route.get('customers', 'CustomerController.index')
  Route.get('customers/datatable', 'CustomerController.indexDatatable')
  Route.post('customers', 'CustomerController.store')
  Route.patch('customers/:id', 'CustomerController.update')
  Route.delete('customers/:id', 'CustomerController.delete')
  Route.post('customers/:id/change-status', 'CustomerController.changeStatus')
  Route.post('validate-document', 'CustomerController.validateDocument')
  Route.get('customers/dropdown', 'CustomerController.dropdown')
  Route.get('customers/debts', 'CustomerController.getDebts')
  Route.get('customers/summary', 'CustomerController.summary')

  Route.get('categories', 'CategoryController.index');
  Route.get('categories/dropdown', 'CategoryController.dropdown')

  Route.get('units/dropdown', 'UnitController.dropdown')

  Route.get('supplies/dropdown', 'SupplyController.suppliesDropDown')
  Route.get('supplies/brands', 'SupplyController.brands')
  Route.get('supplies/download', 'ExcelSupplyController.index')
  Route.get('supplies/:id', 'SupplyController.show')
  Route.get('supplies', 'SupplyController.index')
  Route.post('supplies', 'SupplyController.store')
  Route.post('supplies/store-decrease', 'SupplyController.storeDecrease')
  Route.patch('supplies/:id', 'SupplyController.update')
  Route.delete('supplies/:id', 'SupplyController.inactivate')
  //Route.get('supplies', 'ProductController.supplies')

  Route.get('assets', 'AssetController.index')
  Route.post('assets', 'AssetController.store')
  Route.patch('assets/:id', 'AssetController.update')
  Route.delete('assets/:id', 'AssetController.inactivate')

  Route.get('products', 'ProductController.index')
  Route.post('products', 'ProductController.store')
  Route.patch('products/:id', 'ProductController.update')
  Route.delete('products/:id', 'ProductController.inactivate')
  Route.get('products/summary', 'ProductController.summary')
  Route.delete('products/delete/:id', 'ProductController.destroy')

  Route.get('composition/:id/by-product', 'CompositionController.getByProduct')

  Route.get('mobilities/can-attend', 'MobilityController.canAttendOrder');
  Route.get('mobilities', 'MobilityController.index')
  Route.post('mobilities', 'MobilityController.store')
  Route.patch('mobilities/:id', 'MobilityController.update')
  Route.delete('mobilities/:id', 'MobilityController.inactivate')

  Route.get('resources', 'ResourceController.index')

  Route.get('roles', 'RoleController.index')
  Route.post('roles', 'RoleController.store')
  Route.patch('roles/:id', 'RoleController.update')
  Route.delete('roles/:id', 'RoleController.inactivate')
  Route.get('roles/permissions', 'MenuController.getMenus')

  /*
  * Routes for point of sale
  */
  Route.get('catalogue', 'ProductController.catalogue')

  Route.get('orders-status/dropdown', 'OrderStatusController.dropdown')
  Route.get('orders-status', 'OrderStatusController.index')

  Route.get('orders', 'OrderController.index')
  Route.post('orders', 'OrderController.store')
  Route.patch('orders/change-status', 'OrderController.changeStatus')
  Route.patch('orders/assing-final-deliveryman', 'OrderController.assingFinalDeliveryMan')
  Route.get('orders/by-customer', 'OrderController.getByCustomer')
  Route.post('orders/update', 'OrderController.update')
  Route.get('orders/summary', 'OrderController.getSummary')
  Route.get('orders/get-by-id', 'OrderController.getById');

  /*
  * Routes fro users
  */
  Route.get('delivery-users/dropdown', 'UserController.deliveryUserDropDown')
  Route.get('delivery-users', 'UserController.deliveryUsers')

  Route.post('payments/register', 'PaymentController.register')

  Route.get('production', 'ProductionController.index')

  Route.get('debts/pending', 'DebtController.pendingDebts')
  Route.get('debts/by-customer', 'DebtController.getByCustomer')
  Route.post('debts/register-payment', 'DebtController.registerPayment')

  Route.get('debts/payments', 'DebtPaymentController.getByDebt')

  /*
  * Routes for providers
  */
  Route.get('providers', 'ProviderController.index')
  Route.post('providers', 'ProviderController.store')
  Route.patch('providers/:id', 'ProviderController.update')
  Route.delete('providers/:id', 'ProviderController.inactivate')

  /*
  + Routes for purchases
  */
  Route.post('purchases', 'PurchaseController.store')

  /*
  * Routes for inventory supply
  */
  Route.get('kardex-by-supply', 'KardexSupplyController.getBySupply')
  Route.post('kardex-save-inventory', 'KardexSupplyController.saveNewInventory')

  /*
  * Route for Providers Account
  */
  Route.get('provider-account/pending', 'ProviderAccountController.pendingDebts')
  Route.get('provider-account/by-provider', 'ProviderAccountController.accountsByProvider')
  Route.post('provider-account/amortize', 'ProviderAccountController.amortize')

  Route.get('storehouses', 'StorehouseController.index')
  Route.get('storehouses/inventories', 'StorehouseController.getInventory')
  Route.get('storehouses/inventory-main', 'StorehouseController.getInventoryFromStorehouseMain')
  Route.post('storehouses/recharge', 'StorehouseController.registerRecharge')
  Route.post('storehouses/decrease', 'StorehouseController.registerDecrease')
  Route.post('storehouses/discharge', 'StorehouseController.registerDischarge')

  Route.get('kardex-product', 'KardexProductController.getByInventory')

  Route.get('cashflow/current', 'CashflowController.getCurrentCash')
  Route.get('cashflow/payments-debts', 'CashflowController.getPaymentsDebts')
  Route.get('cashflow/payments-cash', 'CashflowController.getPaymentsOrders')
  Route.post('cashflow/open', 'CashflowController.open')
  Route.post('cashflow/operations', 'CashflowController.registerOperation')
  Route.get('cashflow/operations', 'CashflowController.getOperations')
  Route.get('cashflow/total-amounts', 'CashflowController.getTotalAmounts')
  Route.patch('cashflow/close', 'CashflowController.close')
  Route.get('cashflow/movements-user', 'CashflowController.movementsByUser')

  Route.get('reports/purchases', 'ReportController.purchases')
  Route.get('reports/purchases-detail', 'ReportController.getDetailPurchase')
  Route.get('reports/debts', 'ReportController.debts')
  Route.get('reports/debts/datatable', 'ReportController.debtsDatatable')
  Route.get('reports/sales', 'ReportController.sales')
  Route.post('reports/sales/canceled', 'ReportController.cancelSale')
  Route.get('reports/sales/datatable', 'ReportController.salesDatatable')
  Route.get('reports/sales/sumSalesDatatable', 'ReportController.sumSalesDatatable')
  Route.get('reports/orders', 'ReportController.orders')
  Route.get('reports/orders/datatable', 'ReportController.ordersDatatable')
  Route.get('reports/cash', 'ReportController.cash')
  Route.get('reports/credit-notes', 'ReportController.creditNotes')
  Route.get('reports/sales-accounting', 'ExcelSaleAccountingController.index')
  Route.get('reports/sales-download', 'ExcelSaleController.index')

  Route.get('users/get-all', 'UserController.getAll')
  Route.get('users/get-roles', 'UserController.rolesDropDown')
  Route.post('users/create', 'UserController.store')
  Route.post('users/update', 'UserController.update')
  Route.delete('users/change-status/:id', 'UserController.changeStatus')
  Route.get('users/available-delivery-users', 'UserController.availableDeliveryUsers')

  Route.get('dashboard/sold-products', 'DashboardController.soldProducts')
  Route.get('dashboard/sales-month', 'DashboardController.salesMonth')

  Route.get('notifications/list', 'NotificationController.index');
  Route.post('notifications/generate', 'NotificationController.store');
  Route.get('notifications/get-types', 'NotificationController.getTypes');
  Route.post('notifications/store-types', 'NotificationController.storeTypes');

  Route.post('invoices/generate', 'InvoiceController.generateInvoice');

  Route.get('addresses', 'CustomerAddressController.index');
  Route.post('addresses', 'CustomerAddressController.store');
  Route.put('addresses/:id', 'CustomerAddressController.update');
  Route.delete('addresses/:id', 'CustomerAddressController.destroy');

  Route.get('prices', 'CustomerPriceController.index');
  Route.get('prices/customer/product', 'CustomerPriceController.getPriceCustomerProduct');
  Route.post('prices', 'CustomerPriceController.store');
  Route.delete('prices/:id', 'CustomerPriceController.destroy');
  Route.put('prices/:id', 'CustomerPriceController.update');

  Route.get('drums', 'DrumController.index');
  Route.get('drums/brands', 'DrumController.brands');
  Route.post('drums/store-loan', 'DrumController.storeLoan');
  Route.post('drums/store-return', 'DrumController.storeReturn');
  Route.delete('drums/delete/:id', 'DrumController.deleteLoanReturn');
  Route.get('products/with-return', 'ProductController.productHasReturn');
  Route.post('drums/confirm-return', 'DrumController.confirmReturn');
  Route.get('drums/download-detail', 'ExcelDrumDetailController.index');
  Route.get('drums/download-summary', 'ExcelDrumSummaryController.index');

  Route.get('detail-payments', 'DetailPaymentController.index');
  Route.post('detail-payments/store-confirmation', 'DetailPaymentController.storeConfirmation');
  Route.get('detail-payments/download', 'ExcelPaymentController.index');

  Route.get('takings', 'TakingController.index');
  Route.post('takings', 'TakingController.store');
  Route.get('takings/calculate', 'TakingController.calculate');
  Route.get('takings/download', 'ExcelTakingController.index');
  Route.delete('takings/:id', 'TakingController.destroy');

  Route.get('actualizar-metodo-pago', 'OrderController.storeMethodPayment');
  Route.get('zona', 'ZonaController.index')
  Route.post('zona', 'ZonaController.store')
  Route.delete('zona/:id','ZonaController.delete')
  Route.put('zona/:id','ZonaController.update')
  Route.get('zona/search','ZonaController.search')

  Route.get('/asignacion/prevendedores','AsignacionController.findAllPrevendedores')
  Route.get('/asignacion/clientes','AsignacionController.findAllClientes')

  Route.get('enviar-factura/:id', async ({params}) => {
    let invoiceService = new InvoiceService;
    let invoiceSend = await invoiceService.sendInvoiceToSenda(params.id);
    if (invoiceSend === '0000-CPE ha sido aceptada') {
      let order = await Order.find(params.id);
      if (order) {
        order.send_fe = true;
        await order.save();
      }
    }
    //let data = await invoiceService.generateStructureInvoice(932);
    //console.log(data)
    return invoiceSend;
  })

  Route.get('enviar-nota-credito', async () => {
    //let letras = NumerosaLetras(1380.65);
    let creditNote = await CreditNote.find(3);
    let invoiceService = new InvoiceService;
    let dataResponse = await invoiceService.sendCreditNoteToSenda(creditNote);
    console.log(dataResponse)
    return dataResponse;
  })

  Route.get('dar-baja/:id', async ({params}) => {
    let invoiceService = new InvoiceService;
    let dataResponse = await invoiceService.anularComprobante(params.id);
    console.log(dataResponse)
    return dataResponse;
  })

  Route.get('estructura-factura/:id', async ({params}) => {
    let invoiceService = new InvoiceService;
    let data = await invoiceService.generateStructureInvoice(params.id);

    return data;
  })

}).prefix('api');
